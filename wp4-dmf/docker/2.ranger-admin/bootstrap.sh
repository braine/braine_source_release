#!/bin/bash
set -xe
cd contrib/solr_for_audit_setup
./setup.sh
cd ../../
/opt/solr/ranger_audit_server/scripts/start_solr.sh &
./setup.sh
ranger-admin start
tail -f logfile
