1. Build ranger_base which compiles ranger source code
2. Build ranger-admin image and push to the image repo. This image copies admin bin from ranger_base
3. Build ozone-ranger image which uses apache/ozone base and takes ozone ranger plugin from ranger_base
4. Set appropriate images and configs in helm/kubernetes.