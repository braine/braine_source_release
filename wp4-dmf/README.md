Docker images and Helm charts for:
 - Policy manager based on Apache Ranger
 - Distributed data storage solution based on Apache Ozone
 - Apache Ranger plugin for Ozone along with the relevant configurations
