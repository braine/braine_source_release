#!/bin/bash  
set -x
docker run -d --name telegraf -v $(pwd)/telegraf.conf:/etc/telegraf/telegraf.conf:ro --net=host telegraf:1.21

set +x
