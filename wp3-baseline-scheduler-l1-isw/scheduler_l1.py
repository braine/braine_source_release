import sys
import os
import test_conf


from obj.ue import UE
from obj.pod import Pod
from obj.emdc import Cluster 



class Scheduler:
    def __init__(self, coming_ue =None):
        self.coming_ue = coming_ue

        self.cpu_scores = None
        self.mem_scores = None

    # set ue to scheduler 
    def place_ue(self, ue:UE):
        self.coming_ue = ue
        print(f"UE [{ue}] was placed to scheduler")
    
    def print_pods(self):
        for pod in self.cluster.pods:
            print(type(pod), pod)

# check if pods have enough resources to allocate UE on it
    def if_pods_have_req_res(self):
        pod_to_cpu_availability = {pod.name : pod.cpu_threshold >\
            (pod.cpu_used + self.coming_ue.cpu)\
                for pod in self.cluster.pods}
        pod_to_mem_availability = {pod.name : pod.mem_threshold >\
            (pod.mem_used + self.coming_ue.mem)\
                for pod in self.cluster.pods}
        return pod_to_cpu_availability, pod_to_mem_availability


# get percentage of available resources
    def get_pods_avail_res(self):
        pod_to_cpu_avail =\
            {pod.name : 1 - pod.cpu_used for pod in self.cluster.pods}
        pod_to_mem_avail =\
            {pod.name : 1 - pod.mem_used for pod in self.cluster.pods}
        return pod_to_cpu_avail, pod_to_mem_avail

# unavailable pod will be scored with 0 
    def score_pods(self):
        pod_to_is_have_req_cpu, pod_to_is_have_req_mem = self.if_pods_have_req_res()

        pod_to_cpu_avail, pod_to_mem_avail = self.get_pods_avail_res()

        self.cpu_scores = {k : pod_to_cpu_avail[k] * pod_to_is_have_req_cpu[k]\
                for k in pod_to_is_have_req_cpu.keys()}
        self.mem_scores = {k : pod_to_mem_avail[k] * pod_to_is_have_req_mem[k]\
                for k in pod_to_is_have_req_mem.keys()}
        return self.cpu_scores, self.mem_scores

# choose pod for UE according scores 
    def select_pod_for_ue(self):
        cpu_scores_copy = self.cpu_scores.copy()
        for _ in range(len(self.cpu_scores)):
            best_pod = max(cpu_scores_copy, key=cpu_scores_copy.get)
            cpu_scores_copy.pop(best_pod)
            if not self.mem_scores[best_pod]:
                continue
            return best_pod

# function for ue placement -> update fields (connected UEs, resource values) of the pod and the UE. 
# delete Ue from scheduler (delete object)
    def place_eu_to_pod(self):
        # ue updated fields
        selected_pod_name = self.select_pod_for_ue()
        selected_pod = next((x for x in self.pods if x.name == selected_pod_name), None)
        self.coming_ue.pod = selected_pod
        self.coming_ue.scheduled = True
        # pod updated fields
        selected_pod.ue_connected.append(self.coming_ue)
        # here resources are hard-writen 
        selected_pod.cpu_used += self.coming_ue.cpu
        selected_pod.mem_used += self.coming_ue.mem
        


    # analog for FCFS (first come first serve algorithm)
    def fcfs_scheduler(self, pod, cluster):
        pass

    # consider priority based algorithm 
    def priority_scheduler(self):
        pass


    # shortest computational algorithm
    def shortest_scheduler(self):
        pass




    

