#!/usr/bin/env python
import json
import yaml
import requests
import sys
import argparse
import os
import shutil

# Temporary github location for Visualization specifications
DL_REPO = 'https://raw.githubusercontent.com/johmicrot/BRAINE_VS/main/visualization_specifications/'

# Variables for branding
LOGO = '<img src=%s>'
EMAIL = '<br>%s'
COMP_DESCRIPT = '<br>%s'

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def main():

    args = getOptions()

    # Might want to do a different or more robust check to check if web address
    if 'http' in args.config:
        dash_config = requests.get(args.config).text
        yml_config = yaml.load(dash_config, Loader=yaml.FullLoader)
    else:
        yml_config = yaml.load(open(args.config, 'r'), Loader=yaml.FullLoader)
    # print(bcolors.WARNING + "Warning: No active frommets remain. Continue?" + bcolors.ENDC)
    print(bcolors.OKGREEN +'Sucessfully Loaded config file from: %s' % args.config + bcolors.ENDC)

    modules = yml_config.get('dashboard_elements')

    # Add branding
    mod = json.loads(requests.get(DL_REPO + 'company_branding.json').text)
    mod['options']['content'] = generate_branding(yml_config)

    # Generate the panels in the order specified from the config file
    new_panels = []
    new_panels.append(mod)
    for module in modules:
        mod = requests.get(DL_REPO + module).text
        print(bcolors.OKGREEN + 'Downloading module from: ', DL_REPO + module + bcolors.ENDC)
        new_panels.append(json.loads(mod))

    # Grabs the empty dashboard from the specified repo and converts it to json format
    json_dash = json.loads(requests.get(args.dashboard_name).text)
    json_dash['panels'] = new_panels

    # Save file with indentation
    with open(args.output, 'w') as outfile:
        json.dump(json_dash, outfile, indent=2)
        print(bcolors.OKGREEN + 'Saved new dashboard as: ' + bcolors.ENDC, args.output)

# Process argument inputs
def getOptions(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-d", "--dashboard", type=str, required=True, help="url of the dashboard")
    parser.add_argument("-c", "--config", default=None, help="Naming the Config file. Defaults accepts config with same name as empty dashboard")
    parser.add_argument("-o", "--output", required=True, help="Your destination output file name")
    arguments = parser.parse_args(args)

    # Must check inside the dashboard that it must not containt .json
    # print(bcolors.WARNING + "Config file is not found, using default from: %s" % arguments.config + bcolors.ENDC)
    if '.json' in arguments.dashboard:
        print(bcolors.FAIL + 'Dashboard directory should not contain .json' + bcolors.ENDC)
        exit()
    if '.json' not in arguments.output:
        print(bcolors.FAIL + 'Output file should be a .json' + bcolors.ENDC)
        exit()
    # Not needed, but adds clarity, possibly change/remove later
    parent_directory = arguments.dashboard

    # Strip the tail "/" in case it is included in the input.  Could do a more robust check
    default_dash = parent_directory.strip('/').split('/')[-1]
    arguments.dashboard_name = parent_directory + default_dash + '.json'

    # If -c is not provided the default online config file will be used
    if arguments.config == None:
        arguments.config = parent_directory + default_dash + '.config.yml'
    # If file at local directory or the website doesn't exist, use default
    # Logic here can be changed to be more robust.  Had to do "'http' in arguments.config to prevent requests.get from
    # making requests from non-urls
    elif not os.path.isfile(arguments.config) or \
            (('http' in arguments.config) and requests.get(arguments.config).status_code != 200):
        arguments.config = parent_directory + default_dash + '.config.yml'
        print(bcolors.WARNING + "Config file is not found, using default from: %s" % arguments.config + bcolors.ENDC)


    return arguments


def generate_branding(branding):
    brand = branding.get('branding')

    if 'http' not in brand['image_logo']:
        assert os.path.isfile(brand['image_logo']), bcolors.FAIL + 'Branding logo not found' + bcolors.ENDC

        filename = brand['image_logo'].split('/')[-1]
        new_location = '../monitoring/grafana/branding/' + filename
        # Copy the branding file to the Grafana directory
        shutil.copyfile(brand['image_logo'], new_location)
        # Make the logo point to the Grafana directory in the docker container
        brand['image_logo'] = "/public/branding_img/" + filename

    return LOGO % brand['image_logo'] + \
        EMAIL % brand['email'] + \
        COMP_DESCRIPT % brand['description']


if __name__ == '__main__':
    main()
