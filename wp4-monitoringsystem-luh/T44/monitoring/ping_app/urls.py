from django.urls import include, path

urlpatterns = [
    path('ping', include('ping_app.urls')),
    path('', include('ping_app.urls')),

]
