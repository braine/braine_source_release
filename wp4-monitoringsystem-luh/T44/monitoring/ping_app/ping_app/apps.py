from django.apps import AppConfig

class PingAppConfig(AppConfig):
    name = 'ping_app'
