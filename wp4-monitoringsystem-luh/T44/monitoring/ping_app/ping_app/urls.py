from django.urls import path, include
from ping_app import views

urlpatterns = [
    path('ping', views.ping),
    path('', views.ping),
    path('counter', views.ping_counter),
    path('reset', views.reset)

]