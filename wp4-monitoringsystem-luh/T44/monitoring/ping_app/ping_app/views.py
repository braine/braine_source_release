from django.http import HttpResponse
import time
import os
import yaml

counter = None
configs = yaml.load(open('configs.yaml', 'r'), Loader=yaml.FullLoader)

ping_log_location = configs['pings_log_filename']


# A single ping request.  Save the ping time stamp and number of pings in a log file
def ping(request):
    global counter  # using a global counter is not the best method here.  Look into django model files
    if os.path.isfile(ping_log_location) and os.path.getsize(ping_log_location) != 0:
        if counter is None:
            print('re-loading counter value')
            counter = get_latest_counter()
    else:
        # Create the file
        f = open(ping_log_location, 'w')
        f.close()
        counter = 0

    counter += 1
    with open(ping_log_location, 'a') as f:
        f.write('%i, %s\n' % (counter, time.time()))
    return HttpResponse('pong')


# Would be better to use a Django model and read from that, or use a tmp file.
def ping_counter(request):
    return HttpResponse('ping counter is %i' % get_latest_counter())


# Erase the log file
def reset(request):
    with open(ping_log_location, 'w') as f:  # The mode is r+ instead of r
        f.write('')
    return HttpResponse('Cleared the ping logs')


def get_latest_counter():
    # Assumes newlines are initiated with \n characters
    with open(ping_log_location, 'rb') as f:
        seek_position = 1

        # Loop over the file until it discovers the new line character
        while True:
            f.seek(seek_position, 2)
            end_line = f.read()

            # Skip the last character incase it's a new line
            if '\n' in end_line.decode()[:-1]:
                break
            else:
                seek_position -= 1

    png_counter = end_line.decode().strip('\n').split(',')[0]
    return int(png_counter)
