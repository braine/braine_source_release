#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

import yaml

configs = yaml.load(open('configs.yaml', 'r'), Loader=yaml.FullLoader)
ping_log_location = configs['pings_log_filename']

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ping_app.settings')

    # Create the log files at the start of the application
    if not os.path.isfile(ping_log_location):
        f = open(ping_log_location, 'w')
        f.close()

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
