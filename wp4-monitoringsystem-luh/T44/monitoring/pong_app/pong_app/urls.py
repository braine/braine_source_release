from django.urls import path, include
from django.conf.urls import url
from pong_app import views

urlpatterns = [
    path('pong', views.pong),
    path('', views.pong),
    path('reset', views.reset),

]