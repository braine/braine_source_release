from django.http import HttpResponse
import requests
import time
import yaml

configs = yaml.load(open('configs.yaml', 'r'), Loader=yaml.FullLoader)


# Append the RTT value and timestep to the RTT log file
# Writes RTT, post_pong, and writetime.  Could potentially remove writetime
def write_RTT_to_file(out_file, rtt, pst_pong_time):
    with open(out_file, 'a') as f:  # The mode is r+ instead of r
        f.write('%.6f, %s , %s\n' % (rtt, pst_pong_time, time.time()))


# Call the ping app, and get the "pong" response. Calculate the rount trip time RTT
def pong(request):
    pre_pong = time.time()
    ping_response = requests.get(configs['ping_location'])
    post_pong = time.time()
    RTT = post_pong - pre_pong
    write_RTT_to_file(configs['pongs_log_filename'], RTT, post_pong)

    return HttpResponse('RTT: %s seconds' % RTT)


# Clear the RTT log file
def reset(request):
    with open(configs['pongs_log_filename'], 'w') as f:  # The mode is r+ instead of r
        f.write('')
    return HttpResponse('Cleared the RTT logs')
