#!/usr/bin/env python
import requests
import time
import yaml


def pongs(configs, pongs_per_second, run_time, debug=False):
    counter = 0  # For debugging, can be removed later

    pongs_per_second = int(pongs_per_second)
    run_time = float(run_time)
    end_runtime = time.time() + run_time
    while time.time() < end_runtime:
        pre_call = time.time()

        pong_response = requests.get(configs['pong_location'])

        post_call = time.time()

        ping_pong_RTT = post_call - pre_call

        time_to_wait = (1/pongs_per_second) - ping_pong_RTT
        if time_to_wait > 0:
            time.sleep(time_to_wait)

        if debug:
            counter += 1
            print('counter / RTT:\t %i / %.6f' % (counter, ping_pong_RTT))

# Append the RTT value and timestep to the RTT log file
def write_RTT_to_file(out_file, rtt, post_pong, pre_pong):
    with open(out_file, 'a') as f:  # The mode is r+ instead of r
        f.write('%.6f, %s , %s\n' % (rtt, post_pong, pre_pong))



if __name__ == "__main__":
    configs = yaml.load(open('configs.yaml', 'r'), Loader=yaml.FullLoader)


    pongs(configs, configs['requests_per_second'], configs['runtime_seconds'], True)

    print('Completed %s pongs per second for %s seconds' % (configs['requests_per_second'], configs['runtime_seconds']))

