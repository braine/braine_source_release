# FFB

# Forecasting Functional Block (FBB)
This repository contains the code for the Forecasting Functional Block adopted in Braine project.

The scope of this block is to perform the forecasting of specific parameter(s).

This project is based on Python3.8 interpreter.

## Requirements
All the python project dependencies are stored in the file requirements38.txt and so you can install them by running the following command:

    pip install -r requirements38.txt
    
## Installation
Copy the projet folder in the dedicate machine.

Edit the configuration file of the project.

## Usage
 
Run the command `python3 5grfbbAPI.py`.
The project actvates a flask rest server, listening by default on port 8888.

The main APIs are described following:
### create a new job
POST http://127.0.0.1:8888/Forecasting

   
    { 
      "nsId" : "fgt-82f4710-3d04-429a-8243-5a2ac741fd4d",
      "vnfdId" : "spr2",
      "performanceMetric" :  "VcpuUsageMean",
      "nsdId" : nsEVS_aiml,
      "IL" : 1
    }

### Update the current IL
PUT http://127.0.0.1:8888/Forecasting?job_id=job&IL=x

### Get list of active jobs
GET http://127.0.0.1:8888/Forecasting
### Get details of job_id job
GET http://127.0.0.1:8888/Forecasting?job_id=job

### Stop and exeisting job
DELETE http://127.0.0.1:8888/Forecasting?job_id=job

## Additional info
The project includes an already trained lstm model (resulting from I10 activities), based on the tensorflow keras library, that is integrated in the system, under trainedModels with name lstm11.h5.

Under tools there is a kafka producer able to emulare the behaviour of a prometheus scraper job (for pushing data on a specific Kafka topic)

 
