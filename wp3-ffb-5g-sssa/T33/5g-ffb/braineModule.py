from tools.Classes import ForecastingJob
from tools.externalConnections import ExternalConnections
from threading import Thread, Event
from confluent_kafka import KafkaError, KafkaException
from confluent_kafka.error import ConsumeError
import sys
import configparser
import logging
import json


conffile = 'configBraine.conf'

# logging configuration
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG, filename='braine.log')
log = logging.getLogger("BraineForecasting")



class braineModule:
    def __init__(self, load, save, steps_back, steps_forw, features, main_feat, in_t, out_t):
        self.ec = ExternalConnections(conffile)
        self.producer = ec.createKafkaProducer()
        self.il = 1

        self.intopic = in_t
        self.outtopic = out_t

        self.ec.createKafkaTopic_free(self.intopic)
        self.ec.createKafkaTopic_free(self.outtopic)

        self.fj = ForecastingJob("BraineNS", "UEdata", "lstmCPUEnhanced", "node_cpu_seconds_total",
                                 self.il, "flexRAN-forecaster", outTopic=self.outtopic, output=self.producer)
        if load:
            # input model file to be loaded (no train)
            #loadfile = 'trainedModels/lstmdiff' + str(steps_back) + '_' + str(steps_forw) + '.h5'
            loadfile = 'trainedModels/newLSTM.h5'
        else:
            #dataset to be used for training in case load is False
            loadfile = 'data/dataset_train.csv'
        #output model file
        savefile = 'trainedModels/newLSTM.h5'
        self.features = features
        #self.features = ['avg_rtt_a1', 'avg_rtt_a2', 'avg_loss_a1', 'avg_loss_a2', 'r_a1', 'r_a2']
        self.main_feature = main_feat
        #self.main_feature = 'cpu0'
        self.fj.set_model(steps_back, steps_forw, load, loadfile, save, features=self.features,
                          m_feature=self.main_feature, savefilename=savefile)

        self.data = {}
        for metric in self.features:
            self.data[metric] = []
        self.data[self.main_feature] = []

        self.event = None
        self.t = None
        log.info('module initiated')

    def init_data(self, a, num=10):
        '''
        data['cpu0'] = [0.0, 15.82, 29.78, 43.78, 57.73, 71.73, 85.73, 99.72, 113.7, 127.72]
        data['avg_rtt_a1'] =  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        data['avg_rtt_a2'] =  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        data['avg_loss_a1'] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        data['avg_loss_a2'] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        data['#robots'] =     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        data['r_a1'] =        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        data['r_a2'] =        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        data['cpu0'] =[]
        data['avg_rtt_a1'] = []
        data['avg_rtt_a2'] = []
        data['avg_loss_a1'] = []
        data['avg_loss_a2'] = []
        data['#robots'] = []
        data['r_a1'] = []
        data['r_a2'] = []
        '''
        for row in range(0, num):
            line = a[row].split(';')
            self.data['cpu0'].append(round(float(line[0]), 1))
            self.data['avg_rtt_a1'].append(round(float(line[2]), 1))
            self.data['avg_rtt_a2'].append(round(float(line[3]), 1))
            self.data['avg_loss_a1'].append(round(float(line[6]), 1))
            self.data['avg_loss_a2'].append(round(float(line[7]), 1))
            #self.data['#robots'].append(round(float(line[8]), 1))
            self.data['r_a1'].append(round(float(line[9]), 1))
            self.data['r_a2'].append(round(float(line[10]), 1))

        self.fj.set_data(self.data)
        self.fj.config_t0(self.main_feature, self.data[self.main_feature][0])

        log.info('Initial data loaded')
        print(self.fj.data)

    def add_data(self, val=10):
        for row in range(val, len(a)):
            line = a[row].split(';')
            self.data['cpu0'].append(round(float(line[0]), 1))
            self.data['avg_rtt_a1'].append(round(float(line[2]), 1))
            self.data['avg_rtt_a2'].append(round(float(line[3]), 1))
            self.data['avg_loss_a1'].append(round(float(line[6]), 1))
            self.data['avg_loss_a2'].append(round(float(line[7]), 1))
            #self.data['#robots'].append(round(float(line[8]), 1))
            self.data['r_a1'].append(round(float(line[9]), 1))
            self.data['r_a2'].append(round(float(line[10]), 1))

    def start_job(self):
        self.event = Event()
        self.t = Thread(target=self.fj.run, args=(self.event, ec.createKafkaConsumer(self.il, self.intopic), False))
        self.t.start()
        log.info('Job started')

    def test_forecasting(self):
        log.info('Testing forecasting data')

        consumer = ec.createKafkaConsumer(self.il, self.outtopic)
        while True:
            try:
                msg = consumer.poll(1.0)
                if msg is None:
                    continue
                if msg.error():
                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        # End of partition event
                        log.error('%% %s [%d] end at offset %d\n' % (msg.topic(),msg.partition(), msg.offset()))
                    elif msg.error():
                        raise KafkaException(msg.error())
                else:
                    #sys.stderr.write(json.load(msg.value()))
                    loaded_json = json.loads(msg.value())
                    print(json.dumps(loaded_json, indent=4, sort_keys=True))
            except ConsumeError as e:
                log.error("Consumer error: {}".format(str(e)))
                # Should be commits manually handled?
                consumer.close()
            except KeyboardInterrupt:
                self.event.set()
                self.t.join()
                ec.deleteKafkaTopic(self.intopic)
                ec.deleteKafkaTopic(self.outopic)
                break
        log.info("Leaving the testing loop")


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(conffile)
    log.info('Configuration file parsed and read')
    load = True
    save = False
    steps_back = 10
    steps_forw = 4
    features = ['avg_rtt_a1', 'avg_rtt_a2', 'avg_loss_a1', 'avg_loss_a2', 'r_a1', 'r_a2']
    main_feat = "cpu0"
    # ec = ExternalConnections('config.conf', logging)
    ec = ExternalConnections('config.conf')
    log.debug('Forecasting API: External connection module initialized')
    if 'local' in config:
        in_t = config['local']['input_topic']
        out_t = config['local']['output_topic']

    else:
        in_t = "input_topic"
        out_t = "output_topic"
    log.debug('loaded input topic: {} and output topic. {}'.format(in_t, out_t))

    bm = braineModule(load, save, steps_back, steps_forw, features, main_feat, in_t, out_t)
    a = []
    a.append("1309420.95;217.7;0.0;217.7;1.18;0.0;0.0;0.0;1;0;1")
    a.append("1309434.87;216.8;0.0;216.8;1.37;0.0;0.0;0.0;1;0;1")
    a.append("1309448.79;215.5;0.0;215.5;1.27;0.0;0.0;0.0;1;0;1")
    a.append("1309462.78;215.9;0.0;215.9;1.44;0.0;0.0;0.0;1;0;1")
    a.append("1309476.77;216.4;0.0;216.4;1.31;0.0;0.0;0.0;1;0;1")
    a.append("1309490.74;218.4;0.0;218.4;1.47;0.0;0.0;0.0;1;0;1")
    a.append("1309504.68;216.7;0.0;216.7;1.36;0.0;0.0;0.0;1;0;1")
    a.append("1309518.64;215.5;0.0;215.5;1.29;0.0;0.0;0.0;1;0;1")
    a.append("1309532.57;214.7;0.0;214.7;1.32;0.0;0.0;0.0;1;0;1")
    a.append("1309546.51;214.5;0.0;214.5;1.26;0.0;0.0;0.0;1;0;1")
    a.append("1309560.47;216.8;0.0;216.8;1.30;0.0;0.0;0.0;1;0;1")
    a.append("1309574.43;214.3;0.0;214.3;1.39;0.0;0.0;0.0;1;0;1")
    a.append("1309588.40;216.2;0.0;216.2;1.25;0.0;0.0;0.0;1;0;1")
    a.append("1309602.36;216.3;0.0;216.3;1.34;0.0;0.0;0.0;1;0;1")
    bm.init_data(a, 10)
    bm.start_job()

    bm.test_forecasting()




