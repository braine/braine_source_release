#!/bin/bash -e

# Test variables - Set from K8s manifests.
# export ZOOKEEPER_REPLICAS=3
# export ZOOKEEPER_POD_NAME="zoo"
# export ZOOKEEPER_SERVICE_NAME="zoo-svc"

export KAFKA_BROKER_ID=$(( $(hostname -f | cut -d '-' -f2 | cut -d '.' -f1) + 1 ))

if [[ -z "$NODE_IP" ]]; then
   echo "INFO: NODE_IP not specified. Needed for external access.";
   exit 1; fi

if [[ -z "$FIRST_ADVERTISED_PORT" ]]; then
   echo "INFO: FIRST_ADVERTISED_PORT not specified. Needed for external access.";
   exit 1;
fi

if [[ -z "$KAFKA_PORT" ]]; then
   echo "INFO: KAFKA_PORT not specified. Assuming 9092.";
   export KAFKA_PORT=9092; fi

export KAFKA_ADVERTISED_PORT=$(( $FIRST_ADVERTISED_PORT + $KAFKA_BROKER_ID ));
if [[ -z "$KAFKA_LISTENER_SECURITY_PROTOCOL" ]]; then
   echo "INFO: KAFKA_LISTENER_SECURITY_PROTOCOL not specified. Using PLAINTEXT";
   export KAFKA_LISTENER_SECURITY_PROTOCOL="PLAINTEXT";
fi

LISTENERS="INTERNAL://:$KAFKA_PORT,EXTERNAL://:9094"
ADVERTISED_LISTENERS="INTERNAL://$(hostname -f):$KAFKA_PORT,EXTERNAL://$NODE_IP:$KAFKA_ADVERTISED_PORT"

export KAFKA_LISTENER_SECURITY_PROTOCOL_MAP="INTERNAL:$KAFKA_LISTENER_SECURITY_PROTOCOL,EXTERNAL:$KAFKA_LISTENER_SECURITY_PROTOCOL"
export KAFKA_INTER_BROKER_LISTENER_NAME=INTERNAL
export KAFKA_LISTENERS=$LISTENERS
export KAFKA_ADVERTISED_LISTENERS=$ADVERTISED_LISTENERS

unset KAFKA_PORT; unset FIRST_ADVERTISED_PORT unset KAFKA_ADVERTISED_PORT; unset KAFKA_LISTENER_SECURITY_PROTOCOL;

if [[ -z "$ZOOKEEPER_PORT" ]]; then echo "INFO: Zookeeper port not specified. Assuming 2181."; ZOOKEEPER_PORT=2181; fi

if [[ -z "$KAFKA_ZOOKEEPER_CONNECT" ]];
   then
      if [ -z "$ZOOKEEPER_REPLICAS" ] || [ -z "$ZOOKEEPER_POD_NAME" ] || [[ -z "$ZOOKEEPER_SERVICE_NAME" ]];
        then
           echo "ERROR: Cannot compute zookeeper connect. Define env variables ZOOKEEPER_REPLICAS,POD_NAME,SERVICE_NAME"
           exit 1;
        else
          ZK_DOMAIN_NAME=$ZOOKEEPER_SERVICE_NAME.$(hostname -f | cut -d '.' -f3-)
          KAFKA_ZOOKEEPER_CONNECT=""
          for N in $(seq $ZOOKEEPER_REPLICAS)
          do
             KAFKA_ZOOKEEPER_CONNECT+="$ZOOKEEPER_POD_NAME-$(( $N - 1 )).$ZK_DOMAIN_NAME:$ZOOKEEPER_PORT,"
          done
          export KAFKA_ZOOKEEPER_CONNECT="${KAFKA_ZOOKEEPER_CONNECT::-1}"
      fi
fi

if [[ -n "$KAFKA_HEAP_OPTS" ]]; then
    sed -r -i 's/(export KAFKA_HEAP_OPTS)="(.*)"/\1="'"$KAFKA_HEAP_OPTS"'"/g' "$KAFKA_HOME/bin/kafka-server-start.sh"
    unset KAFKA_HEAP_OPTS
fi

#Issue newline to config file in case there is not one already
echo "" >> "$KAFKA_HOME/config/server.properties"

(
    function updateConfig() {
        key=$1
        value=$2
        file=$3

        # Omit $value here, in case there is sensitive information
        echo "[Configuring] '$key' '$value' in '$file'"

        # If config exists in file, replace it. Otherwise, append to file.
        if grep -E -q "^#?$key=" "$file"; then
            sed -r -i "s@^#?$key=.*@$key=$value@g" "$file" #note that no config values may contain an '@' char
        else
            echo "$key=$value" >> "$file"
        fi
    }

    # Fixes #312
    # KAFKA_VERSION + KAFKA_HOME + grep -rohe KAFKA[A-Z0-0_]* /opt/kafka/bin | sort | uniq | tr '\n' '|'
    EXCLUSIONS="|KAFKA_HOME|KAFKA_DEBUG|KAFKA_GC_LOG_OPTS|KAFKA_HEAP_OPTS|KAFKA_JMX_OPTS|KAFKA_JVM_PERFORMANCE_OPTS|KAFKA_LOG|KAFKA_OPTS|"

    # Read in env as a new-line separated array. This handles the case of env variables have spaces and/or carriage returns. See #313
    IFS=$'\n'
    for VAR in $(env)
    do
        env_var=$(echo "$VAR" | cut -d= -f1)
        if [[ "$EXCLUSIONS" = *"|$env_var|"* ]]; then echo "Excluding $env_var from broker config"; continue; fi

        if [[ $env_var =~ ^KAFKA_ ]]; then
            kafka_name=$(echo "$env_var" | cut -d_ -f2- | tr '[:upper:]' '[:lower:]' | tr _ .)
            updateConfig "$kafka_name" "${!env_var}" "$KAFKA_HOME/config/server.properties"
        fi

        if [[ $env_var =~ ^LOG4J_ ]]; then
            log4j_name=$(echo "$env_var" | tr '[:upper:]' '[:lower:]' | tr _ .)
            updateConfig "$log4j_name" "${!env_var}" "$KAFKA_HOME/config/log4j.properties"
        fi
    done

	while read p; do
	   updateConfig "$(echo $p | cut -d '=' -f0)" "$(echo $p | cut -d '=' -f2)" "$KAFKA_HOME/config/server.properties"
	done < $KAFKA_HOME/config/server.properties.extras
)

if [[ -n "$CUSTOM_INIT_SCRIPT" ]] ; then eval "$CUSTOM_INIT_SCRIPT"; fi

exec "$KAFKA_HOME/bin/kafka-server-start.sh" "$KAFKA_HOME/config/server.properties"
