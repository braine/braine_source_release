The Dockerfile:
   uses alpine as base
   installs java8
   installs zookeeper in /opt/zookeeper/
   calls start-zk.sh for configuration and initialization

The start-zk.sh script:
   Copies config files from sample_zoo.cfg which:
      may contain default sample provided by zookeeper or
      be overwritten by ConfigMap in Kubernetes YAML files
   Creates myid file in data directory
   Reads environment variables to get other zookeeper server info:
      number of repliacs that will be created
      Kubernetes pod, service and namespace values (defaults are set to avoid script failure)
   Edits config file to remove any existing zookeeper servers and add the computed ones
   Starts the zookeeper server in foreground
