#!/bin/bash -e

# Create zoo.cfg from zoo_sample.cfg.
confFile=$ZOOKEEPER_HOME/conf/zoo.cfg
cp /opt/zookeeper/conf/zoo_sample.cfg $confFile
#export ZOOKEEPER_REPLICAS=1

# Create dataDir
dataDir=$(sed -n -e '/^dataDir/p' $confFile | cut -d'=' -f2)
mkdir -p $dataDir

#FQDN: pod-N.svc-name.namespace.svc.cluster.local
POD_NAME=$(hostname -f | cut -d '-' -f1)
echo $(( $(hostname -f | cut -d '-' -f2 | cut -d '.' -f1) + 1 )) > "${dataDir}myid"
DOMAIN_NAME=$(hostname -f | cut -d '.' -f2-)

# Get required variables from environment
if [[ -z "$ZOOKEEPER_REPLICAS" ]]; then echo "ERROR: ZOOKEEPER_REPLICAS missing in ENV. Exiting"; exit 1; fi

#Remove any existing server. info from config file
sed -i '/server./d' $confFile
#Add info on other zookeeper replicas to config file
for N in $(seq $ZOOKEEPER_REPLICAS)
do
echo "server.$N=$POD_NAME-$(( $N - 1 )).$DOMAIN_NAME:2888:3888" >> $confFile
done

#Start zookeeper server
exec "$ZOOKEEPER_HOME/bin/zkServer.sh" "start-foreground"
