#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
#include <librdkafka/rdkafka.h>
#include <curl/curl.h>
#include <json-c/json.h>

#define FLUSH_TIMEOUT (30 * 1000) /* 30 s */

int keep_running = 1;
const char auth[]  = "Authorization: Bearer ";

struct string { size_t len; char *text; };
void init_string(struct string *s) { s->len = 0; s->text = malloc(s->len+1); s->text[0] = '\0'; }
typedef struct { char *api_url; char *api_token; double producer_interval; }k8s_conf_struct;
static void handle_ctrlc (int sig) { fprintf(stderr, "Terminating...\n"); keep_running = 0;}

int msleep(long sec){
    struct timespec ts; int ret;
    if (sec < 0){ errno = EINVAL; return -1; }
    ts.tv_sec = sec / 1; ts.tv_nsec = (sec % 1) * 1000 * 1000 * 1000;
    do { ret = nanosleep(&ts, &ts); } while (ret && errno == EINTR);
    return ret;
}

rd_kafka_conf_t *read_broker_config (const char *config_file) {
    FILE *fp; char errstr[256]; char buf[1024]; int line = 0;

    if (!(fp = fopen(config_file, "r"))) {
        fprintf(stderr, "Failed to open %s: %s\n", config_file, strerror(errno));
        return NULL;
    }

    rd_kafka_conf_t *conf = rd_kafka_conf_new();
    /* Read configuration file, line by line. */
    while (fgets(buf, sizeof(buf), fp)) {
        char *s = buf; char *t; char *key, *value;
        line++;
        /* Left-trim */
        while (isspace(*s)) s++;

        /* Right-trim */
        t = s + strlen(s) - 1;
        while (t >= s && isspace(*t)) t--;
        *(t+1) = '\0';

        /* Ignore empty and comment lines */
        if (!*s || *s == '#') continue;

        /* Expected format: "key=value".
         * Find "=" and split line up into key and value. */
        if (!(t = strchr(s, '=')) || t == s) {
            fprintf(stderr,"%s:%d: invalid syntax: expected key=value\n", config_file, line);
            fclose(fp);
            rd_kafka_conf_destroy(conf);
            return NULL;
        }

        key = s; *t = '\0'; value = t+1;

        /* Set configuration value in config object. */
        if (rd_kafka_conf_set(conf, key, value, errstr, sizeof(errstr)) != RD_KAFKA_CONF_OK) {
            fprintf(stderr,"%s: %d: %s\n", config_file, line, errstr);
            rd_kafka_conf_destroy(conf); fclose(fp); return NULL;
        }
    }
    fclose(fp);
    return conf;
}

k8s_conf_struct read_k8s_config(const char *config_file){
    FILE *fp; char errstr[256]; char buf[1024]; int line = 0;
    k8s_conf_struct configs;
    configs.producer_interval = 10.0;

    if (!(fp = fopen(config_file, "r"))) {
        fprintf(stderr, "Failed to open %s: %s\n", config_file, strerror(errno));
        return configs;
    }

    while (fgets(buf, sizeof(buf), fp)) {
        char *s = buf; char *t; char *key, *value;
        line++;
        /* Left-trim */
        while (isspace(*s)) s++;

        /* Right-trim */
        t = s + strlen(s) - 1;
        while (t >= s && isspace(*t)) t--;
        *(t+1) = '\0';

        /* Ignore empty and comment lines */
        if (!*s || *s == '#') continue;

        /* Expected format: "key=value".
         * Find "=" and split line up into key and value. */
        if (!(t = strchr(s, '=')) || t == s) {
            fprintf(stderr,"%s:%d: invalid syntax: expected key=value\n", config_file, line);
            fclose(fp); return configs;
        }

        key = s; *t = '\0'; value = t+1;
        char * copy = malloc(strlen(value) + 1);
        strcpy(copy, value);

        if (strcmp(key, "api_url") == 0)  configs.api_url = copy;
        else if (strcmp(key, "api_token") == 0) configs.api_token = copy;
        else if (strcmp(key, "producer_interval") == 0) configs.producer_interval = strtod(copy,NULL);
        else fprintf(stderr,"Undefined configuration (%s:%s)\n", key,value);
       }

    fclose(fp);
    return configs;

}

size_t get_curl_data(void *text, size_t size, size_t nmemb, struct string *data){
  size_t new_len = data->len + size*nmemb;
  data->text = realloc(data->text, new_len+1);
  if (data->text == NULL)
    fprintf(stderr, "String realloc() failed when receiving curl response.\n");
  else {
    memcpy(data->text + data->len, text, size*nmemb);
    data->text[new_len] = '\0'; data->len = new_len;
  }
  return size*nmemb;
}

static void producer_ack (rd_kafka_t *producer, const rd_kafka_message_t *rkmessage, void *opaque) {
        if (rkmessage->err) {
                fprintf(stderr, "Delivery failed for topic %s: %s\n",
                        rd_kafka_topic_name(rkmessage->rkt), rd_kafka_err2str(rkmessage->err));
        } else {
            fprintf(stderr,"Message delivered to topic %s: %s\n",
                    rd_kafka_topic_name(rkmessage->rkt), (const char *)rkmessage->payload);
        }
}

static int produce_message (char *topic, char *message, rd_kafka_t *producer) {
        rd_kafka_resp_err_t err;
        err = rd_kafka_producev(producer, RD_KAFKA_V_TOPIC(topic), RD_KAFKA_V_VALUE(message, strlen(message)),
                                RD_KAFKA_V_MSGFLAGS(RD_KAFKA_MSG_F_COPY), RD_KAFKA_V_END);
        if (err) { fprintf(stderr, "Producing failed: %s\n", rd_kafka_err2str(err)); return 1;}
        rd_kafka_poll(producer, 0);
        return 0;
}

static int process_response(char *response, rd_kafka_t *producer){

    struct json_object *parsed_json, *items, *node, *name, *metadata, *usage;
    size_t n_nodes, i; int ret;

    //char *str1="{\"metadata\": { \"name\": \"";
    //char *str2="\" }, \"usage\": ";
    //char *str3=" }";
    char *topic, *message; parsed_json = json_tokener_parse(response);
    json_object_object_get_ex(parsed_json, "items", &items); n_nodes = json_object_array_length(items);

    for ( i = 0; i < n_nodes ; i++ ){
        node = json_object_array_get_idx(items, i);
        json_object_object_get_ex(node, "metadata", &metadata); json_object_object_get_ex(metadata, "name", &name);
        json_object_object_get_ex(node, "usage", &usage);
        topic = (char *) json_object_get_string(name); message = (char *) json_object_get_string(usage);
        //size_t length = strlen(str1) + strlen(name_str) + strlen(str2) + strlen(usage_str) + strlen(str3);
        //char *message = malloc(sizeof(char) * length);
        //snprintf(message, length, "%s%s%s%s%s", str1, name_str, str2, usage_str, str3);
        //printf("Message prepared for %s:%s\n", topic,message);
        ret = produce_message(topic,message,producer);
        }
        rd_kafka_flush(producer,FLUSH_TIMEOUT);
    return ret;
}

int main (int argc, char **argv) {

    /* Set up signal handlers for termination */
    signal(SIGINT, handle_ctrlc); signal(SIGTERM, handle_ctrlc);

    char api_token[500];
    const char *broker_config_file, *k8s_config_file; char errstr[512];
    int i;
    if (argc != 3) {fprintf(stderr, "Usage: %s <broker.config> <k8s.config>\n", argv[0]); exit(1);}
    broker_config_file = argv[1]; k8s_config_file = argv[2];

    rd_kafka_conf_t *broker_conf; rd_kafka_t *producer;
    if (!(broker_conf = read_broker_config(broker_config_file))) {fprintf(stderr,"Broker Config Error.\n");return 1;}
    rd_kafka_conf_set_dr_msg_cb(broker_conf, producer_ack);
    producer = rd_kafka_new(RD_KAFKA_PRODUCER, broker_conf, errstr, sizeof(errstr));
    if (!producer) {fprintf(stderr, "Producer Failed: %s\n", errstr); rd_kafka_conf_destroy(broker_conf); return -1;}
    k8s_conf_struct k8s_conf = read_k8s_config(k8s_config_file);
    if (k8s_conf.api_url == NULL) {fprintf(stderr,"Kubernetes Config Error.\n"); return 1;}
    strcpy(api_token,auth);strcat(api_token,k8s_conf.api_token);

    CURLcode status; CURL *curl = curl_easy_init();
    struct string response;

    struct curl_slist *chunk = NULL; chunk = curl_slist_append(chunk, api_token);
    curl_easy_setopt(curl, CURLOPT_URL, k8s_conf.api_url); curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_curl_data); curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L); curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

    char time_string[20];
    struct tm * time_info;
    while (keep_running == 1){
        time_t begin; time(&begin); time_info = localtime(&begin);
        init_string(&response); status = curl_easy_perform(curl);

        strftime(time_string, 26, "%Y-%b-%d %H:%M:%S", time_info); fprintf(stderr, "Time: %s\n", time_string);

        if( status != CURLE_OK )
            fprintf(stderr, "Curl request failed: %s\n", curl_easy_strerror(status));
        else if (process_response(response.text, producer) == 1)
            fprintf(stderr, "Processing curl response failed.\n");
        else
            fprintf(stderr, "-------------------------------.\n");

        time_t end; time(&end);
        msleep(k8s_conf.producer_interval - difftime(end, begin));
    }

    curl_slist_free_all(chunk); curl_easy_cleanup(curl);
    rd_kafka_destroy(producer);
   return 0;
   }
