#define POINTS 1024
#define CP 1104
#define ONE_OVER_SQRT2_Q15 23170

constant short tw16rep[24]  = {  32767, 0, 30272, -12540, 23169 , -23170, 12539 , -30273,
								32767, 0, 23169, -23170, 0     , -32767, -23170, -23170,
						        32767, 0, 12539, -30273, -23170, -23170, -30273, 12539};

constant short tw16crep[24] = { 0, 32767, 12540, 30272, 23170, 23169 , 30273 , 12539,
						        0, 32767, 23170, 23169, 32767, 0     , 23170 , -23170,
						        0, 32767, 30273, 12539, 23170, -23170, -12539, -30273};

constant short tw64rep[96] = {	32767 ,0      ,32609 ,-3212 ,32137 ,-6393 , 31356,-9512 ,30272 ,-12540,28897 ,-15447,27244 ,-18205,25329 ,-20788, 
								23169 ,-23170 , 20787,-25330,18204 ,-27245, 15446,-28898,12539 ,-30273,9511  ,-31357,6392  ,-32138,3211  , -32610,
								32767 ,0      ,32137 ,-6393 ,30272 ,-12540,27244 ,-18205,23169 ,-23170,18204 ,-27245,12539 ,-30273,6392  ,-32138, 
								0     ,-32767 ,-6393 ,-32138,-12540,-30273,-18205,-27245,-23170,-23170,-27245,-18205,-30273,-12540,-32138,-6393,
								32767 ,0      ,31356 ,-9512 ,27244 ,-18205,20787 ,-25330,12539 ,-30273,3211  ,-32610,-6393 ,-32138,-15447,-28898,
								-23170, -23170,-28898,-15447,-32138,-6393 ,-32610, 3211 ,-30273, 12539,-25330, 20787,-18205, 27244,-9512 ,  31356};

constant short tw64crep[96] = {	0    ,32767 ,3212 ,32609 ,6393 ,32137 ,9512 ,31356 ,12540 ,30272 ,15447 ,28897 ,18205 ,27244 ,20788 ,25329, 
								23170,23169 ,25330,20787 ,27245,18204 ,28898,15446 ,30273 ,12539 ,31357 ,9511  ,32138 , 6392 ,32610 ,3211,
								0    ,32767 ,6393 ,32137 ,12540,30272 ,18205,27244 ,23170 ,23169 ,27245 ,18204 ,30273 ,12539 ,32138 ,6392,  
								32767,0     ,32138,-6393 ,30273,-12540,27245,-18205,23170 ,-23170,18205 ,-27245,12540 ,-30273,6393  ,-32138,
								0    ,32767 ,9512 ,31356 ,18205,27244 ,25330,20787 ,30273 ,12539 ,32610 ,3211  ,32138 ,-6393 ,28898 ,-15447, 
								23170,-23170,15447,-28898,6393 ,-32138,-3211,-32610,-12539,-30273,-20787,-25330,-27244,-18205,-31356,-9512};

constant short tw256[384]   = 	{32767 ,0     ,32757 ,-805  ,32727 ,-1608 ,32678 ,-2411 ,32609 ,-3212 ,32520 ,-4012 ,32412 ,-4808 ,32284 ,-5602 ,
								  32137 ,-6393 ,31970 ,-7180 ,31785 ,-7962 ,31580 ,-8740 ,31356 ,-9512 ,31113 ,-10279,30851 ,-11039,30571 ,-11793,
								 30272 ,-12540,29955 ,-13279,29621 ,-14010,29268 ,-14733,28897 ,-15447,28510 ,-16151,28105 ,-16846,27683 ,-17531,
								  27244 ,-18205,26789 ,-18868,26318 ,-19520,25831 ,-20160,25329 ,-20788,24811 ,-21403,24278 ,-22005,23731 ,-22595,
								 23169 ,-23170,22594 ,-23732,22004 ,-24279,21402 ,-24812,20787 ,-25330,20159 ,-25832,19519 ,-26319,18867 ,-26790,
								  18204 ,-27245,17530 ,-27684,16845 ,-28106,16150 ,-28511,15446 ,-28898,14732 ,-29269,14009 ,-29622,13278 ,-29956,
								 12539 ,-30273,11792 ,-30572,11038 ,-30852,10278 ,-31114,9511  ,-31357,8739  ,-31581,7961  ,-31786,7179  ,-31971,
								  6392  ,-32138,5601  ,-32285,4807  ,-32413,4011  ,-32521,3211  ,-32610,2410  ,-32679,1607  ,-32728,804   ,-32758,
                                 32767 ,     0,32727 ,-1608 ,32609 ,-3212 ,32412 ,-4808 ,32137 ,-6393 ,31785 ,-7962 ,31356 ,-9512 ,30851 ,-11039,
                                  30272 ,-12540,29621 ,-14010,28897 ,-15447,28105 ,-16846,27244 ,-18205,26318 ,-19520,25329 ,-20788,24278 ,-22005,
                                 23169 ,-23170,22004 ,-24279,20787 ,-25330,19519 ,-26319,18204 ,-27245,16845 ,-28106,15446 ,-28898,14009 ,-29622,
                                  12539 ,-30273,11038 ,-30852,9511  ,-31357,7961  ,-31786,6392  ,-32138,4807  ,-32413,3211  ,-32610,1607  ,-32728,
                                 0     ,-32767,-1608 ,-32728,-3212 ,-32610,-4808 ,-32413,-6393 ,-32138,-7962 ,-31786,-9512 ,-31357,-11039,-30852,
                                  -12540,-30273,-14010,-29622,-15447,-28898,-16846,-28106,-18205,-27245,-19520,-26319,-20788,-25330,-22005,-24279,
                                 -23170,-23170,-24279,-22005,-25330,-20788,-26319,-19520,-27245,-18205,-28106,-16846,-28898,-15447,-29622,-14010,
                                  -30273,-12540,-30852,-11039,-31357,-9512 ,-31786,-7962 ,-32138,-6393 ,-32413,-4808 ,-32610,-3212 ,-32728,-1608 ,
                                 32767 ,0     ,32678 ,-2411 ,32412 ,-4808 ,31970 ,-7180 ,31356 ,-9512 ,30571 ,-11793,29621 ,-14010,28510 ,-16151,
                                  27244 ,-18205,25831 ,-20160,24278 ,-22005,22594 ,-23732,20787 ,-25330,18867 ,-26790,16845 ,-28106,14732 ,-29269,
                                 12539 ,-30273,10278 ,-31114,7961  ,-31786,5601  ,-32285,3211  ,-32610,804   ,-32758,-1608 ,-32728,-4012 ,-32521,
                                  -6393 ,-32138,-8740 ,-31581,-11039,-30852,-13279,-29956,-15447,-28898,-17531,-27684,-19520,-26319,-21403,-24812,
                                 -23170,-23170,-24812,-21403,-26319,-19520,-27684,-17531,-28898,-15447,-29956,-13279,-30852,-11039,-31581,-8740 ,
                                  -32138,-6393 ,-32521,-4012 ,-32728,-1608 ,-32758,804   ,-32610,3211  ,-32285,5601  ,-31786,7961  ,-31114,10278 ,
                                 -30273,12539 ,-29269,14732 ,-28106,16845 ,-26790,18867 ,-25330,20787 ,-23732,22594 ,-22005,24278 ,-20160,25831 ,
                                  -18205,27244 ,-16151,28510 ,-14010,29621 ,-11793,30571 ,-9512 ,31356 ,-7180 ,31970 ,-4808 ,32412 ,-2411 ,32678};

constant short tw512rep[512] = 	{ 32767 ,0     ,32764 ,-403  ,32757 ,-805  ,32744 ,-1207 ,32727 ,-1608 ,32705 ,-2010 ,32678 ,-2411 ,32646 ,-2812 ,
								  32609 ,-3212 ,32567 ,-3612 ,32520 ,-4012 ,32468 ,-4410 ,32412 ,-4808 ,32350 ,-5206 ,32284 ,-5602 ,32213 ,-5998 ,
								  32137 ,-6393 ,32056 ,-6787 ,31970 ,-7180 ,31880 ,-7572 ,31785 ,-7962 ,31684 ,-8352 ,31580 ,-8740 ,31470 ,-9127 ,
								  31356 ,-9512 ,31236 ,-9896 ,31113 ,-10279,30984 ,-10660,30851 ,-11039,30713 ,-11417,30571 ,-11793,30424 ,-12167,
								  30272 ,-12540,30116 ,-12910,29955 ,-13279,29790 ,-13646,29621 ,-14010,29446 ,-14373,29268 ,-14733,29085 ,-15091,
								  28897 ,-15447,28706 ,-15800,28510 ,-16151,28309 ,-16500,28105 ,-16846,27896 ,-17190,27683 ,-17531,27466 ,-17869,
								  27244 ,-18205,27019 ,-18538,26789 ,-18868,26556 ,-19195,26318 ,-19520,26077 ,-19841,25831 ,-20160,25582 ,-20475,
								  25329 ,-20788,25072 ,-21097,24811 ,-21403,24546 ,-21706,24278 ,-22005,24006 ,-22302,23731 ,-22595,23452 ,-22884,
								  23169 ,-23170,22883 ,-23453,22594 ,-23732,22301 ,-24007,22004 ,-24279,21705 ,-24547,21402 ,-24812,21096 ,-25073,
								  20787 ,-25330,20474 ,-25583,20159 ,-25832,19840 ,-26078,19519 ,-26319,19194 ,-26557,18867 ,-26790,18537 ,-27020,
								  18204 ,-27245,17868 ,-27467,17530 ,-27684,17189 ,-27897,16845 ,-28106,16499 ,-28310,16150 ,-28511,15799 ,-28707,
								  15446 ,-28898,15090 ,-29086,14732 ,-29269,14372 ,-29447,14009 ,-29622,13645 ,-29791,13278 ,-29956,12909 ,-30117,
								  12539 ,-30273,12166 ,-30425,11792 ,-30572,11416 ,-30714,11038 ,-30852,10659 ,-30985,10278 ,-31114,9895  ,-31237,
								  9511  ,-31357,9126  ,-31471,8739  ,-31581,8351  ,-31685,7961  ,-31786,7571  ,-31881,7179  ,-31971,6786  ,-32057,
								  6392  ,-32138,5997  ,-32214,5601  ,-32285,5205  ,-32351,4807  ,-32413,4409  ,-32469,4011  ,-32521,3611  ,-32568,
								  3211  ,-32610,2811  ,-32647,2410  ,-32679,2009  ,-32706,1607  ,-32728,1206  ,-32745,804   ,-32758,402   ,-32765,
								  0     ,-32767,-403  ,-32765,-805  ,-32758,-1207 ,-32745,-1608 ,-32728,-2010 ,-32706,-2411 ,-32679,-2812 ,-32647,
								  -3212 ,-32610,-3612 ,-32568,-4012 ,-32521,-4410 ,-32469,-4808 ,-32413,-5206 ,-32351,-5602 ,-32285,-5998 ,-32214,
								  -6393 ,-32138,-6787 ,-32057,-7180 ,-31971,-7572 ,-31881,-7962 ,-31786,-8352 ,-31685,-8740 ,-31581,-9127 ,-31471,
								  -9512 ,-31357,-9896 ,-31237,-10279,-31114,-10660,-30985,-11039,-30852,-11417,-30714,-11793,-30572,-12167,-30425,
								  -12540,-30273,-12910,-30117,-13279,-29956,-13646,-29791,-14010,-29622,-14373,-29447,-14733,-29269,-15091,-29086,
								  -15447,-28898,-15800,-28707,-16151,-28511,-16500,-28310,-16846,-28106,-17190,-27897,-17531,-27684,-17869,-27467,
								  -18205,-27245,-18538,-27020,-18868,-26790,-19195,-26557,-19520,-26319,-19841,-26078,-20160,-25832,-20475,-25583,
								  -20788,-25330,-21097,-25073,-21403,-24812,-21706,-24547,-22005,-24279,-22302,-24007,-22595,-23732,-22884,-23453,
								  -23170,-23170,-23453,-22884,-23732,-22595,-24007,-22302,-24279,-22005,-24547,-21706,-24812,-21403,-25073,-21097,
								  -25330,-20788,-25583,-20475,-25832,-20160,-26078,-19841,-26319,-19520,-26557,-19195,-26790,-18868,-27020,-18538,
								  -27245,-18205,-27467,-17869,-27684,-17531,-27897,-17190,-28106,-16846,-28310,-16500,-28511,-16151,-28707,-15800,
								  -28898,-15447,-29086,-15091,-29269,-14733,-29447,-14373,-29622,-14010,-29791,-13646,-29956,-13279,-30117,-12910,
								  -30273,-12540,-30425,-12167,-30572,-11793,-30714,-11417,-30852,-11039,-30985,-10660,-31114,-10279,-31237,-9896 ,
								  -31357,-9512 ,-31471,-9127 ,-31581,-8740 ,-31685,-8352 ,-31786,-7962 ,-31881,-7572 ,-31971,-7180 ,-32057,-6787 ,
								  -32138,-6393 ,-32214,-5998 ,-32285,-5602 ,-32351,-5206 ,-32413,-4808 ,-32469,-4410 ,-32521,-4012 ,-32568,-3612 ,
								  -32610,-3212 ,-32647,-2812 ,-32679,-2411 ,-32706,-2010 ,-32728,-1608 ,-32745,-1207 ,-32758,-805  ,-32765,-403};

constant short twrad2[32]    =  { 32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,
								  32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0};

__kernel void ifft_512(__global const short * restrict x,__global short * restrict y)
{
	short data[POINTS];
	short xtmp0[32][32];
	short ytmp0[32][32];
	
	#pragma loop_coalesce
	for (uchar j=0; j<2; j++)
	{
		ushort l = j*512;
		#pragma unroll
		#pragma ivdep
		for (ushort i=0; i<512; i++)
		{
			data[l+i] = x[l+i];
		}
	}


	//Computations of 16-point iFFT
	#pragma loop_coalesce
	for (uchar j=0; j<32; j++)
	{
		//First stage : 4 Radix-4 butterflies without input twiddles
		short x1_flip0[8], x3_flip0[8];
		short x1_160[8], x2_160[8], x3_160[8];
		short xtemp0[32], ytemp0[32], ztemp0[32];

		//Data Reordering Step
		uchar l = j * 2;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i+=2)
		{
			ushort k=i*32;
			ztemp0[i] = data [k+l];
			ztemp0[i+1] = data [k+l+1];
		}

		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<8; i++)
		{
			x1_flip0[i] = (i&1) ? ztemp0[i+7]*(-1)  : ztemp0[i+9];
			x3_flip0[i] = (i&1) ? ztemp0[i+23]*(-1) : ztemp0[i+25];
	
			ytemp0[i]    = add_sat(add_sat(ztemp0[i], ztemp0[i+16]), add_sat(ztemp0[i+8], ztemp0[i+24]));
			ytemp0[i+16] = sub_sat(add_sat(ztemp0[i], ztemp0[i+16]), add_sat(ztemp0[i+8], ztemp0[i+24]));
			ytemp0[i+24] = add_sat(sub_sat(ztemp0[i], ztemp0[i+16]), sub_sat(x1_flip0[i], x3_flip0[i]));
			ytemp0[i+8]  = sub_sat(sub_sat(ztemp0[i], ztemp0[i+16]), sub_sat(x1_flip0[i], x3_flip0[i]));
		}

		//Data Reordering Step
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<2; i++)
		{
			xtemp0[i]    = ytemp0[i];
			xtemp0[i+2]  = ytemp0[i+8];
			xtemp0[i+4]  = ytemp0[i+16];
			xtemp0[i+6]  = ytemp0[i+24];	

			xtemp0[i+8]   = ytemp0[i+2];
			xtemp0[i+10]  = ytemp0[i+10];
			xtemp0[i+12]  = ytemp0[i+18];
			xtemp0[i+14]  = ytemp0[i+26];

			xtemp0[i+16]  = ytemp0[i+4];
			xtemp0[i+18]  = ytemp0[i+12];
			xtemp0[i+20]  = ytemp0[i+20];
			xtemp0[i+22]  = ytemp0[i+28];

			xtemp0[i+24]  = ytemp0[i+6];
			xtemp0[i+26]  = ytemp0[i+14];
			xtemp0[i+28]  = ytemp0[i+22];
			xtemp0[i+30]  = ytemp0[i+30];	
		}


		// Second stage : 4 Radix-4 butterflies with input twiddles
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<8; i++)
		{
			x1_160[i] = (i&1) ? ((xtemp0[i+8] *tw16crep[i]    +xtemp0[i+7]*tw16crep[i-1])>>15)      : ((xtemp0[i+9]*tw16rep[i+1]   +xtemp0[i+8]*tw16rep[i])>>15);
			x2_160[i] = (i&1) ? ((xtemp0[i+16]*tw16crep[i+8]  +xtemp0[i+15]*tw16crep[i+7])>>15)     : ((xtemp0[i+17]*tw16rep[i+9]  +xtemp0[i+16]*tw16rep[i+8])>>15);
			x3_160[i] = (i&1) ? ((xtemp0[i+24]*tw16crep[i+16] +xtemp0[i+23]*tw16crep[i+15])>>15)    : ((xtemp0[i+25]*tw16rep[i+17] +xtemp0[i+24]*tw16rep[i+16])>>15);

			x1_flip0[i] = (i&1) ? ((xtemp0[i+8]*tw16rep[i]     +xtemp0[i+7]*tw16rep[i-1])>>15)*(-1)   : ((xtemp0[i+9]*tw16crep[i+1]   +xtemp0[i+8]*tw16crep[i])>>15);
			x3_flip0[i] = (i&1) ? ((xtemp0[i+24]*tw16rep[i+16] +xtemp0[i+23]*tw16rep[i+15])>>15)*(-1) : ((xtemp0[i+25]*tw16crep[i+17] +xtemp0[i+24]*tw16crep[i+16])>>15);
				
			ytmp0[j][i]    = add_sat(add_sat(xtemp0[i], x2_160[i]), add_sat(x1_160[i], x3_160[i]));
			ytmp0[j][i+8]  = sub_sat(sub_sat(xtemp0[i], x2_160[i]), sub_sat(x1_flip0[i], x3_flip0[i]));
			ytmp0[j][i+16] = sub_sat(add_sat(xtemp0[i], x2_160[i]), add_sat(x1_160[i], x3_160[i]));
			ytmp0[j][i+24] = add_sat(sub_sat(xtemp0[i], x2_160[i]), sub_sat(x1_flip0[i], x3_flip0[i]));
		}
	}

	
	//Computations of 64-point iFFT
	// Third stage : Radix-4 butterflies with input twiddles
	#pragma loop_coalesce
	for (uchar j=0; j<8; j++)
	{
		short xtmp1_00[32], xtmp2_00[32], xtmp3_00[32];
		short x1_flip0[32], x3_flip0[32];

		uchar k=j*4;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i++)
		{
			xtmp1_00[i]   =	(i&1) ? (ytmp0[j+8][i]*tw64crep[i]    +ytmp0[j+8][i-1]*tw64crep[i-1])>>15  : (ytmp0[j+8][i+1]*tw64rep[i+1]   +ytmp0[j+8][i]*tw64rep[i])>>15;
			xtmp2_00[i]   = (i&1) ? (ytmp0[j+16][i]*tw64crep[i+32] +ytmp0[j+16][i-1]*tw64crep[i+31])>>15 : (ytmp0[j+16][i+1]*tw64rep[i+33]  +ytmp0[j+16][i]*tw64rep[i+32])>>15;
			xtmp3_00[i]   = (i&1) ? (ytmp0[j+24][i]*tw64crep[i+64] +ytmp0[j+24][i-1]*tw64crep[i+63])>>15 : (ytmp0[j+24][i+1]*tw64rep[i+65]  +ytmp0[j+24][i]*tw64rep[i+64])>>15;

			x1_flip0[i] = (i&1) ? ((ytmp0[j+8][i]*tw64rep[i] +ytmp0[j+8][i-1]*tw64rep[i-1])>>15) * (-1)     : (ytmp0[j+8][i+1]*tw64crep[i+1]   +ytmp0[j+8][i]*tw64crep[i])>>15;
			x3_flip0[i] = (i&1) ? ((ytmp0[j+24][i]*tw64rep[i+64] +ytmp0[j+24][i-1]*tw64rep[i+63])>>15) * (-1) : (ytmp0[j+24][i+1]*tw64crep[i+65]  +ytmp0[j+24][i]*tw64crep[i+64])>>15;
	
			xtmp0[k][i]      = add_sat(add_sat(ytmp0[j][i]   , xtmp2_00[i]), add_sat(xtmp1_00[i],xtmp3_00[i])) >> 3;
			xtmp0[k+1][i] 	 = sub_sat(sub_sat(ytmp0[j][i]   , xtmp2_00[i]), sub_sat(x1_flip0[i],x3_flip0[i])) >> 3;
			
			xtmp0[k+2][i]    = sub_sat(add_sat(ytmp0[j][i]   , xtmp2_00[i]), add_sat(xtmp1_00[i],xtmp3_00[i])) >> 3;
			xtmp0[k+3][i]    = add_sat(sub_sat(ytmp0[j][i]   , xtmp2_00[i]), sub_sat(x1_flip0[i],x3_flip0[i])) >> 3;
		}	

	}

	short tmp[2][512];
	//Computations of 256-point iFFT
	// Fourth stage : Radix-4 butterflies with input twiddles
	#pragma loop_coalesce
  	for (uchar j=0; j<2; j++)
	{	
		uchar  l = j * 4;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<128; i+=2)
		{
			tmp[j][i]     = (((xtmp0[l+8][i+1]*tw256[i+1]    + xtmp0[l+8][i]*tw256[i]      + xtmp0[l+16][i+1]*tw256[i+129] + xtmp0[l+16][i]*tw256[i+128] + xtmp0[l+24][i+1]*tw256[i+257] + xtmp0[l+24][i]*tw256[i+256]) >> 15)   + xtmp0[l][i])>>1;
			tmp[j][i+1]   = (((xtmp0[l+8][i+1]*tw256[i]      - xtmp0[l+8][i]*tw256[i+1]    + xtmp0[l+16][i+1]*tw256[i+128] - xtmp0[l+16][i]*tw256[i+129] + xtmp0[l+24][i+1]*tw256[i+256] - xtmp0[l+24][i]*tw256[i+257]) >> 15) + xtmp0[l][i+1])>>1;
		
			tmp[j][i+128] = (((xtmp0[l+24][i+1]*tw256[i+256] - xtmp0[l+24][i]*tw256[i+257] - xtmp0[l+16][i+1]*tw256[i+129] - xtmp0[l+16][i]*tw256[i+128] - xtmp0[l+8][i+1]*tw256[i]      + xtmp0[l+8][i]*tw256[i+1]) >> 15)    + xtmp0[l][i])>>1;
			tmp[j][i+129] = (((xtmp0[l+8][i+1]*tw256[i+1]    + xtmp0[l+8][i]*tw256[i]      - xtmp0[l+16][i+1]*tw256[i+128] + xtmp0[l+16][i]*tw256[i+129] - xtmp0[l+24][i+1]*tw256[i+257] - xtmp0[l+24][i]*tw256[i+256]) >> 15)   + xtmp0[l][i+1])>>1;
		
			tmp[j][i+256] = (((xtmp0[l+16][i+1]*tw256[i+129] + xtmp0[l+16][i]*tw256[i+128] - xtmp0[l+24][i+1]*tw256[i+257] - xtmp0[l+24][i]*tw256[i+256] - xtmp0[l+8][i+1]*tw256[i+1]    - xtmp0[l+8][i]*tw256[i]) >> 15)      + xtmp0[l][i])>>1;
			tmp[j][i+257] = (((xtmp0[l+16][i+1]*tw256[i+128] - xtmp0[l+16][i]*tw256[i+129] - xtmp0[l+24][i+1]*tw256[i+256] + xtmp0[l+24][i]*tw256[i+257] - xtmp0[l+8][i+1]*tw256[i]      + xtmp0[l+8][i]*tw256[i+1]) >> 15)    + xtmp0[l][i+1])>>1;
		
			tmp[j][i+384] = (((xtmp0[l+8][i+1]*tw256[i]      - xtmp0[l+8][i]*tw256[i+1]    - xtmp0[l+16][i+1]*tw256[i+129] - xtmp0[l+16][i]*tw256[i+128] - xtmp0[l+24][i+1]*tw256[i+256] + xtmp0[l+24][i]*tw256[i+257]) >> 15) + xtmp0[l][i])>>1;
			tmp[j][i+385] = (((xtmp0[l+24][i+1]*tw256[i+257] + xtmp0[l+24][i]*tw256[i+256] - xtmp0[l+16][i+1]*tw256[i+128] + xtmp0[l+16][i]*tw256[i+129] - xtmp0[l+8][i+1]*tw256[i+1]    - xtmp0[l+8][i]*tw256[i]) >> 15)    + xtmp0[l][i+1])>>1;
		}

	}


	short tmp1[1024];
	//Computations of 512-point iFFT
	// Fifth stage : Radix-2 butterflies with input twiddles
	#pragma loop_coalesce
	for (uchar j=0; j<16; j++)
	{
		int xtmpr_0[32], xtmpc_0[32], xtmpr_1[32], xtmpc_1[32];
		ushort l = j*32;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32 ; i+=2)
		{
		
		xtmpr_0[i] = tmp[0][l+i+1] * twrad2[i+1] + tmp[0][l+i] * twrad2[i];
		xtmpc_0[i] = tmp[0][l+i+1] * twrad2[i]   + tmp[0][l+i] * twrad2[i+1];

		xtmpr_1[i] = tmp[1][l+i+1] * tw512rep[l+i+1] + tmp[1][l+i] * tw512rep[l+i];
		xtmpc_1[i] = tmp[1][l+i+1] * tw512rep[l+i]   +(tmp[1][l+i] * tw512rep[l+i+1])*(-1);

		tmp1[l+i]     = add_sat(xtmpr_0[i], xtmpr_1[i]) >> 15;
		tmp1[l+i+1]   = add_sat(xtmpc_0[i], xtmpc_1[i]) >> 15;
		tmp1[l+i+512] = sub_sat(xtmpr_0[i], xtmpr_1[i]) >> 15;
		tmp1[l+i+513] = sub_sat(xtmpc_0[i], xtmpc_1[i]) >> 15;
		}
	}

	short tmp2[1024];
	#pragma loop_coalesce
	for (uchar i=0; i<64; i++)
	{
		ushort l= i*16;
		#pragma unroll
		#pragma ivdep
    	for (uchar j=0; j<16 ; j++)
    	{
		tmp2[l+j] = ((tmp1[l+j] * ONE_OVER_SQRT2_Q15) >> 15);
		}
	}


	//Cyclic Prefix Addition for 512-point iFFT
	short tmp3[CP];
	#pragma loop_coalesce
	for (uchar j=0; j<32; j++)
	{
		ushort l = j*32;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i++)
		{
			tmp3[l+i+80] = tmp2[l+i];	
		}
	}

	#pragma unroll
	#pragma ivdep
	for (uchar i=0; i<80 ; i++)
	{
		tmp3[i] = tmp2[944 + i];					//512 minus the CP which is 40
	}


	

	#pragma loop_coalesce
	for (uchar j=0; j<3; j++)
	{
		ushort l = j*368;
		#pragma unroll
		#pragma ivdep
		for (ushort i=0; i<368; i++)
		{
			y[l+i] = tmp3[l+i];	
		}
		
	}
}
