#define POINTS 256
#define CP 276
#define ONE_OVER_SQRT2_Q15 23170

constant short tw16rep[24]  = {  32767, 0, 30272, -12540, 23169 , -23170, 12539 , -30273,
								32767, 0, 23169, -23170, 0     , -32767, -23170, -23170,
						        32767, 0, 12539, -30273, -23170, -23170, -30273, 12539};

constant short tw16crep[24] = { 0, 32767, 12540, 30272, 23170, 23169 , 30273 , 12539,
						        0, 32767, 23170, 23169, 32767, 0     , 23170 , -23170,
						        0, 32767, 30273, 12539, 23170, -23170, -12539, -30273};

constant short tw64rep[96] = {	32767 ,0      ,32609 ,-3212 ,32137 ,-6393 , 31356,-9512 ,30272 ,-12540,28897 ,-15447,27244 ,-18205,25329 ,-20788, 
								23169 ,-23170 , 20787,-25330,18204 ,-27245, 15446,-28898,12539 ,-30273,9511  ,-31357,6392  ,-32138,3211  , -32610,
								32767 ,0      ,32137 ,-6393 ,30272 ,-12540,27244 ,-18205,23169 ,-23170,18204 ,-27245,12539 ,-30273,6392  ,-32138, 
								0     ,-32767 ,-6393 ,-32138,-12540,-30273,-18205,-27245,-23170,-23170,-27245,-18205,-30273,-12540,-32138,-6393,
								32767 ,0      ,31356 ,-9512 ,27244 ,-18205,20787 ,-25330,12539 ,-30273,3211  ,-32610,-6393 ,-32138,-15447,-28898,
								-23170, -23170,-28898,-15447,-32138,-6393 ,-32610, 3211 ,-30273, 12539,-25330, 20787,-18205, 27244,-9512 ,  31356};

constant short tw64crep[96] = {	0    ,32767 ,3212 ,32609 ,6393 ,32137 ,9512 ,31356 ,12540 ,30272 ,15447 ,28897 ,18205 ,27244 ,20788 ,25329, 
								23170,23169 ,25330,20787 ,27245,18204 ,28898,15446 ,30273 ,12539 ,31357 ,9511  ,32138 , 6392 ,32610 ,3211,
								0    ,32767 ,6393 ,32137 ,12540,30272 ,18205,27244 ,23170 ,23169 ,27245 ,18204 ,30273 ,12539 ,32138 ,6392,  
								32767,0     ,32138,-6393 ,30273,-12540,27245,-18205,23170 ,-23170,18205 ,-27245,12540 ,-30273,6393  ,-32138,
								0    ,32767 ,9512 ,31356 ,18205,27244 ,25330,20787 ,30273 ,12539 ,32610 ,3211  ,32138 ,-6393 ,28898 ,-15447, 
								23170,-23170,15447,-28898,6393 ,-32138,-3211,-32610,-12539,-30273,-20787,-25330,-27244,-18205,-31356,-9512};

constant short tw128rep[4][32] = {{32767 ,0     ,32727 ,-1608 ,32609 ,-3212 ,32412 ,-4808 ,32137 ,-6393 ,31785 ,-7962 ,31356 ,-9512 ,30851 ,-11039,
                                 30272 ,-12540,29621 ,-14010,28897 ,-15447,28105 ,-16846,27244 ,-18205,26318 ,-19520,25329 ,-20788,24278 ,-22005},
                                 {23169 ,-23170,22004 ,-24279,20787 ,-25330,19519 ,-26319,18204 ,-27245,16845 ,-28106,15446 ,-28898,14009 ,-29622,
                                 12539 ,-30273,11038 ,-30852,9511  ,-31357,7961  ,-31786,6392  ,-32138,4807  ,-32413,3211  ,-32610,1607  ,-32728},
                                 {0     ,-32767,-1608 ,-32728,-3212 ,-32610,-4808 ,-32413,-6393 ,-32138,-7962 ,-31786,-9512 ,-31357,-11039,-30852,
                                 -12540,-30273,-14010,-29622,-15447,-28898,-16846,-28106,-18205,-27245,-19520,-26319,-20788,-25330,-22005,-24279},
                                 {-23170,-23170,-24279,-22005,-25330,-20788,-26319,-19520,-27245,-18205,-28106,-16846,-28898,-15447,-29622,-14010,
                                 -30273,-12540,-30852,-11039,-31357,-9512 ,-31786,-7962 ,-32138,-6393 ,-32413,-4808 ,-32610,-3212 ,-32728,-1608}};

constant short twrad2[32] = {32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0, 
							 32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0};

__kernel void ifft_128(__global const short * restrict x,__global short * restrict y)
{
	short data[POINTS];
	short xtmp0[8][32];
	short ytmp0[8][32];
	

	#pragma unroll
	#pragma ivdep
	for (ushort i=0; i<POINTS; i++)
	{
		data[i] = x[i];
	}

	//Input of IFFT kernel
	#pragma loop_coalesce
	for (uchar j=0; j<8; j++)
	{
		short x1_flip0[8], x3_flip0[8];
		short x1_160[8], x2_160[8], x3_160[8];
		short xtemp0[32], ytemp0[32], ztemp[32];

		uchar k=j*2;
		//Data Reordering Step
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i+=2)
		{
			ztemp[i] = data [i*8+k];	
			ztemp[i+1] = data [i*8+k+1];
		}

		//Computations of 16-point iFFT
		//First stage : 4 Radix-4 butterflies without input twiddles
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<8; i++)
		{
			x1_flip0[i] = (i&1) ? ztemp[i+7]*(-1)  : ztemp[i+9];
			x3_flip0[i] = (i&1) ? ztemp[i+23]*(-1) : ztemp[i+25];
	
			ytemp0[i]    = add_sat(add_sat(ztemp[i], ztemp[i+16]), add_sat(ztemp[i+8], ztemp[i+24]));
			ytemp0[i+16] = sub_sat(add_sat(ztemp[i], ztemp[i+16]), add_sat(ztemp[i+8], ztemp[i+24]));
			ytemp0[i+24] = add_sat(sub_sat(ztemp[i], ztemp[i+16]), sub_sat(x1_flip0[i], x3_flip0[i]));
			ytemp0[i+8]  = sub_sat(sub_sat(ztemp[i], ztemp[i+16]), sub_sat(x1_flip0[i], x3_flip0[i]));
		}

		//Data Reordering Step
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<2; i++)
		{
			xtemp0[i]    = ytemp0[i];
			xtemp0[i+2]  = ytemp0[i+8];
			xtemp0[i+4]  = ytemp0[i+16];
			xtemp0[i+6]  = ytemp0[i+24];	

			xtemp0[i+8]   = ytemp0[i+2];
			xtemp0[i+10]  = ytemp0[i+10];
			xtemp0[i+12]  = ytemp0[i+18];
			xtemp0[i+14]  = ytemp0[i+26];

			xtemp0[i+16]  = ytemp0[i+4];
			xtemp0[i+18]  = ytemp0[i+12];
			xtemp0[i+20]  = ytemp0[i+20];
			xtemp0[i+22]  = ytemp0[i+28];

			xtemp0[i+24]  = ytemp0[i+6];
			xtemp0[i+26]  = ytemp0[i+14];
			xtemp0[i+28]  = ytemp0[i+22];
			xtemp0[i+30]  = ytemp0[i+30];	
		}


		// Second stage : 4 Radix-4 butterflies with input twiddles
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<8; i++)
		{
			x1_160[i] = (i&1) ? ((xtemp0[i+8] *tw16crep[i]    +xtemp0[i+7]*tw16crep[i-1])>>15)      : ((xtemp0[i+9]*tw16rep[i+1]   +xtemp0[i+8]*tw16rep[i])>>15);
			x2_160[i] = (i&1) ? ((xtemp0[i+16]*tw16crep[i+8]  +xtemp0[i+15]*tw16crep[i+7])>>15)     : ((xtemp0[i+17]*tw16rep[i+9]  +xtemp0[i+16]*tw16rep[i+8])>>15);
			x3_160[i] = (i&1) ? ((xtemp0[i+24]*tw16crep[i+16] +xtemp0[i+23]*tw16crep[i+15])>>15)    : ((xtemp0[i+25]*tw16rep[i+17] +xtemp0[i+24]*tw16rep[i+16])>>15);

			x1_flip0[i] = (i&1) ? ((xtemp0[i+8]*tw16rep[i]     +xtemp0[i+7]*tw16rep[i-1])>>15)*(-1)   : ((xtemp0[i+9]*tw16crep[i+1]   +xtemp0[i+8]*tw16crep[i])>>15);
			x3_flip0[i] = (i&1) ? ((xtemp0[i+24]*tw16rep[i+16] +xtemp0[i+23]*tw16rep[i+15])>>15)*(-1) : ((xtemp0[i+25]*tw16crep[i+17] +xtemp0[i+24]*tw16crep[i+16])>>15);
				
			ytmp0[j][i]    = add_sat(add_sat(xtemp0[i], x2_160[i]), add_sat(x1_160[i], x3_160[i]));
			ytmp0[j][i+8]  = sub_sat(sub_sat(xtemp0[i], x2_160[i]), sub_sat(x1_flip0[i], x3_flip0[i]));
			ytmp0[j][i+16] = sub_sat(add_sat(xtemp0[i], x2_160[i]), add_sat(x1_160[i], x3_160[i]));
			ytmp0[j][i+24] = add_sat(sub_sat(xtemp0[i], x2_160[i]), sub_sat(x1_flip0[i], x3_flip0[i]));
		}
	}

	
	//Computations of 64-point iFFT
	// Third stage : Radix-4 butterflies with input twiddles
	#pragma loop_coalesce
	for (uchar j=0; j<2; j++)
	{
		short xtmp1_00[32], xtmp2_00[32], xtmp3_00[32];
		short x1_flip0[32], x3_flip0[32];

		uchar k=j*4;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i++)
		{
			xtmp1_00[i]   =	(i&1) ? (ytmp0[k+1][i]*tw64crep[i]    +ytmp0[k+1][i-1]*tw64crep[i-1])>>15  : (ytmp0[k+1][i+1]*tw64rep[i+1]   +ytmp0[k+1][i]*tw64rep[i])>>15;
			xtmp2_00[i]   = (i&1) ? (ytmp0[k+2][i]*tw64crep[i+32] +ytmp0[k+2][i-1]*tw64crep[i+31])>>15 : (ytmp0[k+2][i+1]*tw64rep[i+33]  +ytmp0[k+2][i]*tw64rep[i+32])>>15;
			xtmp3_00[i]   = (i&1) ? (ytmp0[k+3][i]*tw64crep[i+64] +ytmp0[k+3][i-1]*tw64crep[i+63])>>15 : (ytmp0[k+3][i+1]*tw64rep[i+65]  +ytmp0[k+3][i]*tw64rep[i+64])>>15;

			x1_flip0[i] = (i&1) ? ((ytmp0[k+1][i]*tw64rep[i] +ytmp0[k+1][i-1]*tw64rep[i-1])>>15) * (-1)     : (ytmp0[k+1][i+1]*tw64crep[i+1]   +ytmp0[k+1][i]*tw64crep[i])>>15;
			x3_flip0[i] = (i&1) ? ((ytmp0[k+3][i]*tw64rep[i+64] +ytmp0[k+3][i-1]*tw64rep[i+63])>>15) * (-1) : (ytmp0[k+3][i+1]*tw64crep[i+65]  +ytmp0[k+3][i]*tw64crep[i+64])>>15;
	
			xtmp0[k][i]      = add_sat(add_sat(ytmp0[k][i]   , xtmp2_00[i]), add_sat(xtmp1_00[i],xtmp3_00[i])) >> 3;
			xtmp0[k+1][i] 	 = sub_sat(sub_sat(ytmp0[k][i]   , xtmp2_00[i]), sub_sat(x1_flip0[i],x3_flip0[i])) >> 3;
			
			xtmp0[k+2][i]    = sub_sat(add_sat(ytmp0[k][i]   , xtmp2_00[i]), add_sat(xtmp1_00[i],xtmp3_00[i])) >> 3;
			xtmp0[k+3][i]    = add_sat(sub_sat(ytmp0[k][i]   , xtmp2_00[i]), sub_sat(x1_flip0[i],x3_flip0[i])) >> 3;
		}	

	}

	short tmp[8][32];
	#pragma loop_coalesce
	for (uchar j=0; j<4; j++)
	{

		//Computations of 128-point iFFT
		// Fourth stage : Radix-2 butterflies with input twiddles  
		int xtmp_0[32], xtmp_1[32];

		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i++)
		{
		xtmp_0[i]   = (i&1) ? (xtmp0[j][i] *twrad2[i-1]   +xtmp0[j][i-1] *twrad2[i]) : (xtmp0[j][i+1] *twrad2[i+1] +xtmp0[j][i] *twrad2[i]); 
		xtmp_1[i]   = (i&1) ? (xtmp0[j+4][i] *tw128rep[j][i-1]   +(xtmp0[j+4][i-1] *tw128rep[j][i]) *(-1)) : (tw128rep[j][i+1] * xtmp0[j+4][i+1] + tw128rep[j][i] * xtmp0[j+4][i]);
			
		tmp[j][i]   = add_sat(xtmp_0[i], xtmp_1[i]) >> 15;
		tmp[j+4][i] = sub_sat(xtmp_0[i], xtmp_1[i]) >> 15;
		}
	}

	short tmp1[256];
	short tmp2 [CP];
	#pragma loop_coalesce
	for (uchar j=0; j<8; j++)
	{
		ushort l= j*32;
		#pragma unroll
		#pragma ivdep
    	for (uchar i=0; i<32 ; i++)
    	{
		tmp1[l+i] = ((tmp[j][i] * ONE_OVER_SQRT2_Q15) >> 15);
		}
	}

	//Cyclic Prefix Addition for 128-point iFFT
	#pragma loop_coalesce
	for (uchar j=0; j<8; j++)
	{
		ushort l = j*32;
		#pragma unroll
		#pragma ivdep
		for (uchar i=0; i<32; i++)
		{
			tmp2[l+i+20] = tmp1[l+i];	
		}
	}

	#pragma unroll
	#pragma ivdep
	for (uchar i=0; i<20 ; i++)
	{
		tmp2[i] = tmp1[236 + i];					//256 minus the CP which is 20
	}


	#pragma unroll
	#pragma ivdep
	for (ushort i = 0; i < 276; i++)
	{
		y[i] = tmp2[i];			
	}

}
