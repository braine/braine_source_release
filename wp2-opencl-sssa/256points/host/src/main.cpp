// Copyright (C) 2013-2018 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

///////////////////////////////////////////////////////////////////////////////////
// This OpenCL application executes a 1D FFT transform on an Altera FPGA.
// The kernel is defined in a device/fft1d_offchip.cl file.  The Altera
// Offline Compiler tool ('aoc') compiles the kernel source into a 'fft1d_offchip.aocx'
// file containing a hardware programming image for the FPGA.  The host program
// provides the contents of the .aocx file to the clCreateProgramWithBinary OpenCL
// API for runtime programming of the FPGA.
//
// When compiling this application, ensure that the Intel(R) FPGA SDK for OpenCL(TM)
// is properly installed.
///////////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <cstdio>
#include <cstdlib>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cstring>
#include <time.h>
#include "AOCLUtils/aocl_utils.h"
#include "CL/opencl.h"
#include <immintrin.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdint.h>
#include <pthread.h>
#include <execinfo.h>

#include "fft_config.h"


using namespace aocl_utils;

//#define STRING_BUFFER_LEN 1024

// ACL runtime configuration
static cl_platform_id platform = NULL;
static cl_device_id device = NULL;
static cl_context context = NULL;

static cl_command_queue queue = NULL;
static cl_command_queue queue2 = NULL;
// static cl_command_queue queue4 = NULL, queue5 = NULL;

static cl_kernel fft_kernel = NULL;
static cl_kernel cyclic_prefix = NULL;
// static cl_kernel fetch_MWT_kernel = NULL, transpose_MWT_kernel = NULL;

static cl_program program = NULL;
static cl_int status = 0;


// Function prototypes
bool init();
void cleanup();
static bool test_fft();

// Device memory buffers
cl_mem d_inData, d_outData;
cl_mem d_inData2, d_outData2;



#define ONE_OVER_SQRT2_Q15 23170
#define mulhi_int16_simd256(a,b) _mm256_slli_epi16(_mm256_mulhi_epi16(a,b),1);


const static int16_t W0s[16] = {32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0,32767,0};

const __m256i *W0_256 =  (__m256i *)W0s;



// Entry point.
int main(int argc, char **argv) {

  if(!init()) {
    return false;
  }
  // Allocate host memory

  test_fft();


  // Free the resources allocated
  cleanup();

  return 0;
}

bool test_fft()
{
  #define SIZE 256
  #define SIZEB 512
  #define CP 40
  #define ITERATIONS 10000
  printf("Launching iFFT transform\n");

  //Profiling parameters
  cl_event timing_event, timing_write, timing_read;
  cl_ulong time_start, time_end, write_start, write_end, read_start, read_end;
  cl_ulong total_time = 0.0f;
  cl_ulong write_time = 0.0f;
  cl_ulong read_time = 0.0f;

  struct timespec start1, end1;
  //struct timespec start2, end2;

  int32_t *buffer = NULL;
  int16_t *result = NULL; 

  buffer = (int32_t*)alignedMalloc(sizeof(int32_t)*SIZE);
  result = (int16_t*)alignedMalloc(sizeof(int16_t)*(SIZEB+CP));

  short nb_prefix_sample, fftsize = 0;
  char etype = 0;

  //~ int16_t* buffer_16 = (int16_t*) buffer;

  __m256i *x256 = (__m256i *)buffer;
  __m256i *result256 = (__m256i *)result;
  register __m256i xintl0,xintl1,xintl2,xintl3,xintl4,xintl5,xintl6,xintl7;

  /*for (int i = 0; i < 32; i++) {
    buffer[i] = 512;
  //  printf("buffer[%d] %d\n", i, buffer[i]);
  }
  for (int j = 32; j < 64; j++) {
    buffer[j] = 256;
  //  printf("buffer[%d] %d\n", i, buffer[i]);
  }*/

  for (int i = 0; i < 256; i++) {
    buffer[i] = i;
  //  printf("buffer[%d] %d\n", i, buffer[i]);
  }

  int16_t* data_16 = (int16_t*) x256;
  //int32_t* xtmp3_32 = (int32_t*) xtmp3;           //Edited: Justine 
  //for (int i = 0; i < 32; i++)
  //   printf("input[%d] %d\n", i, data_16[i]);

 /* printf("\n Input of 128-point iFFT\n");
  for (int i=0; i<8; i++)
  {
    int l= i*16;
    for (int j=0; j<16 ; j++)
    {
      printf(" %d", data_16[l+j]);
    }
    printf(" \n");
  }
  */

  // for (int i = 0; i < 1024; i++)             //Edited: Justine
  //   printf("data_16[%d] %d\n", i, data_16[i]);

  // Create device buffers - assign the buffers in different banks for more efficient
  // memory access
  
   d_inData = clCreateBuffer(context, CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_ONLY, sizeof(int16_t)*SIZEB, NULL, &status);   //256 input data with 16-bits per input
   checkError(status, "Failed to allocate input device buffer\n");
   d_outData = clCreateBuffer(context, CL_MEM_ALLOC_HOST_PTR | CL_MEM_WRITE_ONLY, sizeof(int16_t)*(SIZEB+CP), NULL, &status); //276 output data with 16-bits per output (separating the real and complex part)
   checkError(status, "Failed to allocate output device buffer\n");

   // Set the kernel arguments
    status = clSetKernelArg(fft_kernel, 0, sizeof(cl_mem), (void *)&d_inData);
    checkError(status, "Failed to set kernel arg 0");
    status = clSetKernelArg(fft_kernel, 1, sizeof(cl_mem), (void *)&d_outData);
    checkError(status, "Failed to set kernel arg 1");

    clock_gettime(CLOCK_REALTIME, &start1);
    for (int j = 0; j < ITERATIONS; j++)                          //Removed: Justine since the kernel was launched based on the number of iterations
    { 

    status = clEnqueueWriteBuffer(queue, d_inData, CL_TRUE, 0, sizeof(int16_t)*SIZEB, data_16, 0, NULL, &timing_write);
    checkError(status, "Failed to copy data to device");
    
    //printf("\nLaunching the kernel...\n");
    //printf("Iteration %d\n", j);                            
    status = clEnqueueTask(queue, fft_kernel, 0, NULL, &timing_event);
    status = clFinish(queue);
    

    status = clEnqueueReadBuffer(queue, d_outData, CL_TRUE, 0, sizeof(int16_t)*(SIZEB+CP), result, 0, NULL, &timing_read);
    checkError(status, "Failed to copy data from device");  
    

    status = clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &time_start, NULL);
    status = clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &time_end, NULL);

    status = clGetEventProfilingInfo(timing_write, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &write_start, NULL);
    status = clGetEventProfilingInfo(timing_write, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &write_end, NULL);

    status = clGetEventProfilingInfo(timing_read, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &read_start, NULL);
    status = clGetEventProfilingInfo(timing_read, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &read_end, NULL);

    total_time += time_end - time_start;
    write_time += write_end - write_start;
    read_time += read_end - read_start;
    }
    //checkError(status, "failed to finish");
     clock_gettime(CLOCK_REALTIME, &end1);

  //for (int i = 0; i < 128; i++)
  //printf("output[%d] %d\n", i, result[i]);
  //printf("Processing time fft = %ld ns\n", (((end1.tv_sec * 1000000000) + end1.tv_nsec)-((start1.tv_sec * 1000000000) + start1.tv_nsec))/ITERATIONS);
  //printf("Processing time fft = %lld ns\n", (end1.tv_nsec-start1.tv_nsec)/ITERATIONS);
  //printf("Processing time fft = %lld ns\n", ((end1.tv_sec * 1000000000)-(start1.tv_sec * 1000000000 + start1.tv_sec))/ITERATIONS);
  printf("Processing time ifft = %ld ns\n",  (((end1.tv_sec - start1.tv_sec)* 1000000000) + (end1.tv_nsec - start1.tv_nsec))/ITERATIONS);
  printf("Average time ifft = %lu ns\n", total_time/ITERATIONS);
  printf("Average write ifft = %lu ns\n", write_time/ITERATIONS);
  printf("Average read ifft = %lu ns\n", read_time/ITERATIONS);
  for (int i=0; i<46; i++)
  {
    int l= i*12;
    for (int j=0; j<12 ; j++)
    {
      printf(" %d", result[l+j]);
    }
    printf(" \n");
  }

  
}


bool init() {
  cl_int status;

  if(!setCwdToExeDir()) {
    return false;
  }

  // Get the OpenCL platform.
  platform = findPlatform("Intel(R) FPGA SDK for OpenCL(TM)");
  if(platform == NULL) {
    printf("ERROR: Unable to find Intel(R) FPGA OpenCL platform\n");
    return false;
  }


  // Query the available OpenCL devices.
  scoped_array<cl_device_id> devices;
  cl_uint num_devices;

  devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));
  printf("Platform: %s\n", getPlatformName(platform).c_str());      //Added: Justine
  printf("Using %d device(s)\n", num_devices);              //Added: Justine

  // We'll just use the first device.
  device = devices[0];

  // Create the context.
  context = clCreateContext(NULL, 1, &device, &oclContextCallback, NULL, &status);
  checkError(status, "Failed to create context");

  // Create one command queue for each kernel.
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  checkError(status, "Failed to create command queue");

  // Create the program.
  std::string binary_file = getBoardBinaryFile("ofdm", device);
  printf("Using AOCX: %s\n\n", binary_file.c_str());
  program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);

  // Build the program that was just created.
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  checkError(status, "Failed to build program");

  // Create the kernel - name passed in here must match kernel name in the
  // original CL file, that was compiled into an AOCX file using the AOC tool
  fft_kernel = clCreateKernel(program, "ifft_256", &status);
  checkError(status, "Failed to create kernel");

  return true;
}

// Free the resources allocated during initialization
void cleanup() {
  if(fft_kernel)
  clReleaseKernel(fft_kernel);
  if(program)
  clReleaseProgram(program);
  if(queue)
  clReleaseCommandQueue(queue);
  if(context)
  clReleaseContext(context);
  if (d_inData)
  clReleaseMemObject(d_inData);
  if (d_outData)
  clReleaseMemObject(d_outData);
}
