# WP3 SDN CONTROLLER

Implementation of SDN controller applications and interfaces based on the ONOS open source software.

- Tested with ONOS v2.6, but should work with all subsequent versions

- The ONOS software can be cloned from the official onos pages: git clone https://gerrit.onosproject.org/onos

Once ONOS is running on your machine you can build and install the two applications.

- cd braine-app

- mvn clean install

- onos-app localhost install! ./target/braine-app-2.0.1.oar


