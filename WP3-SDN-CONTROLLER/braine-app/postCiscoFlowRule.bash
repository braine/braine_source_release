#! /bin/bash
REMOTE_PATH='http://192.168.242.4:8181/onos/v1/flowobjectives/of%3A0000000000000222/forward?appId=50'

curl -u karaf:karaf -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '
{
  "flag": "SPECIFIC",
  "priority": 40000,
  "timeout": 0,
  "isPermanent": true,
  "deviceId": "of:0000000000000222",
  "operation": "ADD",
  "selector": {
    "criteria": [
      {
        "type": "VLAN_VID",
        "vlanId": "66"
      },
      {
        "type": "ETH_TYPE",
        "ethType": "0x0800"
      },
      {
        "type": "ETH_DST",
        "mac": "00:00:00:00:00:91"
      },
      {
        "type": "ETH_SRC",
        "mac": "00:00:00:00:00:21"
      },
      {
        "type": "IN_PORT",
        "port": "5097"
      }
    ]
  },
  "treatment": {
    "instructions": [
      {
        "type":"L2MODIFICATION",
        "subtype":"VLAN_ID",
        "vlanId":200
      },
      {
        "type": "OUTPUT",
        "port": "5099"
      }
    ]
  }
}
' "$REMOTE_PATH"

echo 

