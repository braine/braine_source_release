#! /bin/bash
REMOTE_PATH='http://localhost:8181/onos/matrix-app/connectivities/add'

curl -u karaf:karaf -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "hostOne": "00:00:00:00:00:42/None", 
   "hostTwo": "00:00:00:00:00:81/None", 
   "name": "connection-42-81", 
   "bidirectional": true 
 }' "$REMOTE_PATH"

echo 

