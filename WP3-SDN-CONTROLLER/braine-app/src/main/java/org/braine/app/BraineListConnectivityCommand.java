package org.braine.app;

import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Option;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;

/**
 * Sample Apache Karaf CLI command
 */
@Service
@Command(scope = "onos", name = "braine-connectivity-list",
        description = "Show currently active connections")
public class BraineListConnectivityCommand extends AbstractShellCommand {
    @Option(name = "--details", description = "Print detailed information",
            required = false, multiValued = false)
    private boolean detailed = false;

    @Override
    protected void doExecute() {
        ConnectionManager connManager = get(ConnectionManager.class);

        print("--- Connections established ---");
        print(connManager.databaseToString(detailed));
    }
}