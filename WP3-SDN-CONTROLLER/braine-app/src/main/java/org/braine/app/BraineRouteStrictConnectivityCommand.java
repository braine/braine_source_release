package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.*;
import org.onosproject.net.device.DeviceService;

import java.util.*;

/**
 * Sample Apache Karaf CLI command
 */
@Service
@Command(scope = "onos", name = "braine-connectivity-route-strict",
        description = "Route a connection along the strictly specified path")
public class BraineRouteStrictConnectivityCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the connectivity", required = true, multiValued = false)
    @Completion(ConnectivityNameCompleter.class)
    String connectivityName = null;

    @Argument(index = 1, name = "path", description = "Path to be applied", required = true, multiValued = true)
    @Completion(DeviceCompleter.class)
    List<String> devIdStrings = null;

    @Override
    protected void doExecute() {
        DeviceService deviceService = get(DeviceService.class);
        ConnectionManager connManager = get(ConnectionManager.class);

        List<DeviceId> deviceIds = new ArrayList<DeviceId>();

        //TODO fore rerouting bisideractional
        List<DeviceId> reverseDeviceIds = new ArrayList<DeviceId>();
        List<Link> reverseSuggestedPath = new ArrayList<Link>();

        ConnectionData connectionData = connManager.getFromDatabase(connectivityName);

        if (connectionData == null) {
            print("--- [ERROR] connection not found", connectivityName);
            return;
        }

        //Build a list of device Ids
        for (String deviceString : devIdStrings) {
            DeviceId deviceId;

            deviceId = getDeviceByName(deviceService, deviceString);
            if (deviceId == null) {
                deviceId = DeviceId.deviceId(deviceString);
            }

            if (deviceId == null) {
                print("--- [ERROR] device not found", deviceString);
                return;
            }
            
            deviceIds.add(deviceId);
        }

        List<DeviceId> computedPath;
        try {
            computedPath = connectionData.rerouteStrict(deviceIds);
        } catch (IllegalArgumentException e) {
            print("--- [ERROR] %s", e);
            return;
        }
        print("--- [SUCCESS] computed path %s", computedPath);
        print("--- [SUCCESS] new path submitted for connection %s", connectivityName);
    }

    private DeviceId getDeviceByName(DeviceService service, String name) {

        Iterator<Device> it = service.getDevices().iterator();
        while (it.hasNext()) {
            Device currentDevice = it.next();
            if (currentDevice.annotations().value(AnnotationKeys.NAME) != null) {
                if (currentDevice.annotations().value(AnnotationKeys.NAME).equals(name)) {
                    return currentDevice.id();
                }
            }
        }
        return null;
    }
}