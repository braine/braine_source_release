package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.provider.ProviderId;

@Service
@Command(scope = "onos", name = "braine-device-rename",
        description = "Add a new host to the network")
public class BraineRenameDeviceCommand extends AbstractShellCommand {
    static final ProviderId PID = new ProviderId("cli", "org.onosproject.cli");

    @Argument(index = 0, name = "device", description = "Device ID", required = true, multiValued = false)
    @Completion(DeviceCompleter.class)
    String stringDevice = null;

    @Argument(index = 1, name = "name", description = "Friendly name to be assigned", required = false, multiValued = false)
    String stringName = null;

    @Override
    protected void doExecute() {
        DevicesManager devicemanager = get(DevicesManager.class);

        devicemanager.annotateDevice(stringDevice, stringName);
    }
}
