package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.DeviceId;
import org.onosproject.net.device.DeviceService;

@Service
@Command(scope = "onos", name = "braine-device-delete-degraded",
        description = "Un-degrade a device")
public class BraineDeleteDegradedDeviceCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the connectivity", required = true, multiValued = false)
    @Completion(DeviceCompleter.class)
    String deviceString;

    @Override
    protected void doExecute() {
        //TBD
        DevicesManager deviceManager = get(DevicesManager.class);
        DeviceService deviceService = get(DeviceService.class);

        DeviceId deviceId;
        deviceId = deviceManager.getDeviceByName(deviceService, deviceString);

        if (deviceId == null) {
            deviceId = DeviceId.deviceId(deviceString);
        }

        if (deviceManager.getFromDatabase(deviceId) == null) {
            print("--- [ERROR] device was not degraded");
            return;
        }

        deviceManager.removeFromDatabase(deviceId);

        print("--- [SUCCESS] device is now ok");
    }
}