package org.braine.app;

import com.google.common.collect.Lists;
import org.onlab.packet.*;
import org.onosproject.cli.net.EthType;
import org.onosproject.cli.net.IpProtocol;
import org.onosproject.common.DefaultTopology;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.*;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.host.HostService;
import org.onosproject.net.intent.*;
import org.onosproject.net.intent.constraint.EncapsulationConstraint;
import org.onosproject.net.link.LinkService;
import org.onosproject.net.topology.Topology;
import org.onosproject.net.topology.TopologyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.onosproject.cli.AbstractShellCommand.get;
import static org.onosproject.net.flow.DefaultTrafficTreatment.builder;

public class ConnectionData {
    private ApplicationId appId;

    private String name;

    public Host hostOne;
    public Host hostTwo;

    public PointToPointIntent intentOne;
    public PointToPointIntent intentTwo;

    //high-level options
    public int priority = 100;
    public boolean bidirectional = false;
    public int minBandwidth = 0; //kbps
    public int maxBandwidth = 0; //kbps
    public int minLatency = 0; //nanoseconds
    public int maxLatency = 0; //nanoseconds

    public boolean monitoringLatency = false; //When latency monitoring is running
    public int monitoringFlowId = 0;
    public String monitoringUrl = null;

    public boolean isLocal = false;
    public boolean degraded = false; //When a connection uses a degraded node

    //Selector options
    public EthType ethType = null;
    public VlanId vlanIngress = null;
    public IpPrefix ipSrc = null;
    public IpPrefix ipDst = null;
    public IpProtocol ipProto =  null;
    public TpPort tcpSrc = null;
    public TpPort tcpDst = null;
    public TpPort udpSrc = null;
    public TpPort udpDst = null;

    //Treatment options
    public boolean vlanPopEgress = false;
    public VlanId vlanPushEgress = null;
    public VlanId vlanSetEgress = null;

    //Constraints
    public VlanId vlanEncapsulation = null;
    public MplsLabel mplsEncapsulation = null;

    //Data for latency monitoring along IntentOne
    public DeviceId firstDeviceOne;
    public PortNumber firstPortOne;
    public VlanId vlanOne;
    public long lastMeasuredLatencyOne;
    public long lastWatchDogSentOne;
    public long lastWatchDogReceivedOne;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Constructs a VPLS data by given name and encapsulation type.
     *
     * @param name the given name
     * @param one ingress host
     * @param two egress host
     */
    public ConnectionData(String name, Host one, Host two) {
        this.name = name;
        this.hostOne = one;
        this.hostTwo = two;
    }

    public String getName() {
        return name;
    }

    public String toString(boolean detailed) {
        String data;

        if (detailed) {
            if (bidirectional) {
                data = "[" + name
                        + " - bidirectional"
                        + " - from " + hostOne.ipAddresses().toString() + " to " + hostTwo.ipAddresses().toString()
                        + " intents: " + intentOne.key() + " - " + intentTwo.key().toString()
                        + " minBandwidth: " + minBandwidth
                        + " maxBandwidth: " + maxBandwidth
                        + " minLatency: " + minLatency
                        + " maxLatency: " + maxLatency;
            } else {
                data = "[" + name
                        + " - from " + hostOne.ipAddresses().toString() + " to " + hostTwo.ipAddresses().toString()
                        + " intents: " + intentOne.key()
                        + " minBandwidth: " + minBandwidth
                        + " maxBandwidth: " + maxBandwidth
                        + " minLatency: " + minLatency
                        + " maxLatency: " + maxLatency;
            }

            if (this.monitoringLatency) {
                data = data + " telemetryFlowId: " + this.monitoringFlowId + "]";
            } else {
                data = data + "]";
            }
        } else {
            if (bidirectional) {
                data = "[" + name
                        + " - bidirectional"
                        + " - from " + hostOne.ipAddresses().toString() + " to " + hostTwo.ipAddresses().toString();
            } else {
                data = "[" + name
                        + " - from " + hostOne.ipAddresses().toString() + " to " + hostTwo.ipAddresses().toString();
            }

            if (this.monitoringLatency) {
                data = data + " telemetryFlowId: " + this.monitoringFlowId + "]";
            } else {
                data = data + "]";
            }
        }

        return data;
    }

    private String milliseconds(long nanoseconds) {
        double micros, millis;

        micros = nanoseconds / 1000;
        millis = micros / 1000;

        return String.valueOf(millis);
    }

    protected boolean checkBandwidth(List<Link> path, int bandwidth) {
        LinkManager linkManager = get(LinkManager.class);

        if (bandwidth == 0) {
            return true;
        }

        //Check if all links has available bandwidth
        for (Iterator<Link> it = path.iterator(); it.hasNext();) {
            LinkData currentLinkData = linkManager.getFromDatabase(it.next());

            if (bandwidth > currentLinkData.bandwidth) {
                return false;
            }
        }
        return true;
    }

    protected boolean allocateBandwidth(List<Link> path, int bandwidth) {
        LinkManager linkManager = get(LinkManager.class);

        if (bandwidth == 0) {
            return true;
        }

        //Allocate bandwidth
        for (Iterator<Link> it = path.iterator(); it.hasNext();) {
            LinkData currentLinkData = linkManager.getFromDatabase(it.next());

            currentLinkData.bandwidth = currentLinkData.bandwidth - bandwidth;
        }
        return true;
    }

    protected void releaseBandwidth(int bandwidth) {
        LinkService linkService = get(LinkService.class);
        LinkManager linkManager = get(LinkManager.class);

        if (bandwidth == 0) {
            return;
        }

        Iterable<Link> links = linkService.getLinks();
        links.forEach(currentLink -> {
            if (intentOne.suggestedPath().contains(currentLink)) {
                LinkData linkData = linkManager.getFromDatabase(currentLink);
                linkData.bandwidth = linkData.bandwidth + minBandwidth;
            }
            if (bidirectional) {
                if (intentTwo.suggestedPath().contains(currentLink)) {
                    LinkData linkData = linkManager.getFromDatabase(currentLink);
                    linkData.bandwidth = linkData.bandwidth + minBandwidth;
                }
            }
        });
    }

    //Computes all paths between source and destination
    protected void computeAllPaths(DefaultTopology topology, DeviceId src, DeviceId dst) {
        Set<Path> computedPaths = topology.getKShortestPaths(src, dst, 10);

        log.info("Number of computed paths {}", computedPaths.size());
    }

    protected Path computeSinglePath(Topology topology, DeviceId src, DeviceId dst) {
        TopologyService topologyService = get(TopologyService.class);
        DevicesManager devicesManager = get(DevicesManager.class);

        Path selectedPath;

        //--- Get the list of disjoint paths using topology service -------------------------
        Set<Path> computedPaths =
                topologyService.getPaths(topology, src, dst);
        //-----------------------------------------------------------------------------------

        log.warn("A set of {} paths has been computed", computedPaths.size());

        if (computedPaths.size() != 0) {
            selectedPath = computedPaths.iterator().next();
        } else {
            log.error("A path is not available");
            return null;
        }

        return selectedPath;
    }

    //Computes a pair of disjoint paths, if primary is degraded use the backup
    //TODO: to be improved with smarter routing, for avoiding all degraded nodes
    protected Path computeSafePathDisjoint(Topology topology, DeviceId src, DeviceId dst) {
        TopologyService topologyService = get(TopologyService.class);
        DevicesManager devicesManager = get(DevicesManager.class);

        DisjointPath selectedDisjointPath;

        //--- Get the list of disjoint paths using topology service -------------------------
        Set<DisjointPath> computedPaths =
                topologyService.getDisjointPaths(topology, src, dst);
        //-----------------------------------------------------------------------------------

        log.warn("A set of {} paths has been computed", computedPaths.size());

        if (computedPaths.size() != 0) {
            selectedDisjointPath = computedPaths.iterator().next();
        } else {
            log.error("A DisjointPath is not available");
            return null;
        }

        boolean primaryPathDegraded = false;

        for (Link link : selectedDisjointPath.primary().links()) {
            if ((devicesManager.getFromDatabase(link.src().deviceId()) != null) ||
                    (devicesManager.getFromDatabase(link.dst().deviceId()) != null)) {

                log.warn("A degraded link has been identified on the primary path link {}", link);

                primaryPathDegraded = true;
            }
        }

        if (primaryPathDegraded == false) {
            return selectedDisjointPath.primary();
        } else {
            return selectedDisjointPath.backup();
        }
    }

    //Computes a pair of paths, it fails if two paths are not available
    //TODO: to be improved with smarter routing, for avoiding all degraded nodes
    protected Path computeSafePathTwoShortest(Topology topology, DeviceId src, DeviceId dst) {
        TopologyService topologyService = get(TopologyService.class);
        DevicesManager devicesManager = get(DevicesManager.class);

        Path selectedPath;

        //--- Get the list of disjoint paths using topology service -------------------------
        Stream<Path> computedPathsStream =
                topologyService.getKShortestPaths(topology, src, dst);

        List<Path> computedPaths = computedPathsStream.collect(Collectors.toList());
        //-----------------------------------------------------------------------------------

        log.warn("A set of {} paths has been computed", computedPaths.size());

        if (computedPaths.size() == 2) {
            selectedPath = computedPaths.get(1);
        } else {
            log.error("Number of computed paths should be exactly 2");
            return null;
        }

        boolean primaryPathDegraded = false;

        for (Link link : selectedPath.links()) {
            if ((devicesManager.getFromDatabase(link.src().deviceId()) != null) ||
                    (devicesManager.getFromDatabase(link.dst().deviceId()) != null)) {

                log.warn("A degraded link has been identified on the primary path link {}", link);

                primaryPathDegraded = true;
            }
        }

        if (primaryPathDegraded == false) {
            log.info("First path have been selected");
            return selectedPath;
        } else {
            log.info("Second path have been selected");
            return computedPaths.get(0);
        }
    }

    protected Path computeOnePath(Topology topology, DeviceId src, DeviceId dst) {
        TopologyService topologyService = get(TopologyService.class);
        DevicesManager devicesManager = get(DevicesManager.class);

        Path selectedPath;

        //--- Get the list of disjoint paths using topology service -------------------------
        Stream<Path> computedPathsStream =
                topologyService.getKShortestPaths(topology, src, dst);

        List<Path> computedPaths = computedPathsStream.collect(Collectors.toList());
        //-----------------------------------------------------------------------------------

        log.warn("A set of {} paths has been computed", computedPaths.size());

        if (computedPaths.size() == 1) {
            selectedPath = computedPaths.get(0);
        } else {
            log.error("Number of computed paths should be exactly 1");
            return null;
        }

        return selectedPath;
    }

    //Change the path of oldIntent
    protected PointToPointIntent rerouteIntent(PointToPointIntent oldIntent, List<Link> links)
            throws IllegalArgumentException {
        IntentService intentService = get(IntentService.class);

        //Generate the new intent
        PointToPointIntent newIntent = PointToPointIntent.builder()
                .appId(oldIntent.appId())
                .suggestedPath(links)
                .key(oldIntent.key())
                .filteredIngressPoint(oldIntent.filteredIngressPoint())
                .filteredEgressPoint(oldIntent.filteredEgressPoint())
                .selector(oldIntent.selector())
                .treatment(oldIntent.treatment())
                .priority(oldIntent.priority())
                .constraints(oldIntent.constraints())
                .build();

        //Submit new intent
        intentService.submit(newIntent);

        return newIntent;
    }

    //This compute the path and push the intent with flow rules
    protected boolean allocateConnection() throws IllegalArgumentException {
        TopologyService topologyService = get(TopologyService.class);

        Path directPath = null;
        Path reversePath = null;

        //--- Special case where hosts are connected to the same device
        if (hostOne.location().deviceId().equals(hostTwo.location().deviceId())) {

            //Connection is local
            isLocal = true;

            //Submit intents
            intentOne = submitLocalIntentOne();
            if (bidirectional) {
                intentTwo = submitLocalIntentTwo();
            }

            return true;
        }

        //--- Direct path computation ------------------------------
        directPath = computeSafePathDisjoint(topologyService.currentTopology(),
                hostOne.location().deviceId(),
                hostTwo.location().deviceId());

        if (directPath == null) {
            throw new IllegalArgumentException("direct path not available");
        }

        //Check bandwidth along direct path ------------------------------
        if (!checkBandwidth(directPath.links(), minBandwidth)) {
            throw new IllegalArgumentException("no bandwidth on direct path");
        }


        //--- Reverse path computation ------------------------------
        if (bidirectional) {
            reversePath = computeSafePathDisjoint(topologyService.currentTopology(),
                    hostTwo.location().deviceId(),
                    hostOne.location().deviceId());

            if (reversePath == null) {
                throw new IllegalArgumentException("reverse path not available");
            }

            if (!checkBandwidth(reversePath.links(), minBandwidth)) {
                throw new IllegalArgumentException("no bandwidth on reverse path");
            }
        }

        //Allocate bandwidth
        allocateBandwidth(directPath.links(), minBandwidth);
        if (bidirectional) {
            allocateBandwidth(reversePath.links(), minBandwidth);
        }

        //Submit intents
        intentOne = submitIntentOne(directPath);
        if (bidirectional) {
            intentTwo = submitIntentTwo(reversePath);
        }

        return true;
    }

    //Submits the intent from hostOne to hostTwo
    protected PointToPointIntent submitLocalIntentOne() {

        HostService hostService = get(HostService.class);
        CoreService coreService = get(CoreService.class);
        IntentService intentService = get(IntentService.class);

        appId = coreService.getAppId("org.matrix.app");

        ConnectPoint hostOneCP = hostService.getHost(hostOne.id()).location();
        ConnectPoint hostTwoCP = hostService.getHost(hostTwo.id()).location();

        log.info("Installing LOCAL intent between {} and {}", hostOneCP, hostTwoCP);

        TrafficSelector.Builder ingressSelectorBuilder = DefaultTrafficSelector.builder();
        TrafficSelector.Builder egressSelectorBuilder = DefaultTrafficSelector.builder();

        TrafficSelector ingressSelector = ingressSelectorBuilder
                .matchEthSrc(hostService.getHost(hostOne.id()).mac())
                .build();

        TrafficSelector egressSelector = egressSelectorBuilder
                .matchEthDst(hostService.getHost(hostTwo.id()).mac())
                .build();

        FilteredConnectPoint hostOneFCP = new FilteredConnectPoint(hostOneCP, ingressSelector);
        FilteredConnectPoint hostTwoFCP = new FilteredConnectPoint(hostTwoCP, egressSelector);

        PointToPointIntent intent = PointToPointIntent.builder()
                .appId(appId)
                .key(null)
                .priority(priority)
                .filteredIngressPoint(hostOneFCP)
                .filteredEgressPoint(hostTwoFCP)
                .selector(buildDirectSelector())
                .treatment(buildTreatment())
                .constraints(buildConstraints())
                .build();

        log.info("Intent ONE has submitted {}", intent);

        //Used for latency monitoring
        int vlanKey = Integer.decode(intent.key().toString());
        int vlan = vlanKey + 1000;
        this.vlanOne = VlanId.vlanId((short) vlan);

        intentService.submit(intent);

        return intent;
    }

    //Submits the intent from hostTwo to hostOne - for bidirectional
    protected PointToPointIntent submitLocalIntentTwo() {

        HostService hostService = get(HostService.class);
        CoreService coreService = get(CoreService.class);
        IntentService intentService = get(IntentService.class);

        appId = coreService.getAppId("org.matrix.app");

        ConnectPoint hostOneCP = hostService.getHost(hostOne.id()).location();
        ConnectPoint hostTwoCP = hostService.getHost(hostTwo.id()).location();

        log.info("Installing LOCAL intent between {} and {}", hostTwoCP, hostOneCP);

        TrafficSelector.Builder ingressSelectorBuilder = DefaultTrafficSelector.builder();
        TrafficSelector.Builder egressSelectorBuilder = DefaultTrafficSelector.builder();

        TrafficSelector ingressSelector = ingressSelectorBuilder
                .matchEthSrc(hostService.getHost(hostTwo.id()).mac())
                .build();

        TrafficSelector egressSelector = egressSelectorBuilder
                .matchEthDst(hostService.getHost(hostOne.id()).mac())
                .build();

        FilteredConnectPoint hostOneFCP = new FilteredConnectPoint(hostOneCP, ingressSelector);
        FilteredConnectPoint hostTwoFCP = new FilteredConnectPoint(hostTwoCP, egressSelector);

        PointToPointIntent intent = PointToPointIntent.builder()
                .appId(appId)
                .key(null)
                .priority(priority)
                .filteredIngressPoint(hostTwoFCP)
                .filteredEgressPoint(hostOneFCP)
                .selector(buildReverseSelector())
                .treatment(buildTreatment())
                .constraints(buildConstraints())
                .build();

        log.info("Intent TWO has been submitted {}", intent);

        intentService.submit(intent);

        return intent;
    }

    //Submits the intent from hostOne to hostTwo
    protected PointToPointIntent submitIntentOne(Path directPath) {

        HostService hostService = get(HostService.class);
        CoreService coreService = get(CoreService.class);
        IntentService intentService = get(IntentService.class);

        appId = coreService.getAppId("org.matrix.app");

        ConnectPoint hostOneCP = hostService.getHost(hostOne.id()).location();
        ConnectPoint hostTwoCP = hostService.getHost(hostTwo.id()).location();

        log.info("Installing intent between {} and {} along path {}", hostOneCP, hostTwoCP, directPath);

        TrafficSelector.Builder ingressSelectorBuilder = DefaultTrafficSelector.builder();
        TrafficSelector.Builder egressSelectorBuilder = DefaultTrafficSelector.builder();

        TrafficSelector ingressSelector = ingressSelectorBuilder
                .matchEthSrc(hostService.getHost(hostOne.id()).mac())
                .build();

        TrafficSelector egressSelector = egressSelectorBuilder
                .matchEthDst(hostService.getHost(hostTwo.id()).mac())
                .build();

        FilteredConnectPoint hostOneFCP = new FilteredConnectPoint(hostOneCP, ingressSelector);
        FilteredConnectPoint hostTwoFCP = new FilteredConnectPoint(hostTwoCP, egressSelector);

        PointToPointIntent intent = PointToPointIntent.builder()
                .appId(appId)
                .suggestedPath(directPath.links())
                .key(null)
                .priority(priority)
                .filteredIngressPoint(hostOneFCP)
                .filteredEgressPoint(hostTwoFCP)
                .selector(buildDirectSelector())
                .treatment(buildTreatment())
                .constraints(buildConstraints())
                .build();

        log.info("Intent ONE has submitted {}", intent);

        //Used for latency monitoring
        int vlanKey = Integer.decode(intent.key().toString());
        int vlan = vlanKey + 1000;
        this.vlanOne = VlanId.vlanId((short) vlan);

        intentService.submit(intent);

        return intent;
    }

    //Submits the intent from hostTwo to hostOne - for bidirectional
    protected PointToPointIntent submitIntentTwo(Path reversePath) {

        HostService hostService = get(HostService.class);
        CoreService coreService = get(CoreService.class);
        IntentService intentService = get(IntentService.class);

        appId = coreService.getAppId("org.matrix.app");

        ConnectPoint hostOneCP = hostService.getHost(hostOne.id()).location();
        ConnectPoint hostTwoCP = hostService.getHost(hostTwo.id()).location();

        log.info("Installing intent between {} and {} along path {}", hostTwoCP, hostOneCP, reversePath);

        TrafficSelector.Builder ingressSelectorBuilder = DefaultTrafficSelector.builder();
        TrafficSelector.Builder egressSelectorBuilder = DefaultTrafficSelector.builder();

        TrafficSelector ingressSelector = ingressSelectorBuilder
                .matchEthSrc(hostService.getHost(hostTwo.id()).mac())
                .build();

        TrafficSelector egressSelector = egressSelectorBuilder
                .matchEthDst(hostService.getHost(hostOne.id()).mac())
                .build();

        FilteredConnectPoint hostOneFCP = new FilteredConnectPoint(hostOneCP, ingressSelector);
        FilteredConnectPoint hostTwoFCP = new FilteredConnectPoint(hostTwoCP, egressSelector);

        PointToPointIntent intent = PointToPointIntent.builder()
                .appId(appId)
                .suggestedPath(reversePath.links())
                .key(null)
                .priority(priority)
                .filteredIngressPoint(hostTwoFCP)
                .filteredEgressPoint(hostOneFCP)
                .selector(buildReverseSelector())
                .treatment(buildTreatment())
                .constraints(buildConstraints())
                .build();

        log.info("Intent TWO has been submitted {}", intent);

        intentService.submit(intent);

        return intent;
    }

    //Build a new path starting from a loose list of devices
    //Perform several connectivity checks
    public List<DeviceId> rerouteLoose(List<DeviceId> loosePath)
            throws IllegalArgumentException {

        HostService hostService = get(HostService.class);
        TopologyService topologyService = get(TopologyService.class);
        LinkService linkService = get(LinkService.class);

        List<Link> suggestedPathLinks = new ArrayList<Link>();
        List<DeviceId> suggestedPathDevices = new ArrayList<DeviceId>();

        //Check connection between hostOne and first device
        if (!hostService.getConnectedHosts(loosePath.get(0)).contains(this.hostOne)) {
            throw new IllegalArgumentException("hostOne is not connected to the first specified device");
        }

        //Check connection between hostTwo and last device
        int nodeListSize = loosePath.size();
        if (!hostService.getConnectedHosts(loosePath.get(nodeListSize-1)).contains(this.hostTwo)) {
            throw new IllegalArgumentException("hostTwo is not connected to the last specified device");
        }

        //Check for loops in the specified list of nodes
        for (int i=0; i < nodeListSize; i++) {
            for (int j=0; j < nodeListSize; j++) {
                if (i != j) {
                    if (loosePath.get(i).equals(loosePath.get(j))) {
                        throw new IllegalArgumentException("the specified list of nodes includes a loop");
                    }
                }
            }
        }

        //Compute the path traversing the specified nodes
        for (int i=0; i < nodeListSize - 1; i++) {
            Path segment = topologyService.getPaths(topologyService.currentTopology(),
                    loosePath.get(i),
                    loosePath.get(i+1)).iterator().next();

            suggestedPathLinks.addAll(segment.links());
        }

        //Check for loops in the computed path - build list of devices
        int suggestedPathSize = suggestedPathLinks.size();
        for (int i=0; i < suggestedPathSize; i++) {
            if (i==0) {
                suggestedPathDevices.add(suggestedPathLinks.get(i).src().deviceId());
            }
            suggestedPathDevices.add(suggestedPathLinks.get(i).dst().deviceId());
        }

        //Check for loops in the computed path
        for (int i=0; i < suggestedPathDevices.size(); i++) {
            for (int j=0; j < suggestedPathDevices.size(); j++) {
                if (i != j) {
                    if (suggestedPathDevices.get(i).equals(suggestedPathDevices.get(j))) {
                        throw new IllegalArgumentException("the computed path includes a loop");
                    }
                }
            }
        }

        //Check bandwidth along direct path ------------------------------
        if (!checkBandwidth(suggestedPathLinks, minBandwidth)) {
            throw new IllegalArgumentException("reroute no bandwidth on direct path");
        }

        //Reverse the path and do the same for IntentTwo
        List<Link> reversePathLinks = new ArrayList<Link>();
        if (this.bidirectional) {
            //Reverse the list of devices
            List<DeviceId> reversePathDevices = Lists.reverse(suggestedPathDevices);

            //Generate reverse list of links
            int pathSize = reversePathDevices.size();
            for (int i=0; i < pathSize - 1; i++) {
                Set<Link> egressLinks = linkService.getDeviceEgressLinks(reversePathDevices.get(i));
                Set<Link> ingressLinks = linkService.getDeviceIngressLinks(reversePathDevices.get(i+1));

                Set<Link> intersection = new HashSet<Link>(egressLinks);
                intersection.retainAll(ingressLinks);

                if (intersection.isEmpty()) {
                    throw new IllegalArgumentException("device "
                            + reversePathDevices.get(i).toString() + " is not connected to device "
                            + reversePathDevices.get(i+1).toString());
                } else {
                    reversePathLinks.add(intersection.iterator().next());
                }
            }

            //Check bandwidth along reverse path ------------------------------
            if (!checkBandwidth(reversePathLinks, minBandwidth)) {
                throw new IllegalArgumentException("reroute no bandwidth on reverse path");
            }
        }

        //Release bandwidth on old path
        releaseBandwidth(minBandwidth);

        //Allocate bandwidth
        allocateBandwidth(suggestedPathLinks, minBandwidth);
        if (bidirectional) {
            allocateBandwidth(reversePathLinks, minBandwidth);
        }

        //Submit intents
        intentOne = rerouteIntent(intentOne, suggestedPathLinks);
        if (bidirectional) {
            intentTwo = rerouteIntent(intentTwo, reversePathLinks);
        }

        return suggestedPathDevices;
    }

    //Build a new path starting from a strict list of devices
    //Perform several connectivity checks
    public List<DeviceId> rerouteStrict(List<DeviceId> strictPath)
            throws IllegalArgumentException {

        HostService hostService = get(HostService.class);
        LinkService linkService = get(LinkService.class);

        List<Link> suggestedPathLinks = new ArrayList<Link>();
        List<DeviceId> suggestedPathDevices = new ArrayList<DeviceId>();

        //TODO apply to bidirectional

        //Check connection between hostOne and first device
        if (!hostService.getConnectedHosts(strictPath.get(0)).contains(this.hostOne)) {
            throw new IllegalArgumentException("hostOne is not connected to the first specified device");
        }

        //Check connection between hostTwo and last device
        int nodeListSize = strictPath.size();
        if (!hostService.getConnectedHosts(strictPath.get(nodeListSize-1)).contains(this.hostTwo)) {
            throw new IllegalArgumentException("hostTwo is not connected to the last specified device");
        }

        //Check for loops in the specified list of nodes
        for (int i=0; i < nodeListSize; i++) {
            for (int j=0; j < nodeListSize; j++) {
                if (i != j) {
                    if (strictPath.get(i).equals(strictPath.get(j))) {
                        throw new IllegalArgumentException("the specified list of nodes includes a loop");
                    }
                }
            }
        }

        //Check connection between each pair of consecutive devices
        int pathSize = strictPath.size();
        for (int i=0; i < pathSize - 1; i++) {
            Set<Link> egressLinks = linkService.getDeviceEgressLinks(strictPath.get(i));
            Set<Link> ingressLinks = linkService.getDeviceIngressLinks(strictPath.get(i+1));

            Set<Link> intersection = new HashSet<Link>(egressLinks);
            intersection.retainAll(ingressLinks);

            if (intersection.isEmpty()) {
                throw new IllegalArgumentException("device "
                + strictPath.get(i).toString() + " is not connected to device "
                + strictPath.get(i+1).toString());
            } else {
                suggestedPathLinks.add(intersection.iterator().next());
            }
        }

        //Check for loops in the computed path - build list of devices
        int suggestedPathSize = suggestedPathLinks.size();
        for (int i=0; i < suggestedPathSize; i++) {
            if (i==0) {
                suggestedPathDevices.add(suggestedPathLinks.get(i).src().deviceId());
            }
            suggestedPathDevices.add(suggestedPathLinks.get(i).dst().deviceId());
        }

        //Check for loops in the computed path
        for (int i=0; i < suggestedPathDevices.size(); i++) {
            for (int j=0; j < suggestedPathDevices.size(); j++) {
                if (i != j) {
                    if (suggestedPathDevices.get(i).equals(suggestedPathDevices.get(j))) {
                        throw new IllegalArgumentException("the computed path includes a loop");
                    }
                }
            }
        }

        //Check bandwidth along direct path ------------------------------
        if (!checkBandwidth(suggestedPathLinks, minBandwidth)) {
            throw new IllegalArgumentException("reroute no bandwidth on direct path");
        }

        //Reverse the path and do the same for IntentTwo
        List<Link> reversePathLinks = new ArrayList<Link>();
        if (this.bidirectional) {
            //Reverse the list of devices
            List<DeviceId> reversePathDevices = Lists.reverse(suggestedPathDevices);

            //Check generate reverse list of links
            int reversePathSize = reversePathDevices.size();
            for (int i=0; i < reversePathSize - 1; i++) {
                Set<Link> egressLinks = linkService.getDeviceEgressLinks(reversePathDevices.get(i));
                Set<Link> ingressLinks = linkService.getDeviceIngressLinks(reversePathDevices.get(i+1));

                Set<Link> intersection = new HashSet<Link>(egressLinks);
                intersection.retainAll(ingressLinks);

                if (intersection.isEmpty()) {
                    throw new IllegalArgumentException("device "
                            + reversePathDevices.get(i).toString() + " is not connected to device "
                            + reversePathDevices.get(i+1).toString());
                } else {
                    reversePathLinks.add(intersection.iterator().next());
                }
            }

            //Check bandwidth along reverse path ------------------------------
            if (!checkBandwidth(reversePathLinks, minBandwidth)) {
                throw new IllegalArgumentException("reroute no bandwidth on reverse path");
            }
        }

        //Release bandwidth on old path
        releaseBandwidth(minBandwidth);

        //Allocate bandwidth
        allocateBandwidth(suggestedPathLinks, minBandwidth);
        if (bidirectional) {
            allocateBandwidth(reversePathLinks, minBandwidth);
        }

        //Submit intents
        intentOne = rerouteIntent(intentOne, suggestedPathLinks);
        if (bidirectional) {
            intentTwo = rerouteIntent(intentTwo, reversePathLinks);
        }

        return suggestedPathDevices;

    }

    protected TrafficSelector buildDirectSelector() {

        TrafficSelector.Builder selectorBuilder = DefaultTrafficSelector.builder();
        selectorBuilder.matchEthSrc(this.hostOne.mac());
        selectorBuilder.matchEthDst(this.hostTwo.mac());

        if (this.ethType != null) {
            selectorBuilder.matchEthType(this.ethType.value());
        }

        if (this.vlanIngress != null) {
            selectorBuilder.matchVlanId(this.vlanIngress);
        }

        if (this.ipSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPSrc(this.ipSrc);
        }

        if (this.ipDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPDst(this.ipDst);
        }

        if (this.ipProto != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol((byte) this.ipProto.value());
        }

        if (this.tcpSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_TCP)
                    .matchTcpSrc(this.tcpSrc);
        }

        if (this.tcpDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_TCP)
                    .matchTcpDst(this.tcpDst);
        }

        if (this.udpSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_UDP)
                    .matchUdpSrc(this.udpSrc);
        }

        if (this.udpDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_UDP)
                    .matchUdpDst(this.udpDst);
        }

        return selectorBuilder.build();
    }

    protected TrafficSelector buildReverseSelector() {

        TrafficSelector.Builder selectorBuilder = DefaultTrafficSelector.builder();
        selectorBuilder.matchEthSrc(this.hostTwo.mac());
        selectorBuilder.matchEthDst(this.hostOne.mac());

        if (this.ethType != null) {
            selectorBuilder.matchEthType(this.ethType.value());
        }

        if (this.vlanIngress != null) {
            selectorBuilder.matchVlanId(this.vlanIngress);
        }

        if (this.ipSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPDst(this.ipSrc);
        }

        if (this.ipDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPSrc(this.ipDst);
        }

        if (this.ipProto != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol((byte) this.ipProto.value());
        }

        if (this.tcpSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_TCP)
                    .matchTcpDst(this.tcpSrc);
        }

        if (this.tcpDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_TCP)
                    .matchTcpSrc(this.tcpDst);
        }

        if (this.udpSrc != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_UDP)
                    .matchUdpDst(this.udpSrc);
        }

        if (this.udpDst != null) {
            selectorBuilder.matchEthType(Ethernet.TYPE_IPV4)
                    .matchIPProtocol(IPv4.PROTOCOL_UDP)
                    .matchUdpSrc(this.udpDst);
        }

        return selectorBuilder.build();
    }

    protected TrafficTreatment buildTreatment() {

        TrafficTreatment.Builder treatmentBuilder = builder();

        if (this.vlanPopEgress) {
            treatmentBuilder.popVlan();
        }
        if (this.vlanPushEgress != null) {
            treatmentBuilder.pushVlan();
            treatmentBuilder.setVlanId(this.vlanPushEgress);
        }

        if (this.vlanSetEgress != null) {
            treatmentBuilder.setVlanId(this.vlanSetEgress);
        }

        return treatmentBuilder.build();
    }

    protected List<Constraint> buildConstraints() {
        List<Constraint> constraints = new LinkedList<>();

        if (this.vlanEncapsulation != null) {
            EncapsulationConstraint constraint = new EncapsulationConstraint(
                    EncapsulationType.valueOf("VLAN"),
                    this.vlanEncapsulation);

            constraints.add(constraint);
        }

        if (this.mplsEncapsulation != null) {
            EncapsulationConstraint constraint = new EncapsulationConstraint(
                    EncapsulationType.valueOf("MPLS"),
                    this.mplsEncapsulation);

            constraints.add(constraint);
        }

        return constraints;
    }

    protected void deleteConnectivity() throws IllegalArgumentException {
        ConnectionManager connManager = get(ConnectionManager.class);
        IntentService intentService = get(IntentService.class);
        LatencyManager component = get(LatencyManager.class);

        if (connManager.getFromDatabase(name) == null) {
            throw new IllegalArgumentException("connection not found");
        }

        //remove vlan flowrule
        if (monitoringLatency) {
            component.stopLatencyMonitor(name);
        }

        //release bandwidth
        releaseBandwidth(minBandwidth);

        //withdraw intents
        if (bidirectional) {
            intentService.withdraw(intentOne);
            intentService.withdraw(intentTwo);
        } else {
            intentService.withdraw(intentOne);
        }

        connManager.removeFromDatabase(name);
    }
}
