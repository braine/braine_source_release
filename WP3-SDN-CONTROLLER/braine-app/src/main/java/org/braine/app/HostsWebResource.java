package org.braine.app;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onosproject.net.*;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.host.DefaultHostDescription;
import org.onosproject.net.host.HostService;
import org.onosproject.net.topology.TopologyService;
import org.onosproject.rest.AbstractWebResource;

import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;

import static org.onlab.util.Tools.readTreeFromStream;

/**
 * Hosts commands interface.
 */
@Path("hosts")
public class HostsWebResource extends AbstractWebResource {

    /**
     * Get list of hosts.
     *
     * @return 200 OK
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHosts() {
        HostService hostService = get(HostService.class);
        DeviceService deviceService = get(DeviceService.class);

        ArrayNode arrayFlows = mapper().createArrayNode();

        deviceService.getDevices().forEach(device -> {
            hostService.getConnectedHosts(device.id()).forEach( host -> {
                ObjectNode objectNode = mapper().createObjectNode();

                objectNode.put("name", host.annotations().value(AnnotationKeys.NAME));
                objectNode.put("IPs", host.ipAddresses().toString());
                objectNode.put("MAC", host.mac().toString());
                objectNode.put("VLAN", host.vlan().toString());
                objectNode.put("location device", host.location().deviceId().toString());
                objectNode.put("location port", host.location().port().toString());

                arrayFlows.add(objectNode);
            });
        });

        ObjectNode root = this.mapper().createObjectNode().putPOJO("Hosts", arrayFlows);
        return ok(root).build();
    }

    /**
     * Rename the specified host.
     *
     * @return 200 OK
     */
    @POST
    @Path("rename/{hostIP}/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response renameHost(@PathParam("hostIP") String hostIpString,
                               @PathParam("name") String name) {

        HostManager hostManager = get(HostManager.class);
        hostManager.annotateHost(hostIpString, name);

        return Response.ok().build();
    }

    /**
     * Create a new host.
     *
     * @param stream input json
     * @return
     * @onos.rsModel addHost
     */
    @POST
    @Path("add/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createHost(InputStream stream,
                               @PathParam("name") String name) {
        HostManager hostManager = get(HostManager.class);
        DeviceService deviceService = get(DeviceService.class);
        TopologyService topologyService = get(TopologyService.class);

        try {
            // Parse the input stream
            ObjectNode root = readTreeFromStream(mapper(), stream);

            Host host = codec(Host.class).decode(root, HostsWebResource.this);

            // Check the specified device exist
            if (deviceService.getDevice(host.location().deviceId()) == null) {
                throw new IllegalArgumentException("Device is not found");
            }

            HostId hostId = host.id();
            DefaultHostDescription desc = new DefaultHostDescription(
                    host.mac(),
                    host.vlan(),
                    host.locations(),
                    host.ipAddresses(),
                    host.innerVlan(),
                    host.tpid(),
                    true,
                    (SparseAnnotations) host.annotations());

            hostManager.configureHost(hostId, desc);
            hostManager.annotateHost(host.ipAddresses().iterator().next().toString(), name);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }

        return Response.ok().build();
    }
}
