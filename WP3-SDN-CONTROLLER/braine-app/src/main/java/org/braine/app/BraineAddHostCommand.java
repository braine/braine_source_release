package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onlab.packet.IpAddress;
import org.onlab.packet.MacAddress;
import org.onlab.packet.VlanId;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.*;
import org.onosproject.net.host.*;

import java.util.*;

@Service
@Command(scope = "onos", name = "braine-host-add",
        description = "Add a new host to the network")
public class BraineAddHostCommand extends AbstractShellCommand {
    @Argument(index = 0, name = "location", description = "Host connection point", required = true, multiValued = false)
    @Completion(ConnectPointCompleter.class)
    String locationString;

    @Argument(index = 1, name = "name", description = "Host friendly name", required = true, multiValued = false)
    String nameString;

    @Argument(index = 2, name = "mac", description = "Mac address", required = true, multiValued = false)
    String macString;

    @Argument(index = 3, name = "vlan", description = "VLAN id", required = true, multiValued = false)
    String vlanString;

    @Argument(index = 4, name = "ip", description = "IP address", required = true, multiValued = false)
    String ipString;

    @Override
    protected void doExecute() {
        //Format the input parameters
        MacAddress mac = MacAddress.valueOf(macString);
        VlanId vlan = VlanId.vlanId(vlanString);

        HostLocation location = new HostLocation(ConnectPoint.fromString(locationString), System.currentTimeMillis());
        Set<HostLocation> locations = new HashSet<HostLocation>();
        locations.add(location);

        IpAddress ip = IpAddress.valueOf(ipString);
        Set<IpAddress> ips = new HashSet<IpAddress>();
        ips.add(ip);

        Map<String, String> annotationsMap = new HashMap<>();
        annotationsMap.put(AnnotationKeys.NAME, nameString);
        SparseAnnotations annotations = DefaultAnnotations.builder().putAll(annotationsMap).build();

        HostDescription description = new DefaultHostDescription(
                mac, vlan, locations, ips, true, annotations);

        HostId hostId = HostId.hostId(macString + "/" + vlanString);

        //Use the hostManager to push the configured host
        HostManager hostManager = get(HostManager.class);
        hostManager.configureHost(hostId, description);
    }
}
