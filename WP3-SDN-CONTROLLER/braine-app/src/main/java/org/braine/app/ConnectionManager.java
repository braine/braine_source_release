package org.braine.app;

import org.onosproject.net.intent.Key;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Component(immediate = true, service = ConnectionManager.class)
public class ConnectionManager {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public Map<String, ConnectionData> connectionDB = new HashMap<String, ConnectionData>();

    @Activate
    protected void activate() {
        log.info("ConnectionManager has been STARTED");
    }

    @Deactivate
    protected void deactivate() {
        log.info("ConnectionManager has been STOPPED");
    }

    //Viene chiamata dal comando createMatrixEntry
    public void addToDatabase(String connName, ConnectionData connData) {
        connectionDB.put(connName, connData);
    }

    //Da implementare attraverso un comando specifico ad esempio RemoveMatrixEntry
    public void removeFromDatabase(String connName) {
        connectionDB.remove(connName);
    }

    public ConnectionData getFromDatabase(String connName) {
        return connectionDB.get(connName);
    }

    public ConnectionData getFromIntentKey(Key intentKey) {
        for (String key: connectionDB.keySet()) {
            if (connectionDB.get(key).intentOne.key().equals(intentKey)) {
                return connectionDB.get(key);
            }
        }
        return null;
    }

    public ConnectionData getFromMonitoringFlowId(int flowId) {
        for (String key: connectionDB.keySet()) {
            if (connectionDB.get(key).monitoringFlowId == flowId) {
                return connectionDB.get(key);
            }
        }
        return null;
    }

    public Iterable<ConnectionData> getConnections() {
        List connections = new ArrayList<ConnectionData>();
        for (String key: connectionDB.keySet()) {
           connections.add(connectionDB.get(key));
        }
        return connections;
    }

    public String databaseToString(boolean detailed) {
        String data = "";
        for (String key: connectionDB.keySet()) {
            data = data + "--- " + connectionDB.get(key).toString(detailed) + '\n';
        }

        return data;
    }
}