package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.Option;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.Link;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.intent.PointToPointIntent;
import org.onosproject.net.topology.TopologyService;

import java.util.ArrayList;
import java.util.List;

@Service
@Command(scope = "onos", name = "braine-device-add-degraded",
        description = "Mark a device as degraded")
public class BraineAddDegradedDeviceCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the device", required = true, multiValued = false)
    @Completion(DeviceCompleter.class)
    String deviceString;

    //----------------------
    // Reroute option
    //----------------------
    @Option(name = "-r", aliases = "--reroute", description = "Reroute connectivity using degraded node",
            required = false, multiValued = false)
    private boolean reroute = false;

    @Override
    protected void doExecute() {
        DevicesManager devicesManager = get(DevicesManager.class);
        ConnectionManager connectionManager = get(ConnectionManager.class);
        DeviceService deviceService = get(DeviceService.class);
        TopologyService topologyService = get(TopologyService.class);

        DeviceId deviceId;
        deviceId = devicesManager.getDeviceByName(deviceService, deviceString);

        if (deviceId == null) {
            deviceId = DeviceId.deviceId(deviceString);
        }

        Device device = deviceService.getDevice(deviceId);

        if (device == null) {
            print("--- [ERROR] device not found");
            return;
        }

        devicesManager.addToDatabase(deviceId);

        print("--- [SUCCESS] device %s is now degraded", deviceId.toString());

        if (reroute) {
            for (String key : connectionManager.connectionDB.keySet()) {
                ConnectionData connData = connectionManager.getFromDatabase(key);

                //Only for connections not local
                if (!connData.isLocal) {
                    PointToPointIntent intentOne = connData.intentOne;

                    List<Link> links = intentOne.suggestedPath();
                    for (Link link : links) {
                        if ((devicesManager.getFromDatabase(link.src().deviceId()) != null) ||
                                (devicesManager.getFromDatabase(link.dst().deviceId()) != null)) {
                            connData.degraded = true;
                        }
                    }

                    //Reroute degraded connections
                    if (connData.degraded) {
                        List<Link> suggestedPathLinks = connData.computeSafePathDisjoint(topologyService.currentTopology(),
                                connData.hostOne.location().deviceId(),
                                connData.hostTwo.location().deviceId()).links();

                        //Convert list of links in list of devices
                        List<DeviceId> suggestedPathDevices = new ArrayList<>();
                        for (int i = 0; i < suggestedPathLinks.size(); i++) {
                            if (i == 0) {
                                suggestedPathDevices.add(suggestedPathLinks.get(0).src().deviceId());
                            }
                            suggestedPathDevices.add(suggestedPathLinks.get(i).dst().deviceId());
                        }

                        print("--- [INFO] connectivity new path %s", suggestedPathDevices.toString());

                        connData.rerouteStrict(suggestedPathDevices);

                        //After rerouting the connection is not anymore degraded
                        print("--- [INFO] connectivity %s has been rerouted", connData.getName());
                        connData.degraded = false;
                    }
                }
            }
        }
    }
}