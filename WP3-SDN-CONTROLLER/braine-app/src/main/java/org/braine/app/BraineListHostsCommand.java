package org.braine.app;

import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.AnnotationKeys;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.host.HostService;

@Service
@Command(scope = "onos", name = "braine-host-list",
        description = "Show currently known hosts")
public class BraineListHostsCommand extends AbstractShellCommand {

    @Override
    protected void doExecute() {
        HostService hostService = get(HostService.class);
        DeviceService deviceService = get(DeviceService.class);

        print("--- Known hosts ---");

        deviceService.getDevices().forEach(device -> {
            print("--- Device --- %s", device.id());
            hostService.getConnectedHosts(device.id()).forEach( host -> {
                print("Host: %s %s on port %s",
                        host.annotations().value(AnnotationKeys.NAME),
                        host.ipAddresses().toString(),
                        host.location().port().toString());
            });
        });
    }
}
