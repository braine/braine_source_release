package org.braine.app;

import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Option;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.*;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.link.*;
import org.onosproject.net.provider.ProviderId;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.onosproject.net.LinkKey.linkKey;

/**
 * Sample Apache Karaf CLI command
 */
@Service
@Command(scope = "onos", name = "braine-link-list",
        description = "Show links status including bandwidth")
public class BraineListLinksCommand extends AbstractShellCommand {

    private static final String FMT = "src=%s/%s, dst=%s/%s, state=%s, latency=%s, bandwidth=%s";
    private static final String COMPACT = "%s/%s-%s/%s";
    static final ProviderId PID = new ProviderId("cli", "org.onosproject.cli");

    @Option(name = "--compact", description = "Compact output",
            required = false, multiValued = false)
    private boolean compact = false;

    @Override
    protected void doExecute() {

        print("List of latencies as computed by this application:");

        LinkService linkService = get(LinkService.class);
        DeviceService deviceService = get(DeviceService.class);
        LinkManager linkManager = get(LinkManager.class);

        print(linkManager.databaseToString());

        /*ConnectPoint oneCP = ConnectPoint.fromString(one);
        ConnectPoint twoCP = ConnectPoint.fromString(two);
        Link linkTest = linkService.getLink(oneCP, twoCP);*/

        //Portspeed is expressed in Mbps
        //Latency for transmitting 1500 byte frame is (1500*8/1.000.000) / portspeed
        //Latency for transmitting 64 byte frame is (64*8/1.000.000) / portspeed - can be seen as minimum
        //Entrambi i valori vanno espressi in nanosecondi e quindi moltiplicati per 10e9

        /*LinkProviderRegistry registry = get(LinkProviderRegistry.class);
        CliLinkProvider provider = new CliLinkProvider();
        LinkProviderService providerService = registry.register(provider);

        Iterable<Link> linksAnnotateLatency = linkService.getLinks();
        linksAnnotateLatency.forEach(currentLink -> {
            Port txPort = deviceService.getPort(currentLink.src());

            double minLatency = ((64*8)*1000) / txPort.portSpeed();
            double maxLatency = ((1500*8)*1000) / txPort.portSpeed();

            providerService.linkDetected(description(
                    currentLink,
                    AnnotationKeys.LATENCY,
                    String.valueOf(maxLatency)));

            providerService.linkDetected(description(
                    currentLink,
                    AnnotationKeys.BANDWIDTH,
                    String.valueOf(txPort.portSpeed())));
        });
        registry.unregister(provider);

        Iterable<Link> links = linkService.getLinks();
        if (compact) {
            Tools.stream(links)
                    .sorted(Comparator.comparing(link -> linkKey(link).toString()))
                    .forEach(link -> {
                        print(compactLinkString(link));
                    });
        } else {
            Tools.stream(links)
                    .sorted(Comparator.comparing(link -> linkKey(link).toString()))
                    .forEach(link -> {
                        print(linkString(link));
                    });
        }*/

    }

    /**
     * Returns a formatted string representing the given link.
     *
     * @param link infrastructure link
     * @return formatted link string
     */
    public static String linkString(Link link) {
        return String.format(FMT,
                link.src().deviceId(), link.src().port(),
                link.dst().deviceId(), link.dst().port(),
                link.state(),
                link.annotations().value(AnnotationKeys.LATENCY),
                link.annotations().value(AnnotationKeys.BANDWIDTH));
    }

    /**
     * Returns a compact string representing the given link.
     *
     * @param link infrastructure link
     * @return formatted link string
     */
    public static String compactLinkString(Link link) {
        return String.format(COMPACT,
                link.src().deviceId(), link.src().port(),
                link.dst().deviceId(), link.dst().port());
    }

    /**
     * Generate a new description inserting the new value for annotations.
     * Previous values are not overwritten.
     *
     * @param link
     * @param key
     * @param value
     * @return
     */
    private LinkDescription description(Link link, String key, String value) {
        checkNotNull(key, "Key cannot be null");
        DefaultAnnotations.Builder builder = DefaultAnnotations.builder();
        if (value != null) {
            builder.set(key, value);
        } else {
            builder.remove(key);
        }
        return new DefaultLinkDescription(
                link.src(),
                link.dst(),
                link.type(),
                link.isExpected(),
                builder.build());
    }

    private static final class CliLinkProvider implements LinkProvider {
        @Override
        public ProviderId id() {
            return PID;
        }
    }

}
