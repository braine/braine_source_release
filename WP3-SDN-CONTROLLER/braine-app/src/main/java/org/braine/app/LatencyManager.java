/*
 * Copyright 2019-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.braine.app;

import org.onlab.packet.*;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.*;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.flowobjective.DefaultForwardingObjective;
import org.onosproject.net.flowobjective.FlowObjectiveService;
import org.onosproject.net.flowobjective.ForwardingObjective;
import org.onosproject.net.host.HostService;
import org.onosproject.net.intent.*;
import org.onosproject.net.packet.*;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Skeletal ONOS application component.
 */
@Component(immediate = true, service = LatencyManager.class)
public class LatencyManager {

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected IntentService intentService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected ConnectionManager connectionManager;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected RestUtilities restUtilities;

    private ApplicationId appId;

    private final Logger log = LoggerFactory.getLogger(getClass());

    private InternalIntentListener intentListener = new InternalIntentListener();

    private class InternalIntentListener implements IntentListener {
        @Override
        public void event(IntentEvent event) {
            if (event.type() == IntentEvent.Type.INSTALLED) {
                log.info("EVENT intent {} has been INSTALLED", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.INSTALL_REQ) {
                log.info("EVENT intent {} has been REQUESTED", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.FAILED) {
                log.info("EVENT intent {} has been FAILED", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.WITHDRAW_REQ) {
                log.info("EVENT intent {} has been WITHDRAW_REQ", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.WITHDRAWN) {
                log.info("EVENT intent {} has been WITHDRAWN", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.CORRUPT) {
                log.info("EVENT intent {} has been CORRUPT", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.PURGED) {
                log.info("EVENT intent {} has been PURGED", event.subject().key());
            }
            if (event.type() == IntentEvent.Type.REALLOCATING) {
                log.info("EVENT intent {} has been REALLOCATING", event.subject().key());
            }
        }
    }

    @Activate
    protected void activate() {
        appId = coreService.registerApplication("org.matrix.app");

        log.info("ConnectionManager has been STARTED");
    }

    @Deactivate
    protected void deactivate() {

        //Remove all connections, and withdraw related intents
        for (String key: connectionManager.connectionDB.keySet()) {
            ConnectionData connData = connectionManager.getFromDatabase(key);

            if (connData.monitoringLatency) {
                //Remove vlan flowrule at destination
                stopLatencyMonitor(connData.getName());
            }

            if (connData.bidirectional) {
                intentService.withdraw(connData.intentOne);
                intentService.withdraw(connData.intentTwo);
            } else {
                intentService.withdraw(connData.intentOne);
            }

            //connectionManager.removeFromDatabase(key);
        }

        //Remove intent listener
        intentService.removeListener(intentListener);
        intentListener = null;

        log.info("ConnectionManager has been STOPPED");
    }

    protected boolean stopLatencyMonitor(String connectionDataName) {
        ConnectionData connData = connectionManager.getFromDatabase(connectionDataName);

        //Invoke rest PostCard P4 telemetry app
        try {
            restUtilities.deletePostCardIntent(connData);
        } catch (IOException e) {
            log.error("[LATENCY MONITOR]: monitoring cannot be stopped {}", e.getMessage());
            return false;
        }

        connData.monitoringLatency = false;
        connData.monitoringFlowId = 0;
        connData.monitoringUrl = null;

        log.info("[LATENCY MONITOR]: monitoring of connection {} is now OFF", connectionDataName);
        return true;
    }

    //TODO implement activation of post-card or inband Telemetry
    protected boolean startLatencyMonitor(String connectionDataName, int flowId) {
        ConnectionData connData = connectionManager.getFromDatabase(connectionDataName);
        String deleteLocation;

        //Invoke rest PostCard P4 telemetry app
        try {
            deleteLocation = restUtilities.postPostCardIntent("127.0.0.1", connData, flowId);
        } catch (IOException e) {
            log.error("[LATENCY MONITOR]: monitoring cannot be activated {}", e.getMessage());
            return false;
        }

        connData.monitoringLatency = true;
        connData.monitoringFlowId = flowId;
        connData.monitoringUrl = deleteLocation;

        log.info("[LATENCY MONITOR]: monitoring of connection {} is now ON", connectionDataName);
        return true;
    }
}
