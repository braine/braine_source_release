package org.braine.app;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ObjectArrays;
import org.onlab.packet.ChassisId;
import org.onlab.util.Frequency;
import org.onlab.util.ItemNotFoundException;
import org.onosproject.net.*;
import org.onosproject.net.device.*;
import org.onosproject.net.intent.Intent;
import org.onosproject.net.intent.PointToPointIntent;
import org.onosproject.net.link.*;
import org.onosproject.net.provider.ProviderId;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Base64;
import java.util.Iterator;

import static org.onlab.util.Tools.nullIsIllegal;
import static org.onlab.util.Tools.readTreeFromStream;

@Component(immediate = true, service = RestUtilities.class)
public class RestUtilities {

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected DevicesManager deviceManager;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected DeviceService deviceService;

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ObjectMapper mapper = new ObjectMapper();

    @Activate
    protected void activate() {

        log.info("REST utilities component has been STARTED");
    }

    @Deactivate
    protected void deactivate() {

        log.info("REST utilities component has been STOPPED");
    }

    protected String postPostCardIntent(String controllerIP, ConnectionData connData, int flowId)
            throws IOException {

        String monitoringUrl=null;

        //Retrieve intent information
        PointToPointIntent intent = connData.intentOne;

        //Check connection data.
        if (connData.ipSrc == null) {
            throw new IOException("missing IPV4_SRC");
        }
        if (connData.ipDst == null) {
            throw new IOException("missing IPV4_DST");
        }
        /*if (connData.udpSrc == null) {
            throw new IOException("missing UDP_SRC");
        }*/
        if ((connData.udpDst == null) && (connData.tcpDst == null)) {
            throw new IOException("missing UDP_DST or TCP_DST");
        }

        //Build the POST json
        ObjectMapper mapper = new ObjectMapper();

        //Builds each criterion
        ObjectNode ipSrcCrit = mapper.createObjectNode();
        ipSrcCrit.put("type", "IPV4_SRC");
        ipSrcCrit.put("ip", connData.ipSrc.toString());

        ObjectNode ipDstCrit = mapper.createObjectNode();
        ipDstCrit.put("type", "IPV4_DST");
        ipDstCrit.put("ip", connData.ipDst.toString());

        /*ObjectNode udpSrcCrit = mapper.createObjectNode();
        udpSrcCrit.put("type", "UDP_SRC");
        udpSrcCrit.put("udpPort", connData.udpSrc.toInt());*/

        ObjectNode transportDstCrit = mapper.createObjectNode();
        if (connData.udpDst != null) {
            transportDstCrit.put("type", "UDP_DST");
            transportDstCrit.put("udpPort", connData.udpDst.toInt());
        } else {
            transportDstCrit.put("type", "TCP_DST");
            transportDstCrit.put("tcpPort", connData.tcpDst.toInt());
        }

        //Build the array criteria
        ArrayNode criteriaArray = mapper.createArrayNode();
        criteriaArray.add(ipSrcCrit);
        criteriaArray.add(ipDstCrit);
        //criteriaArray.add(udpSrcCrit);
        criteriaArray.add(transportDstCrit);

        //Build the node selector
        ObjectNode selectorNode = mapper.createObjectNode();
        selectorNode.put("criteria", criteriaArray);

        //Build the array metadata
        ArrayNode metadataArray = mapper.createArrayNode();
        metadataArray.add("SWITCH_ID");
        metadataArray.add("HOP_LATENCY");

        //Build the object node
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("selector", selectorNode);
        objectNode.put("headerType", "HOP_BY_HOP");
        objectNode.put("telemetryMode", "POSTCARD");
        objectNode.put("metadataTypes", metadataArray);
        objectNode.put("reportTypes", "TRACKED_FLOW");
        objectNode.put("flowId", flowId);

        //Build the postcardtelemetry array
        ArrayNode telemetryArray = mapper.createArrayNode();
        telemetryArray.add(objectNode);

        ObjectNode finalNode = mapper.createObjectNode();
        finalNode.put("postcardtelemetry", telemetryArray);

        log.info("POST content json {}", finalNode.toString());

        try {
            URL url = new URL("http://" + controllerIP + ":8181"
                    + "/onos/TelemetryBraine-app/postcardTelemetry");

            try {
                String loginPassword = "karaf:karaf";
                String encoded = Base64.getEncoder().encodeToString(loginPassword.getBytes());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty ("Authorization", "Basic " + encoded);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestMethod("POST");

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write(objectNode.toString().getBytes());
                os.flush();
                os.close();

                log.info(String.valueOf(conn.getResponseCode()));
                log.info(conn.getResponseMessage());
                log.info("Delete location {}", conn.getHeaderField("location"));

                monitoringUrl = conn.getHeaderField("location");

            } catch (IOException exe) {
                exe.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //TODO to fix

        return monitoringUrl;
    }

    protected void deletePostCardIntent(ConnectionData connectionData)
            throws IOException {

        try {
            URL url = new URL(connectionData.monitoringUrl);
            log.info(url.toString());

            try {
                String loginPassword = "karaf:karaf";
                String encoded = Base64.getEncoder().encodeToString(loginPassword.getBytes());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty ("Authorization", "Basic " + encoded);
                conn.setRequestMethod("DELETE");

                log.info("Response code: {}", conn.getResponseCode());
                log.info("Response message {}", conn.getResponseMessage());

            } catch (IOException exe) {
                exe.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    protected void deletePostCardIntent(String controllerIP, String connectionName) {
        //Build the DELETE json

        try {
            URL url = new URL("http://" + controllerIP + ":8181"
                    + "/onos/pluggables-app/pluggables/delete/"
                    + connectionName);

            try {
                String loginPassword = "karaf:karaf";
                String encoded = Base64.getEncoder().encodeToString(loginPassword.getBytes());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty ("Authorization", "Basic " + encoded);
                //conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestMethod("DELETE");

                log.info(String.valueOf(conn.getResponseCode()));
                //log.info(conn.getResponseMessage());
                log.info(conn.getRequestMethod());

            } catch (IOException exe) {
                exe.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    protected Frequency getPostCardlIntents(String controllerIP, String intentId) {
        String frequencyString = "0.0";

        try {
            URL url = new URL("http://" + controllerIP + ":8181/onos/optical/intents");
            try {
                String loginPassword = "karaf:karaf";
                String encoded = Base64.getEncoder().encodeToString(loginPassword.getBytes());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty ("Authorization", "Basic " + encoded);
                conn.setRequestMethod("GET");

                log.info(String.valueOf(conn.getResponseCode()));
                log.info(conn.getResponseMessage());
                log.info(conn.getRequestMethod());

                if (conn.getResponseCode() == 404) {
                    log.info("No optical inetnts detected");
                    throw new ItemNotFoundException("getIntents produces an ERROR code 404");
                }

                ObjectNode root = readTreeFromStream(mapper, conn.getInputStream());

                //JsonNode jsonId = root.get("id");
                ArrayNode intentsJson = nullIsIllegal((ArrayNode) root.get("Intents"),
                        "String -intents- not found in the REST reply");

                for (JsonNode node : intentsJson) {
                    String id = node.get("intent id").asText();
                    String frequency = node.get("centralFreq").asText();

                    log.info("Optical intent detected id {} freq {}",
                            id, frequency);

                    if (id.equals(intentId)) {
                        String[] parts = frequency.split(" ");
                        frequencyString = parts[0];
                    }
                }

                //log.info("List of detected devices {}", root);

            } catch (IOException exe) {
                exe.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return Frequency.ofTHz(Double.valueOf(frequencyString));
    }
}