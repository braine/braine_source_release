package org.braine.app;

import org.onosproject.net.AnnotationKeys;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.config.NetworkConfigService;
import org.onosproject.net.config.basics.DeviceAnnotationConfig;
import org.onosproject.net.device.DeviceService;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component(immediate = true, service = DevicesManager.class)
public class DevicesManager {

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected DeviceService deviceService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected NetworkConfigService netcfgService;

    private final Logger log = LoggerFactory.getLogger(getClass());

    public Map<DeviceId, Device> degradedDevicesDB = new HashMap<DeviceId, Device>();

    @Activate
    protected void activate() {
        log.info("DeviceManager has been STARTED");
    }

    @Deactivate
    protected void deactivate() {
        log.info("DeviceManager has been STOPPED");
    }

    //Viene chiamata dal comando createMatrixEntry
    public void addToDatabase(DeviceId deviceId) {
        degradedDevicesDB.put(deviceId, deviceService.getDevice(deviceId));
    }

    //Da implementare attraverso un comando specifico ad esempio RemoveMatrixEntry
    public void removeFromDatabase(DeviceId deviceId) {
        degradedDevicesDB.remove(deviceId);
    }

    public Device getFromDatabase(DeviceId deviceId) {
        return degradedDevicesDB.get(deviceId);
    }

    public void annotateDevice(String stringDevice, String stringName) {
        DeviceId deviceId;
        try {
            deviceId = DeviceId.deviceId(stringDevice);
        } catch (IllegalArgumentException e) {
            deviceId = getDeviceByName(deviceService, stringDevice);
        }

        DeviceAnnotationConfig cfg = netcfgService.getConfig(deviceId, DeviceAnnotationConfig.class);
        if (cfg == null) {
            cfg = new DeviceAnnotationConfig(deviceId);
        }
        cfg.annotation(AnnotationKeys.NAME, stringName);

        netcfgService.applyConfig(deviceId, DeviceAnnotationConfig.class, cfg.node());
    }

    public String databaseToString() {
        String data = "";
        for (DeviceId key: degradedDevicesDB.keySet()) {
            data = data
                    + "--- "
                    + degradedDevicesDB.get(key).id().toString()
                    + " name: " + degradedDevicesDB.get(key).annotations().value(AnnotationKeys.NAME)
                    + '\n';
        }

        return data;
    }

    public DeviceId getDeviceByName(DeviceService service, String name) {

        Iterator<Device> it = service.getDevices().iterator();
        while (it.hasNext()) {
            Device currentDevice = it.next();
            if (currentDevice.annotations().value(AnnotationKeys.NAME) != null) {
                if (currentDevice.annotations().value(AnnotationKeys.NAME).equals(name)) {
                    return currentDevice.id();
                }
            }
        }
        return null;
    }

}