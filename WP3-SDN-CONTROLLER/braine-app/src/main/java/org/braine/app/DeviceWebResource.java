package org.braine.app;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onlab.util.ItemNotFoundException;
import org.onosproject.net.*;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.intent.PointToPointIntent;
import org.onosproject.net.topology.TopologyService;
import org.onosproject.rest.AbstractWebResource;

import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Devices commands interface.
 */
@Path("devices")
public class DeviceWebResource extends AbstractWebResource {

    /**
     * Get list of devices.
     *
     * @return 200 OK
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDevices() {
        DeviceService deviceService = get(DeviceService.class);
        DevicesManager devicesManager = get(DevicesManager.class);

        ArrayNode arrayFlows = mapper().createArrayNode();

        deviceService.getDevices().forEach(device -> {
                ObjectNode objectNode = mapper().createObjectNode();

                objectNode.put("name", device.annotations().value(AnnotationKeys.NAME));
                objectNode.put("id", device.id().toString());

                if (devicesManager.getFromDatabase(device.id()) != null) {
                    objectNode.put("degraded", "true");
                } else {
                    objectNode.put("degraded", "false");
                }

                arrayFlows.add(objectNode);
        });

        ObjectNode root = this.mapper().createObjectNode().putPOJO("Devices", arrayFlows);
        return ok(root).build();
    }

    /**
     * Degrade the specified device.
     *
     * Using the sintax from Braine Telemetry System
     *
     * @return 200 OK
     */
    @POST
    @Path("degrade/{switch}/{flow}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDegradedDevice(@PathParam("switch") String switchIdString,
                                      @PathParam("flow") String flowIdString) {
        boolean reroute = true;

        DevicesManager devicesManager = get(DevicesManager.class);
        ConnectionManager connectionManager = get(ConnectionManager.class);
        DeviceService devicesService = get(DeviceService.class);
        TopologyService topologyService = get(TopologyService.class);

        String deviceIdString = convertSwitchId(switchIdString);
        DeviceId deviceId = DeviceId.deviceId(deviceIdString);
        Device device = devicesService.getDevice(deviceId);

        if (device == null) {
            throw new ItemNotFoundException("Device is not found");
        }

        devicesManager.addToDatabase(deviceId);

        if (reroute) {
            for (String key : connectionManager.connectionDB.keySet()) {
                ConnectionData connData = connectionManager.getFromDatabase(key);

                if (!connData.isLocal) {
                    PointToPointIntent intentOne = connData.intentOne;

                    List<Link> links = intentOne.suggestedPath();
                    for (Link link : links) {
                        if ((devicesManager.getFromDatabase(link.src().deviceId()) != null) ||
                                (devicesManager.getFromDatabase(link.dst().deviceId()) != null)) {
                            connData.degraded = true;
                        }
                    }

                    //Reroute degraded connections
                    if ((connData.degraded) && (Integer.parseInt(flowIdString) == connData.monitoringFlowId)) {
                        List<Link> suggestedPathLinks = connData.computeSafePathTwoShortest(topologyService.currentTopology(),
                                connData.hostOne.location().deviceId(),
                                connData.hostTwo.location().deviceId()).links();

                        //Convert list of links in list of devices
                        List<DeviceId> suggestedPathDevices = new ArrayList<>();
                        for (int i = 0; i < suggestedPathLinks.size(); i++) {
                            if (i == 0) {
                                suggestedPathDevices.add(suggestedPathLinks.get(0).src().deviceId());
                            }
                            suggestedPathDevices.add(suggestedPathLinks.get(i).dst().deviceId());
                        }

                        connData.rerouteStrict(suggestedPathDevices);

                        //After rerouting the connection is not anymore degraded
                        connData.degraded = false;
                    }
                }
            }
        }

        return Response.noContent().build();
    }


    /**
     * Degrade the specified device.
     *
     * @return 200 OK
     */
    /*
    @POST
    @Path("degrade/{device}/{reroute}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDegradedDevice(@PathParam("device") String deviceIdString,
                                      @PathParam("reroute") boolean reroute) {
        DevicesManager devicesManager = get(DevicesManager.class);
        ConnectionManager connectionManager = get(ConnectionManager.class);
        DeviceService devicesService = get(DeviceService.class);
        TopologyService topologyService = get(TopologyService.class);

        DeviceId deviceId = DeviceId.deviceId(deviceIdString);
        Device device = devicesService.getDevice(deviceId);

        if (device == null) {
            throw new ItemNotFoundException("Device is not found");
        }

        devicesManager.addToDatabase(deviceId);

        if (reroute) {
            for (String key : connectionManager.connectionDB.keySet()) {
                ConnectionData connData = connectionManager.getFromDatabase(key);
                PointToPointIntent intentOne = connData.intentOne;

                List<Link> links = intentOne.suggestedPath();
                for (Link link : links) {
                    if ((devicesManager.getFromDatabase(link.src().deviceId()) != null) ||
                            (devicesManager.getFromDatabase(link.dst().deviceId()) != null)) {
                        connData.degraded = true;
                    }
                }

                //Reroute degraded connections
                if (connData.degraded) {
                    List<Link> suggestedPathLinks = connData.computeSafePathTwoShortest(topologyService.currentTopology(),
                            connData.hostOne.location().deviceId(),
                            connData.hostTwo.location().deviceId()).links();

                    //Convert list of links in list of devices
                    List<DeviceId> suggestedPathDevices = new ArrayList<>();
                    for (int i=0; i < suggestedPathLinks.size(); i++) {
                        if (i == 0) {
                            suggestedPathDevices.add(suggestedPathLinks.get(0).src().deviceId());
                        }
                        suggestedPathDevices.add(suggestedPathLinks.get(i).dst().deviceId());
                    }

                    connData.rerouteStrict(suggestedPathDevices);

                    //After rerouting the connection is not anymore degraded
                    connData.degraded = false;
                }
            }
        }

        return Response.noContent().build();
    }*/

    /**
     * Un-Degrade the specified device.
     *
     * @return 200 OK
     */
    @DELETE
    @Path("undegrade/{device}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDegradedDevice(@PathParam("device") String deviceIdString) {
        DevicesManager devicesManager = get(DevicesManager.class);
        DeviceService devicesService = get(DeviceService.class);

        DeviceId deviceId = DeviceId.deviceId(deviceIdString);
        Device device = devicesService.getDevice(deviceId);

        if (device == null) {
            throw new ItemNotFoundException("Device is not found");
        }

        devicesManager.removeFromDatabase(deviceId);

        return Response.noContent().build();
    }

    /**
     * Rename the specified device.
     *
     * @return 200 OK
     */
    @POST
    @Path("rename/{device}/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response renameDevice(@PathParam("device") String deviceIdString,
                                 @PathParam("name") String name) {
        DevicesManager devicesManager = get(DevicesManager.class);
        DeviceService devicesService = get(DeviceService.class);

        DeviceId deviceId = DeviceId.deviceId(deviceIdString);
        Device device = devicesService.getDevice(deviceId);

        if (device == null) {
            throw new ItemNotFoundException("Device is not found");
        }

        devicesManager.annotateDevice(deviceIdString, name);

        return Response.ok().build();
    }

    private String convertSwitchId(String switchId) {
        return "device:bmv2:s0" + switchId;
    }
}
