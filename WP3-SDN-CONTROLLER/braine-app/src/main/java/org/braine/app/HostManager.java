package org.braine.app;

import org.onlab.packet.IpAddress;
import org.onosproject.net.*;
import org.onosproject.net.config.NetworkConfigService;
import org.onosproject.net.host.*;
import org.onosproject.net.provider.ProviderId;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.onosproject.cli.AbstractShellCommand.get;

@Component(immediate = true, service = HostManager.class)
public class HostManager {
    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected HostService hostService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected NetworkConfigService netcfgService;

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Activate
    protected void activate() {
        log.info("HostManager has been STARTED");
    }

    @Deactivate
    protected void deactivate() {
        log.info("HostManager has been STOPPED");
    }

    public void annotateHost(String hostString, String nameString) {
        //Build and register the Service and tje Provider needed to push a new host
        HostService hostService = get(HostService.class);
        HostProviderRegistry hostProviderRegistry = get(HostProviderRegistry.class);
        InternalHostProvider hostProvider = new InternalHostProvider();

        //Unregister if already registerd for some reasons
        hostProviderRegistry.unregister(hostProvider);

        //Register
        HostProviderService hostProviderService = hostProviderRegistry.register(hostProvider);
        hostProvider.setHostProviderService(hostProviderService);

        //Retrieve the speciied host
        Host host;
        try {
            host = hostService.getHostsByIp(IpAddress.valueOf(hostString)).iterator().next();
        } catch (IllegalArgumentException e) {
            host = getHostByName(hostService, hostString);
        }

        log.info("Retrieved host id: {}", host.id());

        //Add new name to annotations
        Map<String, String> annotationsMap = new HashMap<>();
        annotationsMap.put(AnnotationKeys.NAME, nameString);
        SparseAnnotations annotations = DefaultAnnotations.builder().putAll(annotationsMap).build();

        HostDescription description = new DefaultHostDescription(
                host.mac(), host.vlan(), host.locations(), host.ipAddresses(), true,
                annotations);

        //Push the new host
        hostProviderService.hostDetected(host.id(), description, true);

        //Unregister the provider
        hostProviderRegistry.unregister(hostProvider);
    }

    public void configureHost(HostId hostId, HostDescription description) {
        //Build and register the Provider needed to push a new host
        HostProviderRegistry hostProviderRegistry = get(HostProviderRegistry.class);
        InternalHostProvider hostProvider = new InternalHostProvider();

        //Unregister if already registerd for some reasons
        hostProviderRegistry.unregister(hostProvider);

        //Register
        HostProviderService hostProviderService = hostProviderRegistry.register(hostProvider);
        hostProvider.setHostProviderService(hostProviderService);

        //Push the new host
        hostProviderService.hostDetected(hostId, description, true);

        //Unregister the provider
        hostProviderRegistry.unregister(hostProvider);
    }

    private Host getHostByName(HostService service, String name) {

        Iterator<Host> it = service.getHosts().iterator();
        while (it.hasNext()) {
            Host currentHost = it.next();
            if (currentHost.annotations().value(AnnotationKeys.NAME) != null) {
                if (currentHost.annotations().value(AnnotationKeys.NAME).equals(name)) {
                    return currentHost;
                }
            }
        }
        return null;
    }

    /**
     * Internal host provider that provides host events.
     */
    private final class InternalHostProvider implements HostProvider {
        private final ProviderId providerId = new ProviderId("host", "org.matrix.app");
        private HostProviderService hostProviderService;

        /**
         * Prevents from instantiation.
         */
        private InternalHostProvider() {
        }

        public void triggerProbe(Host host) {
            // Not implemented since there is no need to check for hosts on network
        }

        public void setHostProviderService(HostProviderService service) {
            this.hostProviderService = service;
        }

        /*
         * Return the ProviderId of "this"
         */
        public ProviderId id() {
            return providerId;
        }
    }
}
