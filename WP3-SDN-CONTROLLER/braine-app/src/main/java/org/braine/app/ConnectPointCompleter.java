package org.braine.app;

import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.apache.karaf.shell.api.console.CommandLine;
import org.apache.karaf.shell.api.console.Completer;
import org.apache.karaf.shell.api.console.Session;
import org.apache.karaf.shell.support.completers.StringsCompleter;
import org.graalvm.compiler.lir.LIRInstruction;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.common.DefaultTopology;
import org.onosproject.net.ConnectPoint;
import org.onosproject.net.Device;
import org.onosproject.net.Port;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.host.HostService;
import org.onosproject.net.topology.Topology;
import org.onosproject.net.topology.TopologyService;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import java.util.List;
import java.util.SortedSet;

/**
 * ConnectPoint completer.
 */
@Service
public class ConnectPointCompleter implements Completer {

    @Override
    public int complete(Session session, CommandLine commandLine, List<String> candidates) {
        // Delegate string completer
        StringsCompleter delegate = new StringsCompleter();

        // Fetch our service and feed it's offerings to the string completer
        DeviceService service = AbstractShellCommand.get(DeviceService.class);

        TopologyService topologyService = AbstractShellCommand.get(TopologyService.class);
        DefaultTopology topology = (DefaultTopology) topologyService.currentTopology();

        // Generate the device ID/port number identifiers
        for (Device device : service.getDevices()) {
            SortedSet<String> strings = delegate.getStrings();
            for (Port port : service.getPorts(device.id())) {
                if (!port.number().isLogical()) {
                    if (!topology.isInfrastructure(new ConnectPoint(device.id(), port.number()))) {
                        strings.add(device.id().toString() + "/" + port.number());
                    }
                }
            }
        }

        // Now let the completer do the work for figuring out what to offer.
        return delegate.complete(session, commandLine, candidates);
    }

}
