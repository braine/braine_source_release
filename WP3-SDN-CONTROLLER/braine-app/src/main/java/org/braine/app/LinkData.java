package org.braine.app;

import org.onosproject.net.Link;
import org.onosproject.net.device.DeviceService;

public class LinkData {
    public Link link;
    public long bandwidth; //kbps
    public double minLatency; //nanoseconds expressed for 64 bytes frames
    public double maxLatency; //nanoseconds expressed for 1500 bytes frames

    public LinkData(Link link, long bandwidth) {

        this.link = link;

        this.bandwidth = bandwidth;
        this.minLatency = ((64*8)*1000) / bandwidth;
        this.maxLatency = ((1500*8)*1000) / bandwidth;
    }

    public String toString() {
        String data = "src=" + link.src() + " dst=" + link.dst();
        data = data + " min-latency: " + minLatency;
        data = data + " max-latency: " + maxLatency;
        data = data + " bandwidth: " + bandwidth + '\n';
        return data;
    }
}
