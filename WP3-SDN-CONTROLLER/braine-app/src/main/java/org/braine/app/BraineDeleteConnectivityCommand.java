package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;

/**
 * Sample Apache Karaf CLI command
 */
@Service
@Command(scope = "onos", name = "braine-connectivity-delete",
        description = "Delete a connection")
public class BraineDeleteConnectivityCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the connectivity", required = true, multiValued = false)
    @Completion(ConnectivityNameCompleter.class)
    String connectivityName = null;

    @Override
    protected void doExecute() {
        ConnectionManager connManager = get(ConnectionManager.class);
        ConnectionData connData = connManager.getFromDatabase(connectivityName);

        if (connManager.getFromDatabase(connectivityName) == null) {
            print("--- [ERROR] connection not found");
            return;
        }

        try {
            connData.deleteConnectivity();
        } catch (IllegalArgumentException e) {
            print("--- [ERROR] %s", e);
        }

        print("--- [SUCCESS] connection %s is now off", connectivityName);
    }
}