package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;

@Service
@Command(scope = "onos", name = "braine-host-rename",
        description = "Add a new host to the network")
public class BraineRenameHostCommand extends AbstractShellCommand {
    @Argument(index = 0, name = "host", description = "Host id", required = true, multiValued = false)
    @Completion(HostCompleter.class)
    String hostString;

    @Argument(index = 1, name = "name", description = "Host friendly name", required = true, multiValued = false)
    String nameString;

    @Override
    protected void doExecute() {
        HostManager hostManager = get(HostManager.class);

        hostManager.annotateHost(hostString,nameString);
    }
}
