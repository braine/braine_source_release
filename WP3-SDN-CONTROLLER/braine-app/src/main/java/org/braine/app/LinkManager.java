package org.braine.app;

import org.onosproject.net.Link;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.link.LinkService;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(immediate = true, service = LinkManager.class)
public class LinkManager {

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected DeviceService deviceService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected LinkService linkService;

    private final Logger log = LoggerFactory.getLogger(getClass());

    public Map<Link, LinkData> linkDB = new HashMap<Link, LinkData>();

    @Activate
    protected void activate() {
        populateDB();

        log.info("LinkManager has been STARTED");

        //log.info("Populated link database {}", databaseToString());
    }

    @Deactivate
    protected void deactivate() {
        /*for (Link key : linkDB.keySet()) {
            removeFromDatabase(key);
        }*/

        log.info("LinkManager has been STOPPED");
    }

    //Viene chiamata dal comando createMatrixEntry
    public void addToDatabase(Link link) {
        LinkData linkData = new LinkData(link, deviceService.getPort(link.src()).portSpeed());
        linkDB.put(link, linkData);
    }

    public void removeFromDatabase(Link link) {
        linkDB.remove(link);
    }

    public LinkData getFromDatabase(Link link) {
        return linkDB.get(link);
    }

    public Iterable<LinkData> getLinks() {
        List links = new ArrayList<LinkData>();
        for (Link key: linkDB.keySet()) {
            links.add(linkDB.get(key));
        }
        return links;
    }

    public String databaseToString() {
        String data = "";
        for (Link key : linkDB.keySet()) {
            data = data + getFromDatabase(key).toString();
        }

        return data;
    }

    public void populateDB() {
        for (Link link : linkService.getLinks()) {
            addToDatabase(link);
        }
    }
}