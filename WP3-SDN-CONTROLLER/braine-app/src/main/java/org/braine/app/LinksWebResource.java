package org.braine.app;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onosproject.rest.AbstractWebResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Iterator;

/**
 * Links commands interface.
 */
@Path("links")
public class LinksWebResource  extends AbstractWebResource {
    /**
     * Get list of links.
     *
     * @return 200 OK
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLinks() {
        LinkManager linkManager = get(LinkManager.class);

        Iterator linkItr = linkManager.getLinks().iterator();

        ArrayNode arrayFlows = mapper().createArrayNode();

        while (linkItr.hasNext()) {

            LinkData linkData = (LinkData) linkItr.next();

            ObjectNode objectNode = mapper().createObjectNode();

            objectNode.put("link src", linkData.link.src().toString());
            objectNode.put("link dst", linkData.link.dst().toString());
            objectNode.put("bandwidth", linkData.bandwidth);

            arrayFlows.add(objectNode);
        }

        ObjectNode root = this.mapper().createObjectNode().putPOJO("Links", arrayFlows);
        return ok(root).build();
    }
}
