/*
 * Copyright 2020-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.braine.app;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.onlab.packet.*;
import org.onosproject.cli.net.EthType;
import org.onosproject.cli.net.IpProtocol;
import org.onosproject.net.*;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.host.HostService;
import org.onosproject.rest.AbstractWebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.onlab.util.Tools.*;

/**
 * Connectivities commands interface.
 */
@Path("connectivities")
public class ConnectivityWebResource extends AbstractWebResource {

    private final Logger log = LoggerFactory.getLogger(getClass());

     /**
     * Get list of Connectivities.
     *
     * @return 200 OK
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConnectivities() {
        ConnectionManager connManager = get(ConnectionManager.class);

        Iterator connItr = connManager.getConnections().iterator();

        ArrayNode arrayFlows = mapper().createArrayNode();

        while (connItr.hasNext()) {

            ConnectionData connData = (ConnectionData) connItr.next();

            ObjectNode objectNode = mapper().createObjectNode();

            objectNode.put("connection name", connData.getName());
            objectNode.put("host one", connData.hostOne.ipAddresses().toString());
            objectNode.put("host two", connData.hostTwo.ipAddresses().toString());
            objectNode.put("bidirectional", connData.bidirectional);
            objectNode.put("min bandwidth", connData.minBandwidth);
            objectNode.put("max bandwidth", connData.maxBandwidth);
            objectNode.put("min latency", connData.minLatency);
            objectNode.put("max latency", connData.maxLatency);
            objectNode.put("telemetry flow-id", connData.monitoringFlowId);

            arrayFlows.add(objectNode);
        }

        ObjectNode root = this.mapper().createObjectNode().putPOJO("Connectivities", arrayFlows);
        return ok(root).build();
    }

    /**
     * Delete the specified connectivity.
     *
     * @return 200 OK
     */
    @DELETE
    @Path("delete/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteConnectivity(@PathParam("name") String connectivityName) {
        ConnectionManager connManager = get(ConnectionManager.class);

        ConnectionData connData = connManager.getFromDatabase(connectivityName);
        nullIsNotFound(connManager.getFromDatabase(connectivityName), "Connectivity not found");

        connData.deleteConnectivity();

        return Response.ok("Connectivity " + connectivityName + " deleted").build();
    }

    /**
     * Activate monitring on a connectivity.
     *
     * @return 200 OK
     */
    @POST
    @Path("starttelemetry/{name}/{flowid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response startMonitor(@PathParam("name") String connectivityName,
                                 @PathParam("flowid") int telemetryId) {
        ConnectionManager connManager = get(ConnectionManager.class);
        LatencyManager latencyManager = get(LatencyManager.class);

        ConnectionData connData = connManager.getFromDatabase(connectivityName);
        nullIsNotFound(connManager.getFromDatabase(connectivityName), "Connectivity not found");

        if (connManager.getFromMonitoringFlowId(telemetryId) != null) {
            throw new IllegalArgumentException("this flowId is already used");
        } else {

            if (!latencyManager.startLatencyMonitor(connectivityName, telemetryId)) {
                throw new IllegalArgumentException("this connection cannot be monitored check selector");
            }
        }

        return Response.noContent().build();
    }

    /**
     * De-activate monitring on a connectivity.
     *
     * @return 200 OK
     */
    @DELETE
    @Path("stoptelemetry/{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response stopMonitor(@PathParam("name") String connectivityName) {
        ConnectionManager connManager = get(ConnectionManager.class);
        LatencyManager latencyManager = get(LatencyManager.class);

        ConnectionData connData = connManager.getFromDatabase(connectivityName);
        nullIsNotFound(connManager.getFromDatabase(connectivityName), "Connectivity not found");

        latencyManager.stopLatencyMonitor(connectivityName);

        return Response.noContent().build();
    }

    /**
     * Create a connectivity.
     *
     * @param stream input json
     * @return
     * @onos.rsModel addConnectivity
     */
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createConnectivity(InputStream stream) {
        try {
            ObjectNode root = readTreeFromStream(mapper(), stream);

            String connectivityName = decodeJsonConnection(root);

            ObjectNode objectNode = mapper().createObjectNode();

            objectNode.put("Connectivity established", connectivityName);

            return Response
                    .accepted(objectNode)
                    .build();

        } catch (IOException ioe) {
            throw new IllegalArgumentException(ioe);
        }
    }

    /**
     * Reroute an existing connectivity.
     *
     * @param stream input json
     * @return
     * @onos.rsModel rerouteConnectivity
     */
    @POST
    @Path("reroute")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response rerouteConnectivity(InputStream stream) {
        try {
            ObjectNode root = readTreeFromStream(mapper(), stream);

            String connectivityName = decodeJsonRerouteConnection(root);

            ObjectNode objectNode = mapper().createObjectNode();

            objectNode.put("Connectivity rerouted", connectivityName);

            return Response
                    .accepted(objectNode)
                    .build();

        } catch (IOException ioe) {
            throw new IllegalArgumentException(ioe);
        }
    }

    private String decodeJsonRerouteConnection(ObjectNode json) {
        ConnectionManager connManager = get(ConnectionManager.class);

        //Parsing connectivity name
        JsonNode jsonName = json.get("connectivityName");
        if (!jsonName.isTextual()) {
            throw new IllegalArgumentException("connectivity invalid json");
        }
        String name = jsonName.asText();
        ConnectionData connData = connManager.getFromDatabase(name);
        if (connData == null) {
            throw new IllegalArgumentException("Connectivity not found");
        }

        //Parsing reroute type
        JsonNode jsonType = json.get("rerouteType");
        if (!jsonType.isTextual()) {
            throw new IllegalArgumentException("rerouteType invalid json");
        }
        String type = jsonType.asText();
        if (!type.equals("STRICT") && !type.equals("LOOSE")) {
            throw new IllegalArgumentException("rerouteType invalid json value");
        }

        //Parsing the list of device
        JsonNode jsonPath = json.get("suggestedPath");
        if (!jsonPath.isObject()) {
            throw new IllegalArgumentException("suggested path invalid json");
        }
        ArrayNode devicesJson = nullIsIllegal((ArrayNode) jsonPath.get("devices"),
                "Suggested path specified without devices");

        List<DeviceId> deviceIds = new ArrayList<>();

        for (JsonNode node : devicesJson) {
            String deviceString = node.get("deviceId").asText();
            DeviceId deviceId = DeviceId.deviceId(deviceString);
            deviceIds.add(deviceId);
            log.info("Rerouting connection device {}", deviceId);
        }

        try {
            if (type.equals("LOOSE")) {
                connData.rerouteLoose(deviceIds);
            } else {
                connData.rerouteStrict(deviceIds);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        }

        return name;
    }

    private String decodeJsonConnection(ObjectNode json) throws IllegalArgumentException {
        HostService hostService = get(HostService.class);
        ConnectionManager connManager = get(ConnectionManager.class);

        boolean allocationOne = true;
        boolean allocationTwo = true;

        JsonNode jsonName = json.get("name");
        if (!jsonName.isTextual()) {
            throw new IllegalArgumentException("name invalid json");
        }
        String name = jsonName.asText();
        if (connManager.getFromDatabase(name) != null) {
            throw new IllegalArgumentException("name already used");
        }

        //HostOne can be specified by IP or by Host ID
        JsonNode jsonHostOne = json.get("hostOne");
        if (!jsonHostOne.isTextual()) {
            throw new IllegalArgumentException("hostOne invalid json");
        }
        Host hostOne;
        try {
            hostOne = hostService.getHostsByIp(IpAddress.valueOf(jsonHostOne.asText())).iterator().next();
        } catch (IllegalArgumentException e) {
            HostId hostOneId = HostId.hostId(jsonHostOne.asText());
            hostOne = hostService.getHost(hostOneId);
        }
        if (hostOne == null) {
            throw new IllegalArgumentException("hostOne not found");
        }

        //HostTwo can be specified by IP or by Host ID
        JsonNode jsonHostTwo = json.get("hostTwo");
        if (!jsonHostTwo.isTextual()) {
            throw new IllegalArgumentException("hostTwo invalid json");
        }
        Host hostTwo;
        try {
            hostTwo = hostService.getHostsByIp(IpAddress.valueOf(jsonHostTwo.asText())).iterator().next();
        } catch (IllegalArgumentException e) {
            HostId hostTwoId = HostId.hostId(jsonHostTwo.asText());
            hostTwo = hostService.getHost(hostTwoId);
        }
        if (hostTwo == null) {
            throw new IllegalArgumentException("hostTwo not found");
        }

        //---------------------------------------------------------------------------
        //--- All mandatory parameters parsed --- BUILD THE ConnectionData ----------
        ConnectionData connectionData = new ConnectionData(name, hostOne, hostTwo);
        //---------------------------------------------------------------------------

        JsonNode jsonPriority = json.get("priority");
        if (jsonPriority != null) {
            if (!jsonPriority.isInt()) {
                throw new IllegalArgumentException("priority invalid json");
            }
            connectionData.priority = jsonPriority.asInt();
        }

        JsonNode jsonBidirectional = json.get("bidirectional");
        if (jsonBidirectional != null) {
            if (!jsonBidirectional.isBoolean()) {
                throw new IllegalArgumentException("bidirectional invalid json");
            }
            connectionData.bidirectional = jsonBidirectional.asBoolean(false);
        }

        JsonNode jsonMinBandwidth = json.get("min-bandwidth");
        if (jsonMinBandwidth != null) {
            if (!jsonMinBandwidth.isInt()) {
                throw new IllegalArgumentException("bandwidth invalid json");
            }
            connectionData.minBandwidth = jsonMinBandwidth.asInt();
        }

        JsonNode jsonMaxBandwidth = json.get("max-bandwidth");
        if (jsonMaxBandwidth != null) {
            if (!jsonMaxBandwidth.isInt()) {
                throw new IllegalArgumentException("bandwidth invalid json");
            }
            connectionData.maxBandwidth = jsonMaxBandwidth.asInt();
        }

        JsonNode jsonMinLatency = json.get("max-latency");
        if (jsonMinLatency != null) {
            if (!jsonMinLatency.isInt()) {
                throw new IllegalArgumentException("latency invalid json");
            }
            connectionData.minLatency = jsonMinLatency.asInt();
        }

        JsonNode jsonMaxLatency = json.get("max-latency");
        if (jsonMaxLatency != null) {
            if (!jsonMaxLatency.isInt()) {
                throw new IllegalArgumentException("latency invalid json");
            }
            connectionData.maxLatency = jsonMaxLatency.asInt();
        }

        //Parsing SELECTOR
        JsonNode selectorJson = json.get("selector");
        if (selectorJson != null) {
            if (!selectorJson.isObject()) {
                throw new IllegalArgumentException("selector invalid json");
            } else {

                JsonNode jsonEthType = selectorJson.get("ethType");
                if (jsonEthType != null) {
                    if (!jsonEthType.isTextual()) {
                        throw new IllegalArgumentException("ethType invalid json");
                    }
                    connectionData.ethType = EthType.valueOf(jsonEthType.asText());
                }

                JsonNode jsonVlanIngress = selectorJson.get("vlanIngress");
                if (jsonVlanIngress != null) {
                    if (!jsonVlanIngress.isTextual()) {
                        throw new IllegalArgumentException("vlanIngress invalid json");
                    }
                    connectionData.vlanIngress = VlanId.vlanId(jsonVlanIngress.asText());
                }

                JsonNode jsonIpSrc = selectorJson.get("ipSrc");
                if (jsonIpSrc != null) {
                    if (!jsonIpSrc.isTextual()) {
                        throw new IllegalArgumentException("ipSrc invalid json");
                    }
                    connectionData.ipSrc = IpPrefix.valueOf(jsonIpSrc.asText());
                }

                JsonNode jsonIpDst = selectorJson.get("ipDst");
                if (jsonIpDst != null) {
                    if (!jsonIpDst.isTextual()) {
                        throw new IllegalArgumentException("ipDst invalid json");
                    }
                    connectionData.ipDst = IpPrefix.valueOf(jsonIpDst.asText());
                }

                JsonNode jsonIpProto = selectorJson.get("ipProto");
                if (jsonIpProto != null) {
                    if (!jsonIpProto.isTextual()) {
                        throw new IllegalArgumentException("ipProto invalid json");
                    }
                    connectionData.ipProto = IpProtocol.valueOf(jsonIpProto.asText());
                }

                JsonNode jsonTcpSrc = selectorJson.get("tcpSrc");
                if (jsonTcpSrc != null) {
                    if (!jsonTcpSrc.isTextual()) {
                        throw new IllegalArgumentException("tcpSrc invalid json");
                    }
                    connectionData.tcpSrc = TpPort.tpPort(Integer.parseInt(jsonTcpSrc.asText()));
                }

                JsonNode jsonTcpDst = selectorJson.get("tcpDst");
                if (jsonTcpDst != null) {
                    if (!jsonTcpDst.isTextual()) {
                        throw new IllegalArgumentException("tcpDst invalid json");
                    }
                    connectionData.tcpDst = TpPort.tpPort(Integer.parseInt(jsonTcpDst.asText()));
                }

                JsonNode jsonUdpSrc = selectorJson.get("udpSrc");
                if (jsonUdpSrc != null) {
                    if (!jsonUdpSrc.isTextual()) {
                        throw new IllegalArgumentException("udpSrc invalid json");
                    }
                    connectionData.udpSrc = TpPort.tpPort(Integer.parseInt(jsonUdpSrc.asText()));
                }

                JsonNode jsonUdpDst = selectorJson.get("udpDst");
                if (jsonUdpDst != null) {
                    if (!jsonUdpDst.isTextual()) {
                        throw new IllegalArgumentException("udpDst invalid json");
                    }
                    connectionData.udpDst = TpPort.tpPort(Integer.parseInt(jsonUdpDst.asText()));
                }
            }
        }

        //Parsing TREATMENTS 
        JsonNode treatmentJson = json.get("treatment");
        if (treatmentJson != null) {
            if (!treatmentJson.isObject()) {
                throw new IllegalArgumentException("treatment invalid json");
            } else {
                JsonNode jsonVlanPopEgress = treatmentJson.get("vlanPopEgress");
                if (jsonVlanPopEgress != null) {
                    if (!jsonVlanPopEgress.isBoolean()) {
                        throw new IllegalArgumentException("vlanPopEgress invalid json");
                    }
                    connectionData.vlanPopEgress = jsonVlanPopEgress.asBoolean(false);
                }

                JsonNode jsonVlanPushEgress = treatmentJson.get("vlanPushEgress");
                if (jsonVlanPushEgress != null) {
                    if (!jsonVlanPushEgress.isTextual()) {
                        throw new IllegalArgumentException("vlanPushEgress invalid json");
                    }
                    connectionData.vlanPushEgress = VlanId.vlanId(jsonVlanPushEgress.asText());
                }

                JsonNode jsonVlanSetEgress = treatmentJson.get("vlanSetEgress");
                if (jsonVlanSetEgress != null) {
                    if (!jsonVlanSetEgress.isTextual()) {
                        throw new IllegalArgumentException("vlanSetEgress invalid json");
                    }
                    connectionData.vlanSetEgress = VlanId.vlanId(jsonVlanSetEgress.asText());
                }
            }
        }

        //Parsing CONSTRAINTS
        JsonNode constraintsJson = json.get("constraints");
        if (constraintsJson != null) {
            if (!constraintsJson.isObject()) {
                throw new IllegalArgumentException("constraints invalid json");
            } else {
                JsonNode jsonVlanEncap = constraintsJson.get("vlanEncapsulation");
                if (jsonVlanEncap != null) {
                    if (!jsonVlanEncap.isTextual()) {
                        throw new IllegalArgumentException("vlanEncapsulation invalid json");
                    }
                    connectionData.vlanEncapsulation = VlanId.vlanId(jsonVlanEncap.asText());
                }

                JsonNode jsonMplsEncap = constraintsJson.get("mplsEncapsulation");
                if (jsonMplsEncap != null) {
                    if (!jsonMplsEncap.isTextual()) {
                        throw new IllegalArgumentException("mplsEncapsulation invalid json");
                    }
                    connectionData.mplsEncapsulation = MplsLabel.mplsLabel(jsonMplsEncap.asText());
                }
            }
        }

        try {
            connectionData.allocateConnection();
            connManager.addToDatabase(connectionData.getName(),connectionData);
        } catch (IllegalArgumentException e) {
            throw e;
        }

        return name;
    }

    protected TrafficSelector buildReverseSelector(Host hostOne, Host hostTwo) {

        TrafficSelector.Builder selectorBuilder = DefaultTrafficSelector.builder();
        selectorBuilder.matchEthSrc(hostOne.mac());
        selectorBuilder.matchEthDst(hostTwo.mac());

        return selectorBuilder.build();
    }

    private String milliseconds(long nanoseconds) {
        double micros, millis;

        micros = nanoseconds / 1000;
        millis = micros / 1000;

        return String.valueOf(millis);
    }
}
