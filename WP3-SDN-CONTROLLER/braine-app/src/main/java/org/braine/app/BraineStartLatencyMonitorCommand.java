package org.braine.app;

//Prende in ingresso il nome della connectionData

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;

//Invia PackOut ogni 5 secondi al nodo
@Service
@Command(scope = "onos", name = "braine-connectivity-telemetry-start",
        description = "Start latency monitoring on a connection")
public class BraineStartLatencyMonitorCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the connectivity", required = true, multiValued = false)
    @Completion(ConnectivityNameCompleter.class)
    String connectivityName = null;

    @Argument(index = 1, name = "flowid", description = "Integer id of telemetry flow", required = true, multiValued = false)
    @Completion(ConnectivityNameCompleter.class)
    String telemetryIdString = null;

    @Override
    protected void doExecute() {
        LatencyManager latencyManager = get(LatencyManager.class);
        ConnectionManager connManager = get(ConnectionManager.class);

        ConnectionData connData = connManager.getFromDatabase(connectivityName);

        if (connData == null) {
            print("--- [ERROR] connection not found", connectivityName);
            return;
        }

        int telemetryId = Integer.valueOf(telemetryIdString);
        if (connManager.getFromMonitoringFlowId(telemetryId) != null) {
            print("--- [ERROR] this flowId is already used");
            return;
        }

        if (latencyManager.startLatencyMonitor(connectivityName, telemetryId)) {
            print("--- [SUCCESS] connection %s is now monitored with id %s",
                    connectivityName,
                    telemetryId);
        } else {
            print("--- [SUCCESS] connection %s cannot be monitored %s",
                    connectivityName);
        }
    }
}
