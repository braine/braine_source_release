/*
 * Copyright 2014-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.braine.app;

import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import org.apache.karaf.shell.api.console.CommandLine;
import org.apache.karaf.shell.api.console.Completer;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.apache.karaf.shell.api.console.Session;
import org.apache.karaf.shell.support.completers.StringsCompleter;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.AnnotationKeys;
import org.onosproject.net.Host;
import org.onosproject.net.host.HostService;

/**
 * Host completer.
 *
 * It provides host friendly name if present.
 * Otherwise provides host IP address if present and unique.
 * Otherwise provide the ONOS host ID.
 *
 */
@Service
public class HostCompleter implements Completer {

    @Override
    public int complete(Session session, CommandLine commandLine, List<String> candidates) {
        // Delegate string completer
        StringsCompleter delegate = new StringsCompleter();

        HostService service = AbstractShellCommand.get(HostService.class);
        Iterator<Host> it = service.getHosts().iterator();
        SortedSet<String> strings = delegate.getStrings();
        while (it.hasNext()) {
            Host currentHost = it.next();
            if (currentHost.annotations().value(AnnotationKeys.NAME) != null) {
                strings.add(currentHost.annotations().value(AnnotationKeys.NAME));
            } else {
                if (currentHost.ipAddresses().size() == 1) {
                    strings.add(currentHost.ipAddresses().iterator().next().toString());
                } else {
                    strings.add(currentHost.id().toString());
                }
            }
        }

        // Now let the completer do the work for figuring out what to offer.
        return delegate.complete(session, commandLine, candidates);

    }
}
