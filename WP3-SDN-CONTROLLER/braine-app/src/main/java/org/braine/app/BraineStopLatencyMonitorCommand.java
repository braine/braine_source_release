package org.braine.app;

//Prende in ingresso il nome della connectionData

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;

//Invia PackOut ogni 5 secondi al nodo
@Service
@Command(scope = "onos", name = "braine-connectivity-telemetry-stop",
        description = "Stop latency monitoring on a connection")
public class BraineStopLatencyMonitorCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "name", description = "Name of the connectivity", required = true, multiValued = false)
    @Completion(ConnectivityNameCompleter.class)
    String connectivityName = null;

    @Override
    protected void doExecute() {
        LatencyManager component = get(LatencyManager.class);
        ConnectionManager connManager = get(ConnectionManager.class);

        ConnectionData connData = connManager.getFromDatabase(connectivityName);

        if (connData == null) {
            print("--- [ERROR] connection not found", connectivityName);
            return;
        }

        component.stopLatencyMonitor(connectivityName);

        print("--- [SUCCESS] connection %s is now not monitored", connectivityName);
    }
}

