/*
 * Copyright 2019-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.braine.app;

import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Completion;
import org.apache.karaf.shell.api.action.Option;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onlab.packet.*;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.cli.net.*;
import org.onosproject.cli.net.EthType;
import org.onosproject.net.AnnotationKeys;
import org.onosproject.net.Host;
import org.onosproject.net.host.HostService;

import java.util.Iterator;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.onosproject.net.flow.DefaultTrafficTreatment.builder;

/**
 * Sample Apache Karaf CLI command
 */
@Service
@Command(scope = "onos", name = "braine-connectivity-add",
         description = "Generate a new connection")
public class BraineAddConnectivityCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "one", description = "Ingress host ID", required = true, multiValued = false)
    @Completion(HostCompleter.class)
    String hostOneString = null;

    @Argument(index = 1, name = "two", description = "Egress host ID", required = true, multiValued = false)
    @Completion(HostCompleter.class)
    String hostTwoString = null;

    @Argument(index = 2, name = "name", description = "Name of the new connectivity", required = true, multiValued = false)
    String connectivityName = null;

    //----------------------
    // Bidirectional option
    //----------------------
    @Option(name = "--bidirectional", description = "Establish bidirectional connectivity",
            required = false, multiValued = false)
    private boolean bidirectional = false;

    @Option(name = "--priority", description = "Priority of associated intents",
            required = false, multiValued = false)
    private String priorityString = null;

    //---------------------------------
    // Flow match options --- SELECTOR
    //---------------------------------
    @Option(name = "--ethType", description = "Match on Ethernet Type",
            required = false, multiValued = false)
    @Completion(EthTypeCompleter.class)
    private String ethTypeString = null;

    @Option(name = "--vlanIngress", description = "Match on VLAN ID",
            required = false, multiValued = false)
    private String vlanIngressString = null;

    @Option(name = "--ipSrc", description = "Match on IP Prefix in the src address",
            required = false, multiValued = false)
    private String srcIpString = null;

    @Option(name = "--ipDst", description = "Match on IP Prefix in the dst address",
            required = false, multiValued = false)
    private String dstIpString = null;

    @Option(name = "--ipProto", description = "Match on IP Protocol Type",
            required = false, multiValued = false)
    @Completion(IpProtocolCompleter.class)
    private String ipProtoString = null;

    @Option(name = "--tcpSrc", description = "Match on Source TCP Port",
            required = false, multiValued = false)
    private String srcTcpString = null;

    @Option(name = "--tcpDst", description = "Match on Destination TCP Port",
            required = false, multiValued = false)
    private String dstTcpString = null;

    @Option(name = "--udpSrc", description = "Match on Source UDP Port",
            required = false, multiValued = false)
    private String srcUdpString = null;

    @Option(name = "--udpDst", description = "Match on Destination UDP Port",
            required = false, multiValued = false)
    private String dstUdpString = null;

    //------------------------------------
    //Treatment options --- TREATMENT
    //------------------------------------
    @Option(name = "--vlanPopEgress", description = "Pop VLAN Tag",
            required = false, multiValued = false)
    private boolean vlanPopEgressString = false;

    @Option(name = "--vlanPushEgress", description = "Push VLAN ID",
            required = false, multiValued = false)
    private String vlanPushEgressString = null;

    @Option(name = "--vlanSetEgress", description = "Pop VLAN Tag",
            required = false, multiValued = false)
    private String vlanSetEgressString = null;

    //----------------------------------------
    // Constraints options --- CONSTRAINTS
    //----------------------------------------
    @Option(name = "--vlanEncapsulation", description = "Encapsulate using this VLAN ID",
            required = false, multiValued = false)
    private String vlanEncapsulationString = null;

    @Option(name = "--mplsEncapsulation", description = "Encapsulate using this MPLS label",
            required = false, multiValued = false)
    private String encapMplsString = null;

    @Option(name = "--bandwidth", description = "Bandwidth checked and reserved at controller",
            required = false, multiValued = false)
    private String bandwidthString = null;

    @Option(name = "--latency", description = "Target latency periodically checked by controller",
            required = false, multiValued = false)
    private String latencyString = null;

    @Override
    protected void doExecute() {

        ConnectionManager connManager = get(ConnectionManager.class);
        HostService hostService = get(HostService.class);

        //Try to retrieve host, if string is not an IP address search into friendly names
        Host hostOne;
        try {
            hostOne = hostService.getHostsByIp(IpAddress.valueOf(hostOneString)).iterator().next();
        } catch (IllegalArgumentException e) {
            hostOne = getHostByName(hostService, hostOneString);
        }

        Host hostTwo;
        try {
            hostTwo = hostService.getHostsByIp(IpAddress.valueOf(hostTwoString)).iterator().next();
        } catch (IllegalArgumentException e) {
            hostTwo = getHostByName(hostService, hostTwoString);
        }

        if (connManager.getFromDatabase(connectivityName) != null) {
            print("--- [ERROR] a connectivity with the same name is already established");
            return;
        }

        //Si crea la connectiondata e si deve inserire nel database
        //Nome della connessione da prendere come parametro di questo stesso comando
        ConnectionData connectionData = new ConnectionData(connectivityName, hostOne, hostTwo);

        //Parse the CLI options and update the ConnectionData fields
        parseCliOptions(connectionData);

        try {
            connectionData.allocateConnection();
            connManager.addToDatabase(connectionData.getName(),connectionData);
            print("--- [SUCCESS] connection established name %s", connectionData.getName());
        } catch (IllegalArgumentException e) {
            print("--- [ERROR] %s", e);
        }
    }

    protected void parseCliOptions(ConnectionData conn)
            throws IllegalArgumentException {

        //General options
        if (bidirectional) {
            conn.bidirectional = true;
        }

        if (!isNullOrEmpty(priorityString)) {
            conn.priority = Integer.parseInt(priorityString);
        }

        if (!isNullOrEmpty(bandwidthString)) {
            conn.minBandwidth = Integer.parseInt(bandwidthString);
        }

        if (!isNullOrEmpty(latencyString)) {
            conn.maxLatency = Integer.parseInt(latencyString);
        }

        //SELECTOR options
        Short ethType = null;
        if (!isNullOrEmpty(ethTypeString)) {
            conn.ethType = EthType.valueOf(ethTypeString);
        }

        IpPrefix srcIpPrefix;
        if (!isNullOrEmpty(srcIpString)) {
            srcIpPrefix = IpPrefix.valueOf(srcIpString);
            if (srcIpPrefix.isIp4()) {
                conn.ipSrc = srcIpPrefix;
            } else {
                throw new IllegalArgumentException("IP source does not match an IPv4 address");
            }
        }

        IpPrefix dstIpPrefix;
        if (!isNullOrEmpty(dstIpString)) {
            dstIpPrefix = IpPrefix.valueOf(dstIpString);
            if (dstIpPrefix.isIp4()) {
                conn.ipDst = dstIpPrefix;
            } else {
                throw new IllegalArgumentException("IP destination does not match an IPv4 address");
            }
        }

        if (!isNullOrEmpty(vlanIngressString)) {
            conn.vlanIngress = VlanId.vlanId(Short.parseShort(vlanIngressString));
        }

        if (!isNullOrEmpty(ipProtoString)) {
            conn.ipProto = IpProtocol.valueOf(ipProtoString);
        }

        if (!isNullOrEmpty(srcTcpString)) {
            conn.tcpSrc = TpPort.tpPort(Integer.parseInt(srcTcpString));
        }

        if (!isNullOrEmpty(dstTcpString)) {
            conn.tcpDst = TpPort.tpPort(Integer.parseInt(dstTcpString));
        }

        if (!isNullOrEmpty(srcUdpString)) {
            conn.udpSrc = TpPort.tpPort(Integer.parseInt(srcUdpString));
        }

        if (!isNullOrEmpty(dstUdpString)) {
            conn.udpDst = TpPort.tpPort(Integer.parseInt(dstUdpString));
        }

        //TREATMENT options
        if (vlanPopEgressString) {
            conn.vlanPopEgress = true;
        }

        if (!isNullOrEmpty(vlanPushEgressString)) {
            conn.vlanPushEgress = VlanId.vlanId(Short.parseShort(vlanPushEgressString));
        }

        if (!isNullOrEmpty(vlanSetEgressString)) {
            conn.vlanSetEgress = VlanId.vlanId(Short.parseShort(vlanSetEgressString));
        }

        //CONSTRAINTS options
        if (!isNullOrEmpty(vlanEncapsulationString)) {
            conn.vlanEncapsulation = VlanId.vlanId(vlanEncapsulationString);
        }

        if (!isNullOrEmpty(encapMplsString)) {
            conn.mplsEncapsulation = MplsLabel.mplsLabel(encapMplsString);
        }

    }

    private Host getHostByName(HostService service, String name) {

        Iterator<Host> it = service.getHosts().iterator();
        while (it.hasNext()) {
            Host currentHost = it.next();
            if (currentHost.annotations().value(AnnotationKeys.NAME) != null) {
                if (currentHost.annotations().value(AnnotationKeys.NAME).equals(name)) {
                    return currentHost;
                }
            }
        }
        return null;
    }
}
