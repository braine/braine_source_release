package org.braine.app;

import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.AnnotationKeys;
import org.onosproject.net.Device;
import org.onosproject.net.device.DeviceService;
import org.onosproject.utils.Comparators;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
@Command(scope = "onos", name = "braine-device-list",
        description = "Show currently degraded devices")
public class BraineListDevicesCommand extends AbstractShellCommand {

    @Override
    protected void doExecute() {
        DevicesManager deviceManager = get(DevicesManager.class);
        DeviceService deviceService = get(DeviceService.class);

        print("--- Devices ---");
        for (Device device : getSortedDevices(deviceService)) {
            print("--- %s name: %s", device.id(), device.annotations().value(AnnotationKeys.NAME));
        }

        print("--- Degraded devices ---");
        print(deviceManager.databaseToString());
    }

    /**
     * Returns the list of devices sorted using the device ID URIs.
     *
     * @param service device service
     * @return sorted device list
     */
    public static List<Device> getSortedDevices(DeviceService service) {
        List<Device> devices = newArrayList(service.getDevices());
        Collections.sort(devices, Comparators.ELEMENT_COMPARATOR);
        return devices;
    }

}