Env
===
```
export KUBECONFIG=kubeconfig-cnit.yaml
export CTX_CLUSTER1=cluster1
export CTX_CLUSTER2=cluster2
```

Install Istio 1.7.4
===
```
kubectl --context cluster1 create namespace istio-system
kubectl --context cluster1 create secret generic cacerts -n istio-system --from-file=samples/certs/ca-cert.pem --from-file=samples/certs/ca-key.pem --from-file=samples/certs/root-cert.pem --from-file=samples/certs/cert-chain.pem
istioctl install --context $CTX_CLUSTER1 --set hub=172.30.101.1:5000 -f manifests/examples/multicluster/values-istio-multicluster-gateways.yaml
kubectl --context cluster2 create namespace istio-system
kubectl --context cluster2 create secret generic cacerts -n istio-system --from-file=samples/certs/ca-cert.pem --from-file=samples/certs/ca-key.pem --from-file=samples/certs/root-cert.pem --from-file=samples/certs/cert-chain.pem
istioctl install --context $CTX_CLUSTER2 --set hub=172.30.101.1:5000 -f manifests/examples/multicluster/values-istio-multicluster-gateways.yaml
```

Install coredns-plugin
===
There is no way to specify hub for coredns-plugin during install and it defaults to docker.io which has pull limits.
The workaround is to pre-install the image on worker nodes:
```
docker pull 172.30.101.1:5000/coredns-plugin:0.2-istio-1.1
docker tag 172.30.101.1:5000/coredns-plugin:0.2-istio-1.1 istio/coredns-plugin:0.2-istio-1.1
```

Restart services on master node
===
```
mv /etc/kubernetes/manifests/kube-apiserver.yaml /root/kube-apiserver.yaml
# wait for kube-apiserver docker container to stop
# edit yaml if needed ...
mv /root/kube-apiserver.yaml /etc/kubernetes/manifests/kube-apiserver.yaml
# wait for kube-apiserver docker container to start
```

Setup DNS
===
```
scripts/setupdns.sh
```

Building the mesh configuration service
===
The source code of the mesh configuration service is in the `service` folder.
Use the following commands to build and deploy the service:
```
cd service
make
kubectl apply -f ../scripts/braine-mesh.yaml
```

Deploy demo
===
```
kubectl create --context=$CTX_CLUSTER1 namespace x86
kubectl label --context=$CTX_CLUSTER1 namespace x86 istio-injection=enabled
kubectl create --context=$CTX_CLUSTER2 namespace gpu
kubectl label --context=$CTX_CLUSTER2 namespace gpu istio-injection=enabled

kubectl apply --context=$CTX_CLUSTER1 -n x86 -f scripts/braine.yaml
kubectl apply --context=$CTX_CLUSTER1 -n x86 -f scripts/collect.yaml
kubectl apply --context=$CTX_CLUSTER2 -n gpu -f scripts/plot.yaml
```

Using the mesh configuration service
===
The `MESH_SVC_URL` env var should point the service URL, e.g.:
```
export MESH_SVC_URL="http://172.30.200.200:30700"
```

Register EMDCs
---
```
curl -v -F 'emdc=@demo1-cnit.yaml' ${MESH_SVC_URL}/emdc
curl -v -F 'emdc=@demo2-cnit.yaml' ${MESH_SVC_URL}/emdc
```

List all EMDCs
---
```
curl -v ${MESH_SVC_URL}/emdcs
```

Retrieve EMDC by id
---
```
curl -v ${MESH_SVC_URL}/emdc/0
```

Unregister EMDC by id
---
```
curl -X DELETE ${MESH_SVC_URL}/emdc/0
```

Create service entry
---
```
curl -H "Content-Type: application/json" -d '{"host":"plot.gpu.global", "name":"plot-gpu", "namespace":"x86", "portnumber":80, "protocol":"http"}' ${MESH_SVC_URL}/emdc/0/serviceentry/create/1
```

Retrieve all service entries in EMDC
---
```
curl -v ${MESH_SVC_URL}/emdc/0/serviceentries
```

Retrieve service entry by UUID
---
```
curl -v ${MESH_SVC_URL}/emdc/0/serviceentry/f5c1e383-d9d9-4a69-960b-91170e1ed669
```

Delete service entry by UUID
---
```
curl -X DELETE ${MESH_SVC_URL}/emdc/0/serviceentry/f5c1e383-d9d9-4a69-960b-91170e1ed669
```

Creating service entry with shell script (deprecated)
---
```
scripts/mksvcentry.sh
kubectl apply --context=$CTX_CLUSTER1 -n x86 -f svcentry.yaml
```

Clean up
===
```
kubectl delete --context=$CTX_CLUSTER1 -n x86 -f scripts/braine.yaml
kubectl delete --context=$CTX_CLUSTER1 -n x86 -f scripts/collect.yaml
kubectl delete --context=$CTX_CLUSTER2 -n gpu -f scripts/plot.yaml
kubectl delete --context=$CTX_CLUSTER1 -n x86 -f svcentry.yaml
istioctl x uninstall --purge
```
