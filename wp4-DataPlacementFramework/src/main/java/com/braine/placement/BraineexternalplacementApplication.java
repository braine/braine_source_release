package com.braine.placement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BraineexternalplacementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BraineexternalplacementApplication.class, args);
	}

}
