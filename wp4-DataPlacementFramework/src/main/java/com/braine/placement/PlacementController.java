package com.braine.placement;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import java.util.Random;
import java.io.*;
import java.util.*;

//Controller  
@RestController
public class PlacementController {
//using get method and hello-world as URI  
	@GetMapping(path = "/placement")
	public String hello1(@RequestParam(name="keyClass", required = true, defaultValue = "A") String keyClass)
	{
		//Random random = new Random();
		//int randomNode = random.nextInt(size);
		//randomNode = (randomNode==0)?1:randomNode;
		//System.out.println("##################: datanodes:"+randomNode);
		String nodeList="";
		Locale l=Locale.US;
		ResourceBundle res=ResourceBundle.getBundle("nodesandallocation",l);
		nodeList=res.getObject("allDataNodes").toString();
		
		//setCurrentAllocation("currentAllocation1",keyClass);
		nodeList=filterNodesOnKeyClass(keyClass);
		return "datanodes:"+nodeList;
	}
	public String filterNodesOnKeyClass(String keyClass) {
	    Properties propsKeyClass = new Properties();
	    Properties propsNodesAllocation = new Properties();
		String selectedNodes="";
		String propsKeyClassFileName = "./src/main/resources/keyclassconstraints.properties";
		String propsNodesAllocationFileName = "./src/main/resources/nodesandallocation.properties";
	    try {
		      //first load old one:
		      FileInputStream keyClassContraints = new FileInputStream(propsKeyClassFileName);
		      FileInputStream nodesAndAllocation = new FileInputStream(propsNodesAllocationFileName);
		      propsKeyClass.load(keyClassContraints);
		      propsNodesAllocation.load(nodesAndAllocation);
		      keyClassContraints.close();
		      nodesAndAllocation.close();
		      
		      // fetch all key classes 
		      // remove the permitted shared class 
		      Set<String> keys = propsNodesAllocation.stringPropertyNames();
		      String existingClasses="";
		      String shareWith=propsKeyClass.getProperty("keyClass-"+keyClass);
		      String allKeyClass=propsKeyClass.getProperty("allKeyClass");
		      String [] shareParts=shareWith.trim().split(",");
		      for(int i=0;i<shareParts.length;i++) {
		    	  int idx=allKeyClass.indexOf(shareParts[i]);
		    	  allKeyClass = allKeyClass.substring(0, idx) + allKeyClass.substring(idx + 1);
		      }
		      // now allKeyClass has only not sharing key class for the given keyClass
		      String noShareWith=allKeyClass; 
		      String [] noShareParts=noShareWith.trim().split(",");
		      //select the nodes with no shareclass
		      for (String key : keys) {
			      boolean rejectNode=false;
		    	  if (key.startsWith("currentAllocation")) {
		    		  System.out.println(key);
		    		  existingClasses=propsNodesAllocation.getProperty(key);
		    	  	  for (int i=0;i<noShareParts.length;i++) {
		    	  		  if (!noShareParts[i].isEmpty())
		    	  		  if (existingClasses.contains(noShareParts[i])) {
		    	  			  System.out.println(existingClasses+"rejecting"+key+"hai"+noShareParts[i]);
		    	  			  rejectNode=true;
		    	  		  }
		    	  	  }
		    	  	if (!rejectNode)
			    		  selectedNodes=selectedNodes+key.charAt(key.length()-1);  	
		    	  }
		    	  
		        }
		        System.out.println("Selected Nodes ="+selectedNodes+" for KeyClass="+keyClass);
	    } catch (IOException ex) {
		      ex.printStackTrace();
	    }
		return selectedNodes;
	}
	public void setCurrentAllocation(String node, String allocatedClasses) {
	    Properties props = new Properties();

	    String propsFileName = "./src/main/resources/nodesandallocation.properties";
	    try {
	      //first load old one:
	      FileInputStream nodesAndAllocation = new FileInputStream(propsFileName);
	      props.load(nodesAndAllocation);
	      nodesAndAllocation.close();
	      String currentAllocation=props.getProperty(node);
	      if (currentAllocation.equals("Z"))
	    	  props.setProperty(node, allocatedClasses);
	      else {
	    	  if (!currentAllocation.contains(allocatedClasses))
	    		  props.setProperty(node, currentAllocation+","+allocatedClasses);
	      }
	     //save modified property file
	      FileOutputStream output = new FileOutputStream(propsFileName);
	      props.store(output, "NodeList and CurrentAllocation");
	      output.close();
	    } catch (IOException ex) {
	      ex.printStackTrace();
	    }

	}
}