#!/usr/bin/env python
import time
import os
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from influxdb import InfluxDBClient
import pprint as pp
import yaml


influx_data_list = []

configs = yaml.load(open('configs.yaml', 'r'), Loader=yaml.FullLoader)
push_time = time.time()
client = InfluxDBClient(host='localhost', port=8086)

# Easiest way to prevent a race-condition.  We need influxDB to be up and running fully before
# We create the client connection
while True:
    try:
        client.create_database('prometheus')
        client.switch_database('prometheus')  # possibly do a check if db exists, and if not create it
        break
    except:
        print('Waiting for connection to start service')
        time.sleep(1)


print('Influx has started')
# Create the metrics with descriptions.  Generate better names based on prometheus sepcifications


def get_latest_counter(file):
    # Assumes newlines are initiated with \n characters
    with open(file, 'rb') as f:
        seek_position = 1

        # Loop over the file until it discovers the new line character.  Possible infinite loop here.  Add guards
        # against this, or change it to a for loop.
        while True:
            f.seek(seek_position, 2)
            end_line = f.read()

            # Skip the last character incase it's a new line
            if '\n' in end_line.decode()[:-1]:
                break
            else:
                seek_position -= 1

    return end_line.decode()


# If the file is deleted create another as empty
def on_deleted(event):
    f = open(event.src_path, 'w')
    f.close()


def create_influx_datapoint(meaturment, value, ts):
    global push_time
    datapoint = {
        'measurement': meaturment,
        'time': int(float(ts)*1000000000),
        'fields': {
            'counter': value,
            'push-time': int(push_time * 1000000000),
        }
    }
    return datapoint


# If the file is modified, read the last line of the file
def on_modified(event):
    global push_time
    global influx_data_list

    # If log file was deleted just return
    if os.path.isdir(event.src_path):
        return

    # Grab the last line of the file and return the first element in that line
    # adding the .strip('\n').split('\n')[-1] is a quick fix, because sometimes the
    # method returns two new lines, so we split based on newline and take the more recent even (i.e. [-1])
    # Possibly look into creating a more robust get_latest_counter method so it doesn't overshoot
    last_line = get_latest_counter(event.src_path).strip('\n').split('\n')[-1].split(',')
    time.perf_counter()
    last_value = last_line[0]
    last_timestamp = last_line[1]
    # Depending on the file modified, update the metric data
    if 'pings' in event.src_path:
        log_value = int(last_value)
        write_measurement = 'ping-event'
    else:
        log_value = float(last_value)
        write_measurement = 'pong-event'

    influx_data_list.append(create_influx_datapoint(write_measurement, log_value, last_timestamp))

    # Time to push
    if time.time() - push_time >= configs['push_influx_data_per_seconds']:
        push_time = time.time()
        client.write_points(influx_data_list, protocol='json')
        influx_data_list = []


if __name__ == "__main__":

    my_event_handler = PatternMatchingEventHandler()
    my_event_handler.on_modified = on_modified
    my_event_handler.on_deleted = on_deleted

    observer = Observer()
    observer.schedule(my_event_handler, path=configs['log_location'], recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    finally:
        observer.stop()
    observer.join()
