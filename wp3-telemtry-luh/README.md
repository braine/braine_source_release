# WP3-Telemtry-LUH
The BRAINE’s telemetry software comprises of the following components: 

## Prometheus 
Prometheus is application used for monitoring events and alerting.  The service provides local disk storage that is used a time-series database to maintain the metric records.  The method Prometheus uses to collect data is with an HTTP pull model. Prometheus uses this pull-based model which means that the Prometheus system collects the data and feeds it to the Prometheus database (local storage). Our Prometheus is set up to remote write all of its collected data to port 8086, which is read by InfluxDB. https://github.com/prometheus/prometheus 

## InfluxDB 
InfluxDB is a time series database which is used to store time-series records. It also provides the flexibility of the NoSQL schema-less systems for monitoring applications. Prometheus pushes the scraped metrics to InfluxDB, which stores them in a database. InfluxDB is used to store all of our metrics. While Prometheus is able to save and store some metrics for the short-term, InfluxDB is used to permanently save metrics. Additional software is then able to query both InfluxDB and Prometheus to visualize the metrics.  It is also possible for other applications to simply push metrics to InfluxDB, so long as they follow the correct json format, and pushed to the correct URL.  
Influx is used in our setting as time series database which permanently stores Prometheus and node-exporter scraped metrics, and stores ping and pong event records. https://github.com/influxdata/influxdb 

## Node Exporter 
Node-Exporter is a Prometheus-specific exporter which allows us to collect a wide range of operating system and Hardware specific metrics. In the context of BRAINE, we are able to collect up to hundreds of individual metrics, and each UC can specify how many of the metrics are going to be collected.  It presents the metrics via an HTTP endpoint. https://github.com/prometheus/node_exporter 

## cAdvisor 
cAdvisor stands for Container Advisor. It is similar to node-exporter, but it is able to provide container level metrics.  It is able to aggregate and process the collected metrics.  It then exposes the metrics to an HTTP endpoint, which then is scraped by Prometheus. It additionally comes with a Web UI to visualize the metrics collected.   

## AlertManager

The Alertmanager handles alerts sent by client applications such as the Prometheus server. It takes care of deduplicating, grouping, and routing them to the correct receiver integration such as email, PagerDuty, or OpsGenie. It also takes care of silencing and inhibition of alerts.

## How to install and run:

### Dependencies:

Git is used to copy the repository locally, and docker is used to create and run the enviornments. These can be installed with following two lines of code below

`sudo apt install docker`

`sudo apt install git`

### Copy repository to local directory:

After installing git you can now clone the repository by entering the command below

`git clone https://gitlab.com/braine/wp3-telemtry-luh.git`

and then

`cd T34/telemetry`
### Running the main containers:

To run the containers influxdb, prometheus, nodeexporter, grafana, ping, pong, and metric exporter you will first navigate to the recently cloned directory WP4R/T44/monitoring, and type:
`sudo docker-compose up -d`

If you would like to make changes to the containers or configuration files then be sure to run:
`sudo docker-compose up --no-deps --build -d` 
to rebuild your containers

For debugging each individual container can be run by using the command 
`sudo module/docker-compose up --no-deps --build -d`
, where "module" is one of the container sub-folder, i.e. grafana or ping_app.

### Accessing interfaces:

##### Prometheus
Viewed from your web browser by entering the address localhost:9090.
##### Grafana
Grafana can be accessed from localhost:3000.  If you wish to import or create new dashboards see the section "Dashboard Compiler"
##### Node Exporter
Node Exporter metrics which are scraped by Prometheus can be viewd by using localhost:9100.
##### Influx
If you would like to directly query Influx database start by using ssh on the docker container with
`docker exec -it influxdb sh`.  Once inside open up Influx by typing `influx`.  From there you should be able to query the databases you want.  For example to view the ping counter in our prometheus dataset, we can type `use prometheus` followed by the query, i.e. `select * from "ping-event"`


### Running the debugging container loading:

This container will make the pong app query ping at a rate and runtime specified in the loader/configs.yaml.  The configuration file can be modified to increase the number of requests per second and the overall runtime.

### Debugging containers

If you wish to view the container outputs you can run the containers without detaching from the services. 
This can be done by runing `sudo docker-compose up --no-deps --build`.  Note the lack of "-d".
If you wish for specific services to not display an output you can naviage to the docker-compose.yml in the main
directory and add `logging: driver: "none"` to a service.  

E.g. 
<pre>
        influxdb:
         <b>   logging: 
               driver: "none"</b>
            container_name: influxdb
            network_mode: "host"
            build:
              context: influxdb/
            ports:
              - 8086:8086
</pre>
### Stop containers:

To stop all the docker enviornments you can enter the command `sudo docker-compose down`

Alternatively if you wish to stop individual services you can type `sudo docker container stop <container name>`.   You can view the list of container names by using the command `sudo docker container ls'
