import json
import sys
import requests


def main(flexran_url):
    try:
        r = requests.get(url=flexran_url, timeout=0.5).json()
        for mac_stats in r["mac_stats"]:
            bs_id = mac_stats['bs_id']
            for ue in mac_stats['ue_mac_stats']:
                rnti = ue['rnti']
                del ue['rnti']
                del ue['mac_stats']['rnti']
                send = json.dumps({'bs_id': bs_id, 'rnti': rnti, 'info': ue})
                print(send)
    except Exception as e:
        pass


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(0)
    else:
        main(sys.argv[1])
