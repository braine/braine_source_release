#!/bin/bash
set -e
# Any subsequent(*) commands which fail will cause the shell script to exit immediately

BUILD_TRAINER="no"
BUILD_INF="no"
NO_TRAIN="no"
while getopts ":tin" opt; do
  case $opt in
    t) #option -t means build the scheduler-trainer image
      BUILD_TRAINER="yes"
      ;;
    i) #option -i means build the scheduler-inference image
      BUILD_INF="yes"
      ;;
    n) #option -n means no training will be performed
      NO_TRAIN="yes"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

# This file will contain the logs of certain commands executed in the script (like docker build...)
touch braine_logs.txt

#retrieve username
USERNAME=$SUDO_USER

SCHED_STATUS=$(sudo -u $USERNAME kubectl get pods  -l component=kube-scheduler -n kube-system --no-headers -o custom-columns=":status.phase")
# Credit: https://reuvenharrison.medium.com/how-to-wait-for-a-kubernetes-pod-to-be-ready-one-liner-144bbbb5a76f
SCHED_READY=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}')
SCHED_IMAGE=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o=jsonpath="{.items[0].spec.containers[0].image}{'\n'}")

if [ $SCHED_STATUS = "Running" ] && [ $SCHED_IMAGE = "localhost:5000/scheduler-plugins/kube-scheduler:latest" ] && [ $SCHED_READY = "True" ]
then
    echo "Custom version of the scheduler is already running! Exiting..."
    exit 0
fi

# Retrieve number of nodes
NB_NODES=$(sudo -u $USERNAME kubectl get nodes --no-headers | wc -l)
echo "The cluster has ${NB_NODES} nodes. This value will be used for neural network training and inference."
sed -i "/    'NB_NODES': 8,/c\    'NB_NODES': ${NB_NODES}," common/config.py

echo "Creating directory to be references by persistent volume for NN weights."

DIR="/mnt/disk/vol1"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "${DIR} already exists. Skipping..."
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $DIR
fi

if [ $NO_TRAIN = "yes" ]
then
  echo "-n was set. Skipping training."
else

  if [ $BUILD_TRAINER = "yes" ]
  then
    echo "Building scheduler-trainer image, this will take a few minutes..."
    docker build -t localhost:5000/scheduler-trainer:latest -f SchedulerTrainer/Dockerfile . >> braine_logs.txt 2>&1
  fi

  echo "Creating resources for scheduler-trainer"
  sudo -u $USERNAME kubectl apply -f manifests/scheduler-trainer.yaml

  time_slept=0
  while [ $time_slept -lt 120 ]
  do
    echo "Waiting for training to complete..."
    sleep 3
    time_slept=$((time_slept + 3))
    if [ -f /mnt/disk/vol1/DDQN_model_weights.pt ]; then
        echo "NN weights have been saved and can now be used by the inference app!"
        break
    fi
  done
fi

if [ $BUILD_INF = "yes" ]
then
  echo "Building scheduler-inference image, this will take a few minutes..."
  docker build -t localhost:5000/scheduler-inference:latest -f SchedulerInference/Dockerfile . >> braine_logs.txt 2>&1
fi
echo "Creating resources for scheduler-inference"
sudo -u $USERNAME kubectl apply -f manifests/scheduler-inference.yaml

echo "Creating common resources (for training and inference)"
sudo -u $USERNAME kubectl apply -f manifests/common-res.yaml

echo "Creating directory: brainerlscheduler"
SCHED_CODE_DIR=./scheduler-plugins/pkg/brainerlscheduler
if [ -d "$SCHED_CODE_DIR" ]; then
  echo "${SCHED_CODE_DIR} already exists. Skipping..."
else
  mkdir $SCHED_CODE_DIR

  echo "Copying BRAINE RL scheduler plugin code into scheduler code"
  cp braine_rl_scheduling.go ./scheduler-plugins/pkg/brainerlscheduler

  echo "Registering BRAINE RL scheduler plugin"
  sed -i '/command := app.NewSchedulerCommand(/a \                app.WithPlugin(brainerlscheduler.Name, brainerlscheduler.New),' ./scheduler-plugins/cmd/scheduler/main.go
  sed -i '/import (/a \        "sigs.k8s.io/scheduler-plugins/pkg/brainerlscheduler"' ./scheduler-plugins/cmd/scheduler/main.go
fi

# Add label so that it can be removed based on it during clean-up.
# Credit: https://stackoverflow.com/questions/50126741/how-to-remove-intermediate-images-from-a-build-after-the-build
sed -i '/golang/a LABEL stage=builder' ./scheduler-plugins/build/scheduler/Dockerfile

# build image
echo "Checking scheduler Docker image locally"
docker image inspect localhost:5000/scheduler-plugins/kube-scheduler:latest >/dev/null 2>&1 && exists=1 || echo "Image does not exist!" && exists=0

if [ $exists -eq 0 ]; then
    cd scheduler-plugins
    echo "Building custom kube-scheduler image, this will take a few minutes..."
    #make local-image
    docker build -f ./build/scheduler/Dockerfile --build-arg ARCH="amd64" --build-arg RELEASE_VERSION="v20210907-v0.19.9-4-gcafeb5f" -t localhost:5000/scheduler-plugins/kube-scheduler:latest . >> ../braine_logs.txt 2>&1
else
    echo "Image already exists. Skipping..."
fi

# Backup kube-scheduler.yaml
if [ ! -f /etc/kubernetes/kube-scheduler.yaml ]; then
    echo "Backing-up kube-scheduler pod manifest"
    cp /etc/kubernetes/manifests/kube-scheduler.yaml /etc/kubernetes/kube-scheduler.yaml
else
    echo "kube-scheduler yaml already backed-up!"
fi

# Create /etc/kubernetes/braine-sched-rl.yaml
if [ ! -f /etc/kubernetes/braine-sched-rl.yaml ]
then
  echo "Creating custom kube-scheduler configuration"
  cat > /etc/kubernetes/braine-sched-rl.yaml <<EOF
  apiVersion: kubescheduler.config.k8s.io/v1beta1
  kind: KubeSchedulerConfiguration
  leaderElection:
    # (Optional) Change true to false if you are not running a HA control-plane.
    leaderElect: false
  clientConnection:
    kubeconfig: /etc/kubernetes/scheduler.conf
  profiles:
  - schedulerName: default-scheduler
    plugins:
      preScore:
        enabled:
        - name: "BRAINE-RL-Scheduling"
          weight: 10001
        disabled:
        - name: "*"
      score:
        enabled:
        - name: "BRAINE-RL-Scheduling"
          weight: 10001
        disabled:
        - name: "*"
EOF
else
  echo "/etc/kubernetes/braine-sched-rl.yaml already exists!"
fi

# Modify /etc/kubernetes/manifests/kube-scheduler.yaml
echo "Modifying kube-scheduler manifest based on new configuration"
# Retrieve the prometheus URL from the configmap to be used as an env variable in the pod
ENV_VAL=$(sudo -u $USERNAME kubectl get cm sched-configmap -o jsonpath={.data.prometheus_base_url} -n kube-system)
cat <<EOF > /etc/kubernetes/manifests/kube-scheduler.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    component: kube-scheduler
    tier: control-plane
  name: kube-scheduler
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-scheduler
    - --authentication-kubeconfig=/etc/kubernetes/scheduler.conf
    - --authorization-kubeconfig=/etc/kubernetes/scheduler.conf
    - --bind-address=127.0.0.1
    - --config=/etc/kubernetes/braine-sched-rl.yaml
    - --port=0
    - --v=1000
    image: localhost:5000/scheduler-plugins/kube-scheduler:latest
    env:
    - name: PROMETHEUS_BASE_URL
      value: $ENV_VAL
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 10259
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: kube-scheduler
    resources:
      requests:
        cpu: 100m
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 10259
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    volumeMounts:
    - mountPath: /etc/kubernetes/scheduler.conf
      name: kubeconfig
      readOnly: true
    - mountPath: /etc/kubernetes/braine-sched-rl.yaml
      name: braine-sched-rl
      readOnly: true
  hostNetwork: true
  dnsPolicy: ClusterFirstWithHostNet
  priorityClassName: system-node-critical
  volumes:
  - hostPath:
      path: /etc/kubernetes/scheduler.conf
      type: FileOrCreate
    name: kubeconfig
  - hostPath:
      path: /etc/kubernetes/braine-sched-rl.yaml
      type: FileOrCreate
    name: braine-sched-rl
status: {}
EOF

# check if scheduler is running and ready
time_slept=0
while [ $time_slept -lt 180 ]
do
  echo "Waiting for scheduler pod to be ready..."
  sleep 3
  time_slept=$((time_slept + 3))
  echo "Waited ${time_slept}/180 s."
  SCHED_STATUS=$(sudo -u $USERNAME kubectl get pods  -l component=kube-scheduler -n kube-system --no-headers -o custom-columns=":status.phase")
  # Credit: https://reuvenharrison.medium.com/how-to-wait-for-a-kubernetes-pod-to-be-ready-one-liner-144bbbb5a76f
  SCHED_READY=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}')
  SCHED_IMAGE=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o=jsonpath="{.items[0].spec.containers[0].image}{'\n'}")

  if [ $SCHED_STATUS = "Running" ] && [ $SCHED_IMAGE = "localhost:5000/scheduler-plugins/kube-scheduler:latest" ] && [ $SCHED_READY = "True" ]
  then
      echo "kube-scheduler is ${SCHED_STATUS} using the custom image: ${SCHED_IMAGE}"
      break
  fi
done
