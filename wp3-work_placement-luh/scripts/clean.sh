#!/bin/bash
set -e
# Any subsequent(*) commands which fail will cause the shell script to exit immediately

# retrieve options
RM_TRAINER="no"
RM_INF="no"
RM_CS="no"
while getopts ":tis" opt; do
  case $opt in
    t) #option -t means remove the scheduler-trainer image
      RM_TRAINER="yes"
      ;;
    i) #option -i means remove the scheduler-inference image
      RM_INF="yes"
      ;;
    s) #option -s means remove the custom scheduler image
      RM_CS="yes"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

#retrieve username
USERNAME=$SUDO_USER

SCHED_STATUS=$(sudo -u $USERNAME kubectl get pods  -l component=kube-scheduler -n kube-system --no-headers -o custom-columns=":status.phase")
SCHED_READY=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}')
SCHED_IMAGE=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o=jsonpath="{.items[0].spec.containers[0].image}{'\n'}")
if [ $SCHED_STATUS = "Running" ] && [[ $SCHED_IMAGE = "k8s.gcr.io/kube-scheduler"* ]] && [ $SCHED_READY = "True" ]
then
    echo "Default version of the scheduler is already running! Exiting..."
    exit 0
fi

echo "Deleting resources created for custom scheduler"
sudo -u $USERNAME kubectl diff -f manifests/scheduler-trainer.yaml >/dev/null 2>&1 && ok=$?
# if return code is 0, resources are installed. So, we will delete them
if [ -z "${ok+set}" ]
then
  echo "Resources not installed!"
elif [ $ok -eq 0 ]
then
  sudo -u $USERNAME kubectl delete -f manifests/scheduler-trainer.yaml --force --wait=false
  echo "Scheduler trainer resources deleted!"
else
  echo "A problem has occurred while deleting resources!"
fi

sudo -u $USERNAME kubectl diff -f manifests/common-res.yaml >/dev/null 2>&1 && ok=$?
# if return code is 0, resources are installed. So, we will delete them
if [ -z "${ok+set}" ]
then
  echo "Resources not installed!"
elif [ $ok -eq 0 ]
then
  # https://stackoverflow.com/questions/55672498/kubernetes-clustsr-stuck-on-removing-pv-pvc
  sudo -u $USERNAME kubectl patch pv static-pv -p '{"metadata":{"finalizers":null}}'
  sudo -u $USERNAME kubectl patch pvc pvc -p '{"metadata":{"finalizers":null}}'

  sudo -u $USERNAME kubectl delete -f manifests/common-res.yaml --force --wait=false
  echo "Common resources deleted!"
else
  echo "A problem has occurred while deleting resources!"
fi

DIR="/mnt/disk/vol1"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "Deleting directory containing NN weights"
  rm -rf $DIR
else
  ###  Control will jump here if $DIR does NOT exists ###
  echo "Error: ${DIR} not found. Skipping..."
fi

sudo -u $USERNAME kubectl diff -f manifests/scheduler-inference.yaml >/dev/null 2>&1 && ok=$?
if [ -z "${ok+set}" ]
then
  echo "Resources not installed!"
elif [ $ok -eq 0 ]
then
  sudo -u $USERNAME kubectl delete -f manifests/scheduler-inference.yaml
  echo "Scheduler trainer resources deleted!"
else
  echo "A problem has occurred while deleting resources!"
fi

# Recover kube-scheduler.yaml and delete braine-sched-rl.yaml
echo "Recovering default kube-scheduler"
SCHED_POD_FILE="/etc/kubernetes/kube-scheduler.yaml"
if [ ! -f $SCHED_POD_FILE ]; then
    echo "${SCHED_POD_FILE} not found! Skipping..."
else
    cp $SCHED_POD_FILE /etc/kubernetes/manifests/
fi

KUBE_SCHED_CONFIG_FILE="/etc/kubernetes/braine-sched-rl.yaml"
if [ ! -f $KUBE_SCHED_CONFIG_FILE ]; then
    echo "${KUBE_SCHED_CONFIG_FILE} not found! Skipping..."
else
    rm $KUBE_SCHED_CONFIG_FILE
fi

# check if scheduler is running
time_slept=0
while [ $time_slept -lt 180 ]
do
  echo "Waiting for scheduler pod to be ready..."
  SCHED_STATUS=$(sudo -u $USERNAME kubectl get pods  -l component=kube-scheduler -n kube-system --no-headers -o custom-columns=":status.phase")
  SCHED_READY=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}')
  SCHED_IMAGE=$(sudo -u $USERNAME kubectl get pods -l component=kube-scheduler -n kube-system -o=jsonpath="{.items[0].spec.containers[0].image}{'\n'}")
  sleep 3
  time_slept=$((time_slept + 3))
  echo "Waited ${time_slept}/180 s."
  if [ $SCHED_STATUS = "Running" ] && [[ $SCHED_IMAGE = "k8s.gcr.io/kube-scheduler"* ]] && [ $SCHED_READY = "True" ]
  then
      echo "kube-scheduler is running using the image: ${SCHED_IMAGE}"
      break
  fi
done

if [ $RM_CS = "yes" ]
then
  echo "Removing custom scheduler Docker image"
  docker image prune --force --filter label=stage=builder
  docker image rm localhost:5000/scheduler-plugins/kube-scheduler || echo "Error while removing image: localhost:5000/scheduler-plugins/kube-scheduler"
fi

if [ $RM_TRAINER = "yes" ]
then
  echo "Removing scheduler trainer Docker image"
  docker image rm localhost:5000/scheduler-trainer:latest || echo "Error while removing image: localhost:5000/scheduler-trainer:latest"
fi

if [ $RM_INF = "yes" ]
then
  echo "Removing scheduler inference Docker image"
  docker image rm localhost:5000/scheduler-inference:latest || echo "Error while removing image: localhost:5000/scheduler-inference:latest"
fi