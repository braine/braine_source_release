#Since We are not using d3rlpy
#from d3rlpy.algos import DQN
import torch
import torch.nn as nn
import torch.autograd as autograd
import torch.optim as optim
from common.drl_algs.buffer_methods import *
import operator


# Base code was taken from https://github.com/higgsfield/RL-Adventure
class DQN(nn.Module):
    def __init__(self,
                 env,
                 model_cfg,
                 mask_env):
        super(DQN, self).__init__()

        self.USE_CUDA = torch.cuda.is_available()
        self.Variable = lambda *args, **kwargs: autograd.Variable(*args,
                                                                  **kwargs).cuda() if self.USE_CUDA else autograd.Variable(
            *args, **kwargs)
        self.env = env
        self.model_type = model_cfg['MODEL_ARCH']
        self.buffer_type = model_cfg['BUFFER_TYPE']
        self.gamma = model_cfg['GAMMA']
        self.optimizer = None
        self.batch_size = model_cfg['BATCH_SIZE']
        self.num_actions = model_cfg['OUTPUT_NODES']
        self.lr = model_cfg['LR']
        self.mask_env = mask_env

        # Generate the hidden states
        layers = [nn.Linear(self.env.observation_space.shape[0], 128)]
        # -1 becuase the first linera layer generates one hidden layer
        for i in range(model_cfg['NUM_H'] - 1):
            layers.append(nn.Linear(128, 128))
            layers.append(nn.ReLU())
        layers.append(nn.Linear(128, env.action_space.n))

        # A current and target model is helpful for training
        self.current_model = nn.Sequential(*layers)
        self.target_model = nn.Sequential(*layers)

        if self.USE_CUDA:
            self.current_model = self.current_model.cuda()
            self.target_model = self.target_model.cuda()

        self.optimizer = optim.AdamW(self.current_model.parameters(), lr=self.lr)

        self.target_model.load_state_dict(self.current_model.state_dict())

        if self.buffer_type == 'PER':
            self.replay_buffer = NaivePrioritizedBuffer(100000, 0.9)
        elif self.buffer_type == 'normal':
            self.replay_buffer = ReplayBuffer(100000)

    def forward(self, x):
        return self.current_model(x)

    def predict(self, x):
        state1 = self.Variable(torch.FloatTensor(x).unsqueeze(0))
        q_value = self.forward(state1)
        action = q_value.max(1)[1].data[0]
        action = action.item()
        return action

    def act(self, state, epsilon):
        # If masking enviornment is not being used just set the mask to a list of 1s
        if not self.mask_env:
            mask = np.ones(self.num_actions)
        else:
            mask = self.env.get_valid_act_mask()

        machines_from_mask = np.where(np.array(mask) == 1)[0]
        if random.random() > epsilon:
            state = self.Variable(torch.FloatTensor(state).unsqueeze(0))
            q_value = self.forward(state)
            q_list = q_value.tolist()[0]

            q_valid_dict = {v_action:q_list[v_action] for v_action in machines_from_mask}
            action = max(q_valid_dict.items(), key=operator.itemgetter(1))[0]

        else:
            action = int(np.random.choice(machines_from_mask, 1))

        return action

    def update_target_model(self):

        self.target_model.load_state_dict(self.current_model.state_dict())

    def get_replay_buffer(self):

        state, action, reward, next_state, done, weights, indices = self.replay_buffer.sample(self.batch_size,
                                                                                              self.beta)

        return state, action, reward, next_state, done, weights, indices

    def compute_td_loss(self):
        state, action, reward, next_state, done, weights, indices = self.get_replay_buffer()

        state = self.Variable(torch.FloatTensor(np.float32(state)))
        next_state = self.Variable(torch.FloatTensor(np.float32(next_state)))
        action = self.Variable(torch.LongTensor(action))
        reward = self.Variable(torch.FloatTensor(reward))
        done = self.Variable(torch.FloatTensor(done))
        if self.buffer_type == 'PER' or self.buffer_type == 'BPER':
            weights = self.Variable(torch.FloatTensor(weights))

        q_values = self.current_model(state)
        next_q_values = self.target_model(next_state)

        q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
        if self.model_type == 'dqn':
            next_q_value = next_q_values.max(1)[0]
        elif self.model_type == 'ddqn':
            next_q_state_values = self.target_model(next_state)
            next_q_value = next_q_state_values.gather(1, torch.max(next_q_values, 1)[1].unsqueeze(1)).squeeze(1)
        expected_q_value = reward + self.gamma * next_q_value * (1 - done)

        # We could simplify code if for the standard case weights = 1 or a vector of 1's
        td_error = q_value - expected_q_value.detach()

        if self.buffer_type == 'PER' or self.buffer_type == 'BPER':
            loss = (td_error).pow(2) * weights
        else:
            loss = (q_value - self.Variable(expected_q_value.data)).pow(2).mean()

        loss = loss.mean()
        self.optimizer.zero_grad()
        loss.backward()

        if self.buffer_type == 'PER' or self.buffer_type == 'BPER':
            prios = td_error.abs() + 1e-5
            self.replay_buffer.update_priorities(indices, prios.data.cpu().numpy())

        self.optimizer.step()
        return loss

