GYM_ENV_CFG = {
    'NB_NODES': 8,
    'REWARD_TYPE': 'exp_frag',
    'SEED': 42,
    'cpu_req': 't-uniform',
    'mem_req': 't-uniform',
    'disk_req': 't-uniform',
    'USE_SCHED_CLASS': False,
    'TASK_LOAD': 'LOW',
    'MC_CAP': 'AAAAAAAA',
    'MM_CAP': 'AAAAAAAA',
    'MD_CAP': 'AAAAAAAA',
    'ACTUAL_USG': True,
    'ACT_MASK': True,
    'WAIT_ENV': True,
    'SCALE_MODE': 'machine_limit',
    'NB_RES_DIM': 3,
    'TERM_COND': 'not-time',
    'RUNTIME': 1e4,
    'nextstate_lookahead': False
}

# MC_CAP_VALUES (Machine CPU Capacity values) is a representation of
# the total cores available per machine type
# MM_CAP_VALUES (Machine Memeory) represents the total normalized
# memory available per machine type

GLOBAL_CFG = {
    'TASK_DURS_LOW': [30, 60, 128, 256],
    'COMPARISON_ALGORITHMS': ['G-Util', 'G-ExpFrag', 'G-MAvgFrag', 'G-Frag', 'Random', 'K8s'],
    'TASK_DURS_MEDIUM': [],
    'TASK_DURS_HIGH': [],
    'MC_CAP_VALUES': {'A': 1, 'B': 1, 'C': 1, 'D': 1, 'E': 1},
    'MM_CAP_VALUES': {'A': 1, 'B': 0.5, 'C': 0.25, 'D': 0.12, 'E': 0.06},
    'MD_CAP_VALUES': {'A': 1, 'B': 0.5, 'C': 0.25, 'D': 0.12, 'E': 0.06},
    'SEED': 42,
    'features_to_include': ['cpu_req', 'mem_req', 'disk_req'],
    'DATA_LOCATION': 'common/input/task_data_uniform_all_dimensions.zip',
    'PRINT_RESULTS_RATE': 200,
}
# 'EPS_DECAY of  10_000_000 takes 32_088_255 steps to get to 0.05
#                45_951_199 to get to 0.02
# 'EPS_DECAY of  1_000_000 takes 3_208_826 steps to get to 0.05
#                4_595_120 to get to 0.02
# 'EPS_DECAY of  100_000 takes 320_883 steps to get to 0.05
#                459_512 to get to 0.02
# 'EPS_DECAY of  1_000 takes 3209 steps to get to 0.05
#                4596 to get to 0.02
DRL_CFG = {
    'BATCH_SIZE': 64,
    'TRAIN_PERCENTAGE': 0.8,
    'DATASET_TRAIN_LEN': 100_000,
    'TOTAL_FRAMES_PER_EPI': 1_000,
    'TRAIN_EPISODES': 5, #number of episodes to train for
    'EPS_START': 1, # Epsilon controls the explore/exploit rate
    'EPS_FINAL': 0.01,
    'EPS_DECAY': 100_000,
    'BETA_START': 0.4, # Control the PER sample weights
    'BETA_FRAMES': 10_000,
    'GAMMA': 0.99,
    'LR': 1e-3,
    'NUM_H': 5,
    'TARGET_UPD_INT': 1000,
    'MODEL_ARCH': 'ddqn',
    'BUFFER_TYPE': 'PER',
    'OUTPUT_NODES': GYM_ENV_CFG['NB_NODES'],

}

if GYM_ENV_CFG['TERM_COND'] == 'time':
    DRL_CFG['MAX_STEPS'] = int(GYM_ENV_CFG['RUNTIME'] * DRL_CFG['TRAIN_EPISODES'])
else:
    DRL_CFG['MAX_STEPS'] = DRL_CFG['TOTAL_FRAMES_PER_EPI'] * DRL_CFG['TRAIN_EPISODES']