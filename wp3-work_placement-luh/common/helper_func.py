import math
import numpy as np
import operator
import copy
import os
import pandas as pd
import glob
from common.config import GYM_ENV_CFG, GLOBAL_CFG, DRL_CFG
from ast import literal_eval

# Takes a Dataset and which features to index.
# Returns a dictionary for the attributes so we can reference
# the attribute name and get the correct index   -john16/7/21
def gen_states(dataset, features_to_include):
    attr_idx = {}
    for idx, val in enumerate(dataset.columns.to_list()):
        attr_idx[val] = idx
    state_indices = []
    for elm in features_to_include:
        state_indices.append(attr_idx[elm])
    return attr_idx, state_indices


def exponential_decay_func(start, final, decay):
    return lambda frame_idx: final + (start - final) * math.exp(
        -1. * frame_idx / decay)


def beta_func(start, frames):
    return lambda x: min(1.0, start + x * (1.0 - start) / frames)


# Not currently being used -john16/7/21
def smoother_func(vector, smoother):
    if len(vector) % smoother != 0 and len(vector) > smoother:
        diff = len(vector) % smoother
        vector = vector[diff:]
    else:
        return vector
    return np.mean(vector.reshape(-1, smoother), axis=1)



def generate_kube_action(env, valid_actions):
    # based on formula here: https://github.com/kubernetes/kubernetes/blob/b96c86f1b9d92880845724d2030da3e2adac89e5/pkg/scheduler/framework/plugins/noderesources/least_allocated.go#L59
    mach_status = env.machine_status
    scores = {}
    scores_valid = {}
    MaxNodeScore = 1
    for mach in mach_status:
        cpu_capacity, mem_capacity, disk_capacity = env.machine_limits(mach)
        task_list = mach_status[mach]
        requested_cpu = []
        requested_mem = []
        requested_disk = []
        for item in task_list:
            cpu_req = item['cpu_req']
            mem_req = item['mem_req']
            disk_req = item['disk_req']
            requested_cpu.append(cpu_req)
            requested_mem.append(mem_req)
            requested_disk.append(disk_req)

        cpu_score = (cpu_capacity - sum(requested_cpu)) * MaxNodeScore / cpu_capacity
        mem_score = (mem_capacity - sum(requested_mem)) * MaxNodeScore / mem_capacity
        disk_score = (disk_capacity - sum(requested_disk)) * MaxNodeScore / disk_capacity
        scores[mach] = cpu_score + mem_score + disk_score

        if mach in valid_actions:
            scores_valid[mach] = scores[mach]
    return max(scores_valid.items(), key=operator.itemgetter(1))[0]


def calculate_per_unused(usages, nb_res_dims):
    nb_unused = 0
    nb_nodes = int(len(usages) / nb_res_dims)
    for i_m in range(nb_nodes):
        if usages[i_m] == 0 and usages[i_m + nb_nodes] + usages[i_m + 2 * nb_nodes] == 0:
            nb_unused += 1
    perc_unused = round((nb_unused / nb_nodes) * 100, 2)
    return perc_unused


# Needs a better function name
def full_episode_reward(train_env, results_dict):
    for mode in GLOBAL_CFG['COMPARISON_ALGORITHMS']:
        print(mode)
        env_copy = copy.deepcopy(train_env)
        env_copy.testing = True
        done = False
        epi_reward = 0
        while not done:
            if mode == 'G-Util':
                action = greedy_policy(env_copy, 'util')
            elif mode == 'G-ExpFrag':
                action = greedy_policy(env_copy, 'exp_frag')
            elif mode == 'G-MAvgFrag':
                action = greedy_policy(env_copy, 'm_avg_frag')
            elif mode == 'G-Frag':
                action = greedy_policy(env_copy, 'frag')
            elif mode == 'Random':
                mask = env_copy.get_valid_act_mask()
                machines_from_mask = np.where(np.array(mask) == 1)[0]
                action = int(np.random.choice(machines_from_mask, 1))
            elif mode == 'K8s':
                mask = env_copy.get_valid_act_mask()
                machines_from_mask = np.where(np.array(mask) == 1)[0]
                action = generate_kube_action(env_copy, machines_from_mask)
            else:
                print('incorrect policy used')
            next_state, reward, done, _ = env_copy.step(action)
            epi_reward += reward

        results_dict['wait'][mode].append(env_copy.cum_waittime)
        results_dict['runtime'][mode].append(env_copy.elapsed_time)
        results_dict['reward'][mode].append(epi_reward)
        results_dict['numjobs'][mode].append(env_copy.i)

    return results_dict


def greedy_policy(env, reward_type):
    greedy_list = []
    state = env.state
    mask = env.get_valid_act_mask()

    machines_from_mask = np.where(mask == 1)[0]
    for machine in machines_from_mask:
        usages = state[env.usg_start:].copy()
        updated_usages = env.update_usages(usages, machine)
        reward = env.get_reward(machine, updated_usages, reward_type)
        greedy_list.append(reward)


    max_val = np.max(greedy_list)
    max_indicies = np.where(greedy_list == max_val)[0]
    # Get max value to check how many machines share the same max_value.  If more than one
    # machines share the same maximum reward value, randomly pick one element.
    if len(max_indicies) > 1:
        max_idx = np.random.choice(max_indicies)
    else:
        max_idx = np.argmax(greedy_list)
    return machines_from_mask[max_idx]


def pre_process_data(mode='train'):
    task_data = pd.read_csv(GLOBAL_CFG['DATA_LOCATION'])

    if not GYM_ENV_CFG['USE_SCHED_CLASS']:
        if 'sched_class' in task_data.columns:
            task_data = task_data.drop(columns=['sched_class'])

    else:
        encoded_class = pd.get_dummies(task_data.sched_class)
        encoded_class.columns = ['sc0', 'sc1', 'sc2', 'sc3']
        task_data.drop(columns=['sched_class'], inplace=True)
        task_id = list(task_data.task_id_2)
        encoded_class['task_id_2'] = task_id
        task_data = pd.merge(task_data, encoded_class, on='task_id_2')

    attr_idx, state_indices = gen_states(dataset=task_data, features_to_include=GLOBAL_CFG['features_to_include'])

    dataset_len = len(task_data)
    train_test_split = int(DRL_CFG['TRAIN_PERCENTAGE'] * dataset_len)
    if mode == 'train':
        data_subset = task_data.to_numpy()[:train_test_split, :]
    else:
        data_subset = task_data.to_numpy()[train_test_split:, :]


    task_durs = GLOBAL_CFG['TASK_DURS_' + GYM_ENV_CFG['TASK_LOAD']]
    subset_task_durs = np.random.choice(task_durs, data_subset.shape[0])

    return data_subset, subset_task_durs, attr_idx, state_indices


def get_latest_save_number():
    existing_files = glob.glob('test_results/*.npz')
    files_numbers = []
    for elm in existing_files:
        # I understand this piece of code is a bit funny, but can't think of a better solution now.
        # Remove comment before publishing! :)
        files_numbers.append(int(elm.split('/')[-1].split('.')[0]))
    files_numbers.sort(key=int)
    print('files_numbers', files_numbers)
    if len(existing_files) == 0:
        save_number = 0
    else:
        latest_file_number = files_numbers[-1]
        save_number = int(latest_file_number)
    return save_number


def save_results(all_rewards, losses, gpas, episodes_waittime, episodes_reward):
    save_number = get_latest_save_number() + 1

    outline = '%i.npz\t%s\t%s' % (save_number, GYM_ENV_CFG, DRL_CFG)

    # Mover this to a configuration file later
    arry_names = ['training_rewards',
                  'losses',
                  'greedy_action_accuracy_avg',
                  'waiting_dict',
                  'rewards_dict']

    outline += '\t' + str(arry_names)
    outline += '\n'

    if not os.path.isdir('test_results'):
        os.mkdir('test_results')

    with open('test_results/test_logs.tsv', 'a') as the_file:
        the_file.write(outline)

    np.savez_compressed('test_results/%s' % save_number,
                        training_rewards=all_rewards,
                        losses=losses,
                        greedy_action_accuracy_avg=gpas,
                        waiting_dict=episodes_waittime,
                        rewards_dict=episodes_reward,
                        arry_names=arry_names)
    print('Saved file with filename %s.npz: ' % save_number)
    return


def test_extractor(tests_info, filename):
    single_test_results = np.load('test_results/' + filename, allow_pickle=True)
    single_test_info = tests_info.loc[tests_info.filename == filename]

    array_names = literal_eval(single_test_info['array_names'].iloc[0])

    dstr = dict(single_test_results)
    dstr['rewards_dict'] = dstr['rewards_dict'].item()
    dstr['waiting_dict'] = dstr['waiting_dict'].item()

    return dstr, array_names


def display_progress(env, total_steps,episode):
    if total_steps % GLOBAL_CFG['PRINT_RESULTS_RATE'] == 0 and not env.done:
        if GYM_ENV_CFG['TERM_COND'] == 'time':
            current_runtime = env.elapsed_time + (GYM_ENV_CFG['RUNTIME'] * episode)
            max_time = GYM_ENV_CFG['RUNTIME'] * DRL_CFG['TRAIN_EPISODES']
            p_complete = (current_runtime / max_time) * 100
            print('epi%i %%: %.3f , training %%: %.3f' % (
            episode, 100 * env.elapsed_time / GYM_ENV_CFG['RUNTIME'], p_complete))

        else:
            total_steps = env.i + (DRL_CFG['TOTAL_FRAMES_PER_EPI'] * episode)
            print('epi%i %%: %.3f , training %%: %.3f' % (episode,
                                                          100 * env.i / DRL_CFG['TOTAL_FRAMES_PER_EPI'],
                                                          100 * total_steps/DRL_CFG['MAX_STEPS']
                                                          )
                  )