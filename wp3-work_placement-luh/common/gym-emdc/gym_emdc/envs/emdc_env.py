import gym
from gym import spaces
from common.rewards import *


class EMDCEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self,
                 task_list,
                 state_indices,
                 attr_idx,
                 task_dur_list,
                 gym_cfg,
                 global_cfg,
                 drl_cfg,
                 start_idx=0,
                 testing=False,
                 ):

        super().__init__()

        self.gym_cfg = gym_cfg
        self.drl_cfg = drl_cfg
        self.global_cfg = global_cfg
        # Number of resource dimensions, 3 if considering cpu, mem, disk
        self.nb_res_dims = gym_cfg['NB_RES_DIM']
        self.run_time =  gym_cfg['RUNTIME']

        self.term_cond = gym_cfg['TERM_COND']
        self.max_steps_per_epi = drl_cfg['TOTAL_FRAMES_PER_EPI']
        self.nb_w_nodes = gym_cfg['NB_NODES']
        self.wait_env = gym_cfg['WAIT_ENV']
        self.scale_mode = gym_cfg['SCALE_MODE']
        self.reward_type = gym_cfg['REWARD_TYPE']
        # Flag to indicate whether node usages are updated based on actual task usages or using the requested amount.
        self.actual_usg_flag = gym_cfg['ACTUAL_USG']
        # Flag to indicate whether we're using the environment to test/evaluate algorithms or not.
        self.masking = gym_cfg['ACT_MASK']  # indicates whether we apply action masking or not

        # Define action and observation space. They must be gym.spaces objects
        # Example when using discrete actions:
        self.action_space = spaces.Discrete(self.nb_w_nodes)
        # Mapping from real column/attribute name to idx
        self.attr_idx = attr_idx

        np.random.seed(gym_cfg['SEED'])

        # The indexes corresponding to the state elements in a given row of task_list
        self.state_indices = state_indices

        # 6+2*self.nb_w_nodes:  for cpu request, mem request, sched_class(one-hot encodedx4), then
        # we have nodes' cpu usages and memory usages

        self.observation_space = spaces.Box(low=0,
                                            high=1,
                                            shape=(len(self.state_indices) + self.nb_res_dims * self.nb_w_nodes,),
                                            dtype=np.float32)

        # To iterate through rows of task list
        self.i = 0
        self.current_waittime = 0
        self.cum_waittime = 0
        self.task_list = task_list
        self.task_list_len = len(task_list)
        # This indicates the desired starting index in task_list
        self.start_idx = start_idx
        self.usg_start = len(self.state_indices)

        self.state = np.zeros(self.usg_start + self.nb_res_dims * self.nb_w_nodes)

        self.state[:self.usg_start] = self.task_list[self.start_idx][self.state_indices]

        # This gives a view of the machines at a given time:
        # Which tasks are they running, how much resources they are using and when they will be completed
        # Updated with each time step
        self.machine_status = {}
        for idx in range(self.nb_w_nodes):
            self.machine_status[idx] = []

        self.done = False  # indicates whether this is the end of the episode

        self.task_dur_list = task_dur_list

        #Used so we don't shuffle the dataset during testing a trained model
        self.testing = testing
        self.res_dim_map = {0: 'cpu', 1: 'mem', 2: 'disk'}

        # Using something else other then time right now to not break the code.
        self.elapsed_time = 0

        self.times_to_completion = []

    def step(self, action):

        # Add new task to machine and update state
        real_cpu_usg, real_mem_usg, real_disk_usg = self.get_task_usage()
        rem_time = self.task_dur_list[self.i + self.start_idx]

        self.machine_status[action].append({'cpu': real_cpu_usg,
                                            'mem': real_mem_usg,
                                            'disk': real_disk_usg,
                                            'rem_time': rem_time,
                                            'task_id': self.i,
                                            'cpu_req': self.task_list[self.i + self.start_idx][
                                                self.attr_idx['cpu_req']],
                                            'mem_req': self.task_list[self.i + self.start_idx][
                                                self.attr_idx['mem_req']],
                                            'disk_req': self.task_list[self.i + self.start_idx][
                                                self.attr_idx['disk_req']]})
        # Update the state because we added a task to machine_status
        self.update_state()
        next_state = self.state.copy()

        # Calculate reward
        usages = self.state[self.usg_start:].copy()

        # # Used for next step in the learning process
        # # Prepares the state for the next incomming task
        self.prepare_next_step()

        reward = self.get_reward(action, usages)

        self.done = self.termination_check()

        # Add the next task request to the state
        if self.gym_cfg['nextstate_lookahead']:
            next_state[:self.usg_start] = self.state[:self.usg_start]

        return next_state, reward, self.done, {}

    def termination_check(self):
        # Applies to all tests
        if self.no_more_data():
            return True

        if self.term_cond == 'time':
            return self.over_runtime()
        else:
            return self.no_more_steps()

    def prepare_next_step(self):
        # Update the part of the state corresponding to the next task's attributes
        # No need to do this when this is the last row of task_list

        self.i += 1
        self.update_task_req()

        # Previously was update_machine_status.  It makes more sence to update all states in time and then to
        # add the next state, so this was put above the addition of the task
        self.time_jump_machine_status()

        # Checks if there availability for the next task, and if not it will jump forward in time untill
        # there is an availability

        if self.get_valid_act_mask().sum() == 0:
            self.jump_to_availability()


    # If the current task cannot go on a machine we progress forward in time based on the remaining time in the
    # currently running tasks
    def jump_to_availability(self):
        # No machines are available for the current task so we will progress in time by the minimum remaining
        # time remaining
        if len(self.times_to_completion) == 0:
            return
        self.times_to_completion.sort()
        completion_times_iter = iter(self.times_to_completion)

        # Here we record current jump and previous jump to later take the difference in time between the last jump
        # and the current jump to get the correct cumulative change in time that task_dict['rem_time'] experiences
        # Another way to look at it is that we subtract the previous jump to cancel it from
        # task_dict['rem_time'] and just consider the most recent jump in time (curr_jump)
        curr_jump = next(completion_times_iter)
        prev_jump = 0

        self.current_waittime = 0
        # Loop while there are no valid actions left
        while self.get_valid_act_mask().sum() == 0:
            relative_jump = curr_jump - prev_jump
            self.current_waittime += relative_jump

            for machine in self.machine_status:
                all_tasks = self.machine_status[machine]
                still_running_tasks = []
                for task_dict in all_tasks:

                    task_dict['rem_time'] -= relative_jump
                    # Uncompleted tasks with time remaining
                    if task_dict['rem_time'] > 0:
                        still_running_tasks.append(task_dict)
                self.machine_status[machine] = still_running_tasks


            prev_jump = curr_jump
            curr_jump = next(completion_times_iter)
            self.update_state()

        self.cum_waittime += self.current_waittime
        self.elapsed_time += self.current_waittime
        return



    # Progresses forward in time by a timestep of magnitude 'tsp'
    def time_jump_machine_status(self):

        tsp = self.time_since_prev()
        self.elapsed_time += tsp

        self.times_to_completion = []
        for machine in self.machine_status:
            running_tasks = self.machine_status[machine]
            # possible slowness ehre
            still_running_tasks = []
            # decrement remaining time for tasks by 1
            for task_dict in running_tasks:
                task_dict['rem_time'] -= tsp
                # Only include tasks that have not completed
                if task_dict['rem_time'] > 0:
                    still_running_tasks.append(task_dict)

                    # Keep track of the smallest remaining time
                    if task_dict['rem_time'] not in self.times_to_completion:
                        self.times_to_completion.append(task_dict['rem_time'])
            self.machine_status[machine] = still_running_tasks
        return


    # Here we will generate how much time has passed since the previous submission.
    # This will allow us to dynamically change the task generation rate
    # e.g. if the machines are overloaded we can start to generate less frequently.
    # Time units are in seconds

    # in reality we would have this
    def time_since_prev(self):

        # Don't dynamically generate values but either return a constant, or follow some pre-defined
        # function based on time
        if self.testing:
            return 0.5

        # Shouldn't just be a fixed number, but will work for now
        # Can be a random number generated with a specific mean

        # One possibly call a funtion to get the overall average usage, and generate the usages
        # to supply a constant number of cpu usage
        # E.g. if OMU (overall machine usage) < 0.9 : create tasks at a rate of once per 10ms
        #   else : create tasks at rate of 100ms

        # or we can do a check if the machine has to wait (due to overloaded machines)  we reduce the task generation
        # rate by 10ms or so, and repeat this untill the machines never have to wait

        # Side note is that we probably wouldn't want to do this dynamic task generation when making comparisons with
        # different algorightms e.g. K8, Greedy.

        return 0.5

    # As tasks are removed due to the progression of time we need to calculate update_state to show the correct
    # usage across all machiens

    # This would be more efficient if we are able to just dynamically add/subtract tasks as they start/expire
    # It would not require that we loop over each task every step.
    def update_state(self):
        for machine in self.machine_status:

            # More efficent
            tot_cpu_usg, tot_mem_usg, tot_disk_usg = 0, 0, 0
            for item in self.machine_status[machine]:
                tot_cpu_usg += item['cpu']
                tot_mem_usg += item['mem']
                tot_disk_usg += item['disk']
            if self.scale_mode == 'resource_usage':
                tot_cpu_usg, tot_mem_usg, tot_disk_usg = self.scaled_task_usage(machine,
                                                                                tot_cpu_usg,
                                                                                tot_mem_usg,
                                                                                tot_disk_usg)

            # update the cpu usage of machine machine based on the sum of cpu usages of the tasks running on it
            self.state[self.usg_start + machine] = tot_cpu_usg
            self.state[self.usg_start + self.nb_w_nodes + machine] = tot_mem_usg
            self.state[self.usg_start + 2 * self.nb_w_nodes + machine] = tot_disk_usg

    def wait(self):
        self.update_machine_status()
        self.update_state()

    # Calls the wait function until atleast one machine is open
    def wait_for_availability(self):

        cnt = 0
        # Exit loop when a machine can take the current task
        while not self.done:
            # A or B.  A is only for if the termination condition is time,  while B applies
            # to all enviornments and just checks if we reach the end of the dataset
            if self.term_cond == 'time' and self.time >= self.run_time or self.no_more_data():
                break
            # No machines are available to take the current task
            if self.get_valid_act_mask().sum() == 0:
                cnt += 1
                self.time += 1
                self.wait()
            else:
                break
        self.current_waittime = cnt
        self.cum_waittime += self.current_waittime
        return

    def shuffle_dataset(self):
        np.random.shuffle(self.task_list)
        np.random.shuffle(self.task_dur_list)

    def reset(self):
        self.i = 0  # to iterate through rows of task list
        self.elapsed_time = 0
        self.cum_waittime = 0
        self.current_waittime = 0
        if not self.testing:
            self.shuffle_dataset()

        self.state = np.zeros(self.usg_start + self.nb_res_dims * self.nb_w_nodes)
        self.state[:self.usg_start] = self.task_list[self.start_idx][self.state_indices]

        # This gives a view of the machines at a given time:
        # what tasks are they running, how much resources they are using and when they will be completed
        # will be updated in each time step
        self.machine_status = {}
        for idx in range(self.nb_w_nodes):
            self.machine_status[idx] = []

        # Indicates whether this is the end of the episode
        self.done = False

        return self.state

    def render(self, mode='human', close=False):
        pass

    def update_machine_status(self):
        for machine in self.machine_status:
            running_tasks = self.machine_status[machine]
            # possible slowness ehre
            running_tasks_temp = []
            # decrement remaining time for tasks by 1
            for task_dict in running_tasks:
                task_dict['rem_time'] -= 1
                # remove tasks where rem_time = 0 because they are no longer running on machine
                if task_dict['rem_time'] > 0:

                    running_tasks_temp.append(task_dict)

                    # Keep track of the smallest remaining time
                    if task_dict['rem_time'] < self.min_rem_time:
                        self.min_rem_time = task_dict['rem_time']
            self.machine_status[machine] = running_tasks_temp
            return
    def one_node_full(self):
        usages = self.state[self.usg_start:]
        # check if there is any element with a usage value (either cpu or memory or disk) greater than 1
        if len([i for i in usages if i >= 1]) > 0:
            return True
        else:
            return False

    def set_start_idx(self, value):
        self.start_idx = value

    # Returns an array that represents if individual machines are available for a task
    # to be schedled on it (1), or not (0).
    def get_valid_act_mask(self):
        # No machine masking is being used, so just return all 1's, which means all
        # machiens are available
        if not self.masking:
            valid_act_mask = np.ones(self.nb_w_nodes)
        else:
            req_cpu, req_mem, req_disk = self.state[:self.usg_start]
            cpu_usages = self.state[self.usg_start:self.usg_start + self.nb_w_nodes]
            mem_usages = self.state[self.usg_start + self.nb_w_nodes:self.usg_start + 2 * self.nb_w_nodes]
            disk_usages = self.state[self.usg_start + 2 * self.nb_w_nodes:]
            valid_act_mask = np.zeros(self.nb_w_nodes)

            for machine in range(self.nb_w_nodes):
                tmp_cpu_usg = req_cpu
                tmp_mem_usg = req_mem
                tmp_disk_usg = req_disk

                if self.scale_mode == 'resource_usage':
                    tmp_cpu_usg, tmp_mem_usg, tmp_disk_usg = self.scaled_task_usage(machine,
                                                                                    req_cpu,
                                                                                    req_mem,
                                                                                    req_disk)
                    m_cpu_limit = 1
                    m_mem_limit = 1
                    m_disk_limit = 1

                # Scale mode is  'machine_limit'
                else:
                    m_cpu_limit, m_mem_limit, m_disk_limit = self.machine_limits(machine)

                # If adding the task usage to the total machine usage results in a usage less than 1,
                # then that machine is a feasible action.
                if tmp_cpu_usg + cpu_usages[machine] <= m_cpu_limit and \
                        tmp_mem_usg + mem_usages[machine] <= m_mem_limit and \
                        tmp_disk_usg + disk_usages[machine] <= m_disk_limit:
                    valid_act_mask[machine] = 1

        return valid_act_mask

    # Function used during the greedy policy calculation.  Note that the only the usages are passed to this function
    def update_usages(self, usages, action):

        real_cpu_usg, real_mem_usg, real_disk_usg = self.get_task_usage()

        if self.scale_mode == 'resource_usage':
            real_cpu_usg, real_mem_usg, real_disk_usg = self.scaled_task_usage(action,
                                                                               real_cpu_usg,
                                                                               real_mem_usg,
                                                                               real_disk_usg)

        # this will update the CPU usage for the machine represented by 'action'.
        usages[action] += real_cpu_usg
        # this will update the memory usage for the machine represented by 'action'.
        usages[action + self.nb_w_nodes] += real_mem_usg

        usages[action + (self.nb_res_dims - 1) * self.nb_w_nodes] += real_disk_usg

        return usages

    # return cpu/mem task usages depending on whether we're using actual usage or assuming requests
    def get_task_usage(self, usg_mode = 'real'):
        if self.actual_usg_flag:
            cpu_usg_col = self.attr_idx['cpu_rate']
            # real CPU usage of the task currently being scheduled
            real_cpu_usg = self.task_list[self.i + self.start_idx][cpu_usg_col]

            mem_usg_col = self.attr_idx['can_mem_usg']
            # real mem usage of the task currently being scheduled
            real_mem_usg = self.task_list[self.i + self.start_idx][mem_usg_col]

            disk_usg_col = self.attr_idx['local_disk_usg']
            # real disk usage of the task
            real_disk_usg = self.task_list[self.i + self.start_idx][disk_usg_col]
        else:
            real_cpu_usg = self.state[0]  # We assume the task will end up using exactly its CPU request.
            real_mem_usg = self.state[1]  # Same for memory
            real_disk_usg = self.state[2]

        return real_cpu_usg, real_mem_usg, real_disk_usg

    def machine_limits(self, machine):
        machine_cpu_type = self.gym_cfg['MC_CAP'][machine]
        machine_cpu_cap = self.global_cfg['MC_CAP_VALUES'][machine_cpu_type]
        machine_mem_type = self.gym_cfg['MM_CAP'][machine]
        machine_mem_cap = self.global_cfg['MM_CAP_VALUES'][machine_mem_type]
        machine_disk_type = self.gym_cfg['MD_CAP'][machine]
        machine_disk_cap = self.global_cfg['MD_CAP_VALUES'][machine_disk_type]
        return machine_cpu_cap, machine_mem_cap, machine_disk_cap

    def scaled_task_usage(self,machine,cpu_usg, mem_usg, disk_usg):
        machine_cpu_cap, machine_mem_cap, machine_disk_cap = self.machine_limits(machine)
        return cpu_usg/machine_cpu_cap, mem_usg/machine_mem_cap, disk_usg/machine_disk_cap

    def over_runtime(self):
        if self.elapsed_time >= self.run_time:
            self.elapsed_time = self.run_time
            # do something else like exit somehow
            return True
        else:
            return False
    def update_task_req(self):
        if self.i + self.start_idx < len(self.task_list):
            self.state[:self.usg_start] = self.task_list[self.i + self.start_idx][self.state_indices]
        return

    def no_more_steps(self):
        return self.i + self.start_idx >= self.max_steps_per_epi

    def no_more_data(self):
        return self.i + self.start_idx >= self.task_list_len

    # I made this function so other sections of code doesn't have to import
    # the reward library.
    def get_reward(self,action, usages,reward_type= None):
        if reward_type is None:
            reward_type = self.reward_type
        return get_reward(action,
                          usages,
                          self.nb_w_nodes,
                          self.nb_res_dims,
                          self.res_dim_map,
                          self.current_waittime,
                          reward_type)

