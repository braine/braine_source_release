from gym.envs.registration import register

register(
    id='emdc-v0',
    entry_point='gym_emdc.envs:EMDCEnv',
)