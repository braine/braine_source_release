import numpy as np

def get_reward(action,
               usages,
               nb_w_nodes,
               nb_res_dims,
               res_dim_map,
               current_waittime,
               reward_type):
    if reward_type == 'util':
        return util_reward(action, usages, nb_w_nodes, nb_res_dims, res_dim_map)
    elif reward_type == 'under_util':
        reward = under_util_penalty(action, usages, nb_w_nodes)
    elif reward_type == 'wait':
        reward = low_waiting(current_waittime)
    elif reward_type == 'exp_frag':
        reward = experimental_frag(usages, nb_w_nodes)
    elif reward_type == 'm_avg_frag':
        reward = machine_frag_avg(usages, nb_w_nodes)
    elif reward_type == 'frag':
        reward = frag(usages, nb_w_nodes)
    elif reward_type == 'survive':
        reward = survive_reward()
    elif reward_type == 'simple':
        reward = simple_reward(usages, nb_w_nodes)
    elif reward_type == 'nb_unused':
        reward = reward_nb_unused(usages, nb_w_nodes)
    else:
        print(reward_type, ' is not a valid reward type.')
        reward = None
    return reward


# Survive only penalizes the agent if it fills up a machine over capacity
def survive_reward():
    # This reward function can be useful when debugging an enviornment where the enviornment ends
    return 1


# Survive only penalizes the agent if it fills up a machine over capacity
def low_waiting(current_waittime):
    if current_waittime > 1:
        return 0
    else:
        return 1


def experimental_frag(usages, nb_w_nodes):
    cpu = 1 - usages[:nb_w_nodes]
    mem = 1 - usages[nb_w_nodes:nb_w_nodes * 2]
    disk = 1 - usages[nb_w_nodes * 2:]
    frag_val = 0
    for i in range(nb_w_nodes):
        m_cpu = cpu[i]
        m_mem = mem[i]
        m_disk = disk[i]
        max_unused = max([m_disk, m_mem, m_cpu])
        scaled_m_cpu = m_cpu/max_unused
        scaled_m_mem = m_mem / max_unused
        scaled_m_disk = m_disk / max_unused

        frag_val += 1 - scaled_m_cpu * scaled_m_mem * scaled_m_disk
    return frag_val/nb_w_nodes

# Average Fragmentation of maximum on a per-machine basis
def machine_frag_avg(usages, nb_w_nodes):
    # if placement on machine is empty dont return a reward
    # otherwise min-max normalize the cpu, mem, and disk and do a dot product to get the
    # fragmentation metric
    cpu = 1 - usages[:nb_w_nodes]
    mem = 1 - usages[nb_w_nodes:nb_w_nodes * 2]
    disk = 1 - usages[nb_w_nodes * 2:]
    frag_val = 0
    for i in range(nb_w_nodes):
        m_cpu = cpu[i]
        m_mem = mem[i]
        m_disk = disk[i]
        max_unused = max([m_disk, m_mem, m_cpu])
        sum_unused = sum([m_disk, m_mem, m_cpu])
        frag_val += 1 - max_unused/sum_unused
    return frag_val/nb_w_nodes

# Fragmentation of maximum across all machines
def frag(usages, nb_w_nodes):
    free_space = 1 - usages
    max_free = np.max(free_space)
    total_free = np.sum(free_space)

    return 1 - max_free/total_free


def util_reward(action, usages, nb_w_nodes, nb_res_dims, res_dim_map):
    scale_factor = 1
    K_u = 3
    penalty = 1

    # get the usage of the selected machine
    usg_sel_dict = {'cpu': None, 'mem': None, 'disk': None}
    for r in range(nb_res_dims):
        usg_sel_dict[res_dim_map[r]] = usages[action + r * nb_w_nodes]

    return (np.sum(np.power(usages, K_u)) / scale_factor) * penalty

def simple_reward(action, usages, nb_w_nodes):
    # if action is the one with the most resource usage reward = 1
    # otherwise -1

    # sums cpu and mem usage per machine
    usages_3d = [usages[i] + usages[i + nb_w_nodes] + usages[i + 2 * nb_w_nodes] for i in
                 range(nb_w_nodes)]

    # index of the most used machine
    max_i = np.argmax(usages_3d)
    if max_i == action:
        reward = 1
    else:
        reward = -1

    return reward


def under_util_penalty(usages, nb_w_nodes):
    K_u = 3
    temp_list = []
    # retrieve usages of the used machines
    for i_m in range(nb_w_nodes):
        if usages[i_m] > 0 or usages[i_m + nb_w_nodes] > 0 or usages[i_m + 2 * nb_w_nodes] > 0:
            temp_list.append(1 - usages[i_m])
            temp_list.append(1 - usages[i_m + nb_w_nodes])
            temp_list.append(1 - usages[i_m + 2 * nb_w_nodes])

    return -np.sum(np.power(temp_list, K_u))


def reward_nb_unused(usages, nb_w_nodes):
    # find number of unused machines

    nb_unused = 0
    for i_m in range(nb_w_nodes):
        # check if machine is not used on ALL resource dimensions
        if usages[i_m] == 0 and usages[i_m + nb_w_nodes] == 0 and usages[i_m + 2 * nb_w_nodes] == 0:
            nb_unused += 1

    return nb_unused