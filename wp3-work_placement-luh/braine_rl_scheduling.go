package brainerlscheduler

import (
        "bytes"
        "context"
        "encoding/json"
        "fmt"
        "io/ioutil"
        "log"
        "net/http"
        "os"
        "strconv"
        v1 "k8s.io/api/core/v1"
        "k8s.io/apimachinery/pkg/runtime"
        framework "k8s.io/kubernetes/pkg/scheduler/framework/v1alpha1"
        schedutil "k8s.io/kubernetes/pkg/scheduler/util"
        "k8s.io/kubernetes/pkg/scheduler/framework/plugins/helper"
)

var (
        outfile, _ = os.Create("/usr/local/my.log")
        l          = log.New(outfile, "", 0)
        nodeFeatMap      = map[string]map[string]float64{} // this map will hold the node features (i.e. their util rates across all resources)
)

type brainerlscheduler struct {
        handle framework.FrameworkHandle
}

var _ = framework.ScorePlugin(&brainerlscheduler{})
var _ = framework.PreScorePlugin(&brainerlscheduler{})

const (
        Name             = "BRAINE-RL-Scheduling"
        preScoreStateKey = "PreScoreQValues"
)

type PreScoreState struct {
        // framework.Resource
        category map[string]map[string]float32
}

func (qv *PreScoreState) Clone() framework.StateData {
        return qv
}

// Name returns name of the plugin. It is used in logs, etc.
func (ps *brainerlscheduler) Name() string {
        return Name
}

func (ps *brainerlscheduler) PreScore(ctx context.Context, state *framework.CycleState, pod *v1.Pod, nodes []*v1.Node) *framework.Status {
        //_, err := ps.handle.SnapshotSharedLister().NodeInfos().Get(nodeName)
        if len(nodes) == 0 {
                // No nodes to score.
                return nil
        }

        maxCap   := map[string]int64{"cpu": 0, "mem": 0, "st": 0}
        nodeCapacities := map[string]map[interface{}]interface{}{"cpu": {}, "mem": {}, "st": {}}

        for _, node := range nodes {

                // retrieve cpu usage from prometheus
                // Reference for the query: https://stackoverflow.com/questions/34923788/prometheus-convert-cpu-user-seconds-to-cpu-usage
                promBaseUrl := os.Getenv("PROMETHEUS_BASE_URL")
                cpuUsgQuery := promBaseUrl+"/api/v1/query?query=1-avg(rate(node_cpu_seconds_total{mode='idle',instance='"+node.Name+"',job='node-exporter'}[1m]))"
                cpuUsgVal := getValFromQuery(cpuUsgQuery)

                cpuCap, _ := node.Status.Allocatable[v1.ResourceCPU]

                nodeCapacities["cpu"][node.Name] = cpuCap.Value()
                if cpuCap.Value() >= maxCap["cpu"] {
                        maxCap["cpu"] = cpuCap.Value()
                }

                // retrieve mem. usage from prometheus
                // Reference for the query: https://github.com/prometheus/node_exporter/issues/877
                memUsgQuery := promBaseUrl+"/api/v1/query?query=((node_memory_MemTotal_bytes{job='node-exporter',instance='"+node.Name+"'}-node_memory_MemFree_bytes{job='node-exporter',instance='"+node.Name+"'}-node_memory_Buffers_bytes{job='node-exporter',instance='"+node.Name+"'}-node_memory_Cached_bytes{job='node-exporter',instance='"+node.Name+"'})/node_memory_MemTotal_bytes{job='node-exporter',instance='"+node.Name+"'})"
                memUsgVal := getValFromQuery(memUsgQuery)

                memCap, _ := node.Status.Allocatable[v1.ResourceMemory]

                nodeCapacities["mem"][node.Name] = memCap.Value()
                if memCap.Value() >= maxCap["mem"] {
                        maxCap["mem"] = memCap.Value()
                }

                // Reference for the query: https://stackoverflow.com/questions/57357532/get-total-and-free-disk-space-using-prometheus
                stUsgQuery := promBaseUrl+"/api/v1/query?query=1-(((node_filesystem_avail_bytes{mountpoint='/',fstype!='rootfs',instance='"+node.Name+"',job='node-exporter'})/(node_filesystem_size_bytes{mountpoint='/',fstype!='rootfs',instance='"+node.Name+"',job='node-exporter'})))"
                stUsgVal := getValFromQuery(stUsgQuery)

                stCap, _ := node.Status.Allocatable[v1.ResourceEphemeralStorage]

                nodeCapacities["st"][node.Name] = stCap.Value()
                if stCap.Value() >= maxCap["st"] {
                        maxCap["st"] = stCap.Value()
                }
                
                nodeFeatMap[node.Name] = map[string]float64{"cpu": cpuUsgVal, "mem": memUsgVal, "disk": stUsgVal}
                

	}
        l.Println("Node Capacities: ", nodeCapacities)
        l.Println("Max capacities: ", maxCap)

        podRequestCPU := calculatePodResourceRequest(pod, v1.ResourceCPU)
        l.Println("Pod CPU resource request", podRequestCPU)
        // podRequestCPU is in millicores
        // maxCap["cpu"] is in entire cores
        podReqCPUScaled := float64(podRequestCPU) / float64(maxCap["cpu"] * 1000)
        l.Println("Scaled Pod CPU resource request", podReqCPUScaled)

        podRequestMem := calculatePodResourceRequest(pod, v1.ResourceMemory)
        l.Println("Pod Mem resource request", podRequestMem)
        // podRequestMem comes in bytes
        // maxCap["mem"] is in bytes? To be checked!
        podRequestMemScaled := float64(podRequestMem) / float64(maxCap["mem"])
        l.Println("Scaled Pod Mem resource request", podRequestMemScaled)

        podRequestSt := calculatePodResourceRequest(pod, v1.ResourceEphemeralStorage)
        l.Println("Pod eph. storage resource request", podRequestSt)
        // podRequestSt comes in ?
        // maxCap["st"] is in ? 
        podRequestStScaled := float64(podRequestSt) / float64(maxCap["st"])
        l.Println("Scaled Pod eph. storage resource request", podRequestStScaled)


        podFeatMap := map[string]float64{
                "Cpu_req":    float64(podReqCPUScaled),
                "Memory_Req": float64(podRequestMemScaled),
                "Disk_Req":   float64(podRequestStScaled),
        }

        // The following map denotes the Pod and node Features 
        map0 := make(map[string]interface{})

        map0["nodeFeatures"] = nodeFeatMap
        map0["PodFeatures"] = podFeatMap

        l.Println(map0)

        preScoreState := getQValues(map0)
        state.Write(preScoreStateKey, preScoreState)

        return nil
}

func (ps *brainerlscheduler) Score(ctx context.Context, state *framework.CycleState, pod *v1.Pod, nodeName string) (int64, *framework.Status) {
        _, err := ps.handle.SnapshotSharedLister().NodeInfos().Get(nodeName)
        if err != nil {
                return 0, framework.NewStatus(framework.Error, fmt.Sprintf("getting node %q from Snapshot: %v", nodeName, err))
        }
        result, _ := state.Read(preScoreStateKey)  // retrieves the memory address for the statekey
        c, _ := result.(*PreScoreState)
        qValue := c.category["q_values"][nodeName]
        l.Println("Orignal Q-value/score of the Node", nodeName,"--->", qValue)  // writing to container log score of given Node
        var i64 int64
	    i64  = int64(qValue *5)
	    l.Println("Node score after casting to int:",i64)
        return i64, nil
}

func (ps *brainerlscheduler) ScoreExtensions() framework.ScoreExtensions {
        return ps
}

func (ps *brainerlscheduler) NormalizeScore(ctx context.Context, state *framework.CycleState, pod *v1.Pod, scores framework.NodeScoreList) *framework.Status {

        return helper.DefaultNormalizeScore(framework.MaxNodeScore, false, scores)
}
func New(_ runtime.Object, h framework.FrameworkHandle) (framework.Plugin, error) {
        return &brainerlscheduler{handle: h}, nil
}
func getQValues(map0 map[string]interface{}) *PreScoreState {

        result := &PreScoreState{}

        jsonData, _ := json.Marshal(map0)
        // httpposturl := "http://10.244.0.74:5000/" // needs to be mapped to the CLUSTER IP 0.0.0.0:5000
        httpposturl := "http://sched-inference-svc.default.svc.cluster.local:5000/"  // name of the service running on the server
        httpgeturl := "http://sched-inference-svc.default.svc.cluster.local:5000/ping"  // returns status : Healthy
        //fmt.Println("HTTP JSON POST URL:", httpposturl)
        resp, err := http.Get(httpgeturl)
        if err != nil {
                log.Fatalln(err)
        }
        body, err := ioutil.ReadAll(resp.Body)
        if err != nil {
                log.Fatalln(err)
        }
        //Convert the body to type string
        sb := string(body)
        l.Println("Output of the ping GET Request", sb)  // writing the Output of get request to container logs

        request, error := http.NewRequest("POST", httpposturl, bytes.NewBuffer(jsonData))
        request.Header.Set("Content-Type", "application/json; charset=UTF-8")
        client := &http.Client{}
        response, error := client.Do(request)

        if error != nil {
                panic(error)
        }
        defer response.Body.Close()

        data, _ := ioutil.ReadAll(response.Body)
        xdata := string(data)
        jsonMap := make(map[string]map[string]float32)
        err1 := json.Unmarshal([]byte(xdata), &jsonMap)

        if err1 != nil {
                panic(err1)
        }

        result.category = jsonMap
        return result  // q_values for all the nodes in cluster

}

func getValFromQuery(q string) float64 {

        resp, err := http.Get(q)

        if err != nil {
                log.Fatalln(err)
        }

        body, err := ioutil.ReadAll(resp.Body)
        if err != nil {
                log.Fatalln(err)
        }

        //Convert the body to type string
        sb := string(body)

        // the response has the following format: {"status":"success","data":{"resultType":"vector","result":[{"metric":{},"value":[1632756246.745,"0.0403181818181606"]}]}}
        // we need to parse it to get the value of the metric
	obj := map[string]interface{}{}
	
	if err := json.Unmarshal([]byte(sb), &obj); err != nil {
		log.Fatal(err)
	}
	
	valueArr, _ := find(obj, "value")
	
	arr := valueArr.([]interface{})
	iv := arr[1] // interface value
	sv := iv.(string) // string value
	usgVal, _ := strconv.ParseFloat(sv, 64)   

	return usgVal
}

// Source: https://github.com/kubernetes/kubernetes/blob/master/pkg/scheduler/framework/plugins/noderesources/resource_allocation.go
// calculatePodResourceRequest returns the total non-zero requests. If Overhead is defined for the pod and the
// PodOverhead feature is enabled, the Overhead is added to the result.
// podResourceRequest = max(sum(podSpec.Containers), podSpec.InitContainers) + overHead
func calculatePodResourceRequest(pod *v1.Pod, resource v1.ResourceName) int64 {
	var podRequest int64
	for i := range pod.Spec.Containers {
		container := &pod.Spec.Containers[i]
		value := schedutil.GetNonzeroRequestForResource(resource, &container.Resources.Requests)
		podRequest += value
	}

	for i := range pod.Spec.InitContainers {
		initContainer := &pod.Spec.InitContainers[i]
		value := schedutil.GetNonzeroRequestForResource(resource, &initContainer.Resources.Requests)
		if podRequest < value {
			podRequest = value
		}
	}

	return podRequest
}

// Source: https://stackoverflow.com/a/45757396
// function for searching the value associated with a key from the map recursively
// example map: {"status":"success","data":{"resultType":"vector","result":[{"metric":{},"value":[1632756246.745,"0.0403181818181606"]}]}}
func find(obj interface{}, key string) (interface{}, bool) {
	mobj, ok := obj.(map[string]interface{})
	if !ok {
		return nil, false
	}
	
	for k, v := range mobj {		
		if k == key {
			return v, true
		} 
		if m, ok := v.(map[string]interface{}); ok {
			if res, ok := find(m, key); ok {
				return res, true
			}
		} 
		if va, ok := v.([]interface{}); ok {
			for _, a := range va {
				if res, ok := find(a, key); ok {
					return res,true
				}
			}
		}
	}
	
	return nil,false
}