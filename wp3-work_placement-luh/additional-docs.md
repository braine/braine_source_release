## Setting-up Promtheus and node-exporter as pods
### Setting up Prometheus
- Clone the following repository:
```
git clone https://github.com/bibinwilson/kubernetes-prometheus
cd kubernetes-prometheus
```
- Add the following lines to `config-map.yaml` in the section `scrape_configs:`-> `- job_name: 'node-exporter'`->`relabel_configs:`. This will allow to use the node name (on which the node-exporter pod is running) instead of the pod IP address. 
```

        # map node name to instance label <-- THIS
        - source_labels: [__meta_kubernetes_pod_node_name]
          action: replace
          target_label: instance
```
- In `prometheus-service.yaml`, replace the lines:

```
    type: NodePort  
    - port: 8080
      targetPort: 9090 
      nodePort: 30000
```
with
```
  type: ClusterIP
  ports:
    - targetPort: 9090
      port: 30000
```
This will allow us to create a `ClusterIP` service, instead of a `NodePort` service. The updated yaml file will look like this:

```
apiVersion: v1
kind: Service
metadata:
  name: prometheus-service
  namespace: monitoring
  annotations:
      prometheus.io/scrape: 'true'
      prometheus.io/port:   '9090'

spec:
  selector:
    app: prometheus-server
  type: ClusterIP
  ports:
    - targetPort: 9090
      port: 30000
```

- Run the following commands to create the required Kubernetes resources:
```
kubectl create namespace monitoring
kubectl create -f clusterRole.yaml
kubectl create -f config-map.yaml
kubectl create -f prometheus-deployment.yaml 
kubectl create -f prometheus-service.yaml --namespace=monitoring
```
### Setting up node-exporter
- Run the following commands to create the node-exporter resources in your K8s cluster:
```
git clone https://github.com/bibinwilson/kubernetes-node-exporter
cd kubernetes-node-exporter
kubectl create -f daemonset.yaml
kubectl create -f service.yaml
```
### Clean-up 

If you no longer need this set-up, you can delete the whole namespace: 

```
kubectl delete ns monitoring --force
kubectl delete clusterrole prometheus
kubectl delete clusterrolebinding prometheus
```

### Credit
- https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/
- https://devopscube.com/node-exporter-kubernetes/
