<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->




<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="BRAINE">
    <img src="images/braine_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">BRAINE Scheduler</h3>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-braine-project">About The BRAINE Project</a>
    </li>
        <li>
      <a href="#about-the-scheduler">About The Scheduler</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#clean-up">Clean-up</a></li>
    <li><a href="#known-issues">Known issues</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE BRAINE PROJECT -->
## About The BRAINE Project

Thanks to its multiple technical advantages, edge computing is capable of considerably boosting services and applications. It can do this by supporting AI natively instead of relying on the cloud. The EU-funded BRAINE project aims to further the development of edge computing by focusing on energy-efficient hardware and AI software that can process Big Data at the edge and support security, data privacy and sovereignty. To achieve its goal, it will build a seamless edge microdata centre interlinked with AI-enabled network interface cards. Through its work, the project will advance Europe’s position in intelligent edge computing and promote growth in sectors such as smart manufacturing, the Internet of Things, smart cities and smart healthcare.

## About The Scheduler
The scheduler is developed as part of  T3.3 of WP3. 
It is responsible for finding the best node to host a pod. 
To do so, it adopts a deep reinforcement learning (DRL) approach to calculate scores for the worker nodes. It can use multiple reward functions, depending on the objective to be optimized.  
The diagram below shows the different modules that comprise the scheduler:

![alt text](images/Scheduler-Component-Diagram.png "Scheduler component diagram")

- **SchedulerTrainer.** Will be deployed as a pod and it is in charge of training the neural network that will be used during inference. Currently, the neural network is trained based on interactions with a simulated EMDC environment. However, later on, training can be performed based on real cluster data collected by the ExperienceCollector in order to continuously adapt to the real workload patterns. When the training is complete, the weights of the neural network will be persisted in a volume called DRLModelWeights.  

- **SchedulerInference.** This pod consists in a RESTful API that exposes an endpoint to return the node scores. More specifically, this endpoint is invoked with an input corresponding to the RL state, which is then used to perform a forward pass through the trained neural network. 

- **K8sScheduler.** This pod adds a custom RL scoring plugin to the default K8s scheduler. The RL scoring plugin implements the interfaces of the PreScore and Score extension points. In particular, the PreScore function queries the SchedulerInference pod to retrieve the node scores based on the current RL state. The RL state is formed by pod features and nodes’ features. The required pod features are its resource requests which can be retrieved from the p parameter of the PreScore function. As for the nodes’ features, they correspond to their current resource utilization levels, which are retrieved from the TelemetryDB. Then, the obtained node scores are written into the state variable. Since the Score function also has access to this information (as demonstrated by the presence of state in its inputs), it can retrieve the score corresponding to the specific node that is currently being evaluated.   

<!-- GETTING STARTED -->
## Getting Started


### Prerequisites

- Docker. You may find instructions on how to use Docker commands as a non-root user [here](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user).

- You need to have a Kubernetes cluster set up. 

- In order to allow the scheduler trainer and inference pods to be schedulable on the master/control plane node, you need to run the following command:
```
kubectl taint node MASTER_NODE_NAME node-role.kubernetes.io/master:NoSchedule-
``` 
- Prometheus and node-exporter. They are used to retrieve node resource usage metrics. Make sure that the Prometheus endpoint can be reached from within a pod (e.g. `kube-scheduler` ). Right now, the following base URL is configured in `manifests/common-res.yaml`:
```
http://prometheus-service.monitoring.svc.cluster.local:30000
``` 
where `prometheus-service` is a `clusterIP ` service in the `monitoring` namespace. If you have a different URL, you can replace it in `manifests/common-res.yaml` in the `data` section of the config map definition. Additional documentation regarding this prerequisite can be found [here](additional-docs.md).
### Installation
In order to have a working version of the different components shown above, please run the following steps in order:
1. Clone this repository and `cd` into it:

```
git clone --recurse-submodules https://gitlab.com/braine/wp3-work_placement-luh.git
cd wp3-work_placement-luh
```
Note the use of `--recurse-submodules`, since this repo contains a submodule called `scheduler-plugins`, which is maintained by Kubernetes SIG scheduling to host out-of-tree scheduler plugins. The original repo can be found [here](https://github.com/kubernetes-sigs/scheduler-plugins). 


2. The `scripts` folder contains a script to automate the different steps needed to build the different components shown in the diagram above. In particular, it will:
 
 - Build a Docker image to be used by the scheduler trainer pod and deploy it on your K8s cluster. If this step is successful, the neural network weights file `DDQN_model_weights.pt` will be available under `/mnt/disk/vol1`.
 - Build a Docker image to be used by the scheduler inference pod and deploy it on your K8s cluster.
 - Build a custom version of the `kube-scheduler` pod. Most of the included commands are based on instructions provided in the original `scheduler-plugins` repo. They can be found [here](https://github.com/kubernetes-sigs/scheduler-plugins/tree/master/doc).
 
 The script can be run as follows:

```
chmod +x ./scripts/build_custom_sched.sh && sudo ./scripts/build_custom_sched.sh -t -i 
```

The following options can be used with it:
- `-t` : To build the scheduler trainer Docker image. This option can be omitted if the image is already present and no changes have been made to it.
- `-i` : To build the scheduler inference Docker image. This option can be omitted if the image is already present and no changes have been made to it.
- `-n` : To skip the training process.

**Note**: You can modify training parameters by changing their values in `common/config.py`.
 
- Here is an example output after running this script:
```
Creating directory: customplugin
Copying custom plugin code into scheduler code
Registering customplugin
Checking scheduler Docker image
Image does not exist
Building image
.
.
.
Successfully tagged localhost:5000/scheduler-plugins/kube-scheduler:latest
kube-scheduler yaml already backed-up!
Creating custom kube-scheduler configuration
Modifying kube-scheduler manifest based on new configuration
Waiting for scheduler pod to be ready...
Waited 3/180 s.
.
.
.
Waiting for scheduler pod to be ready...
Waited 90/180 s.
Waiting for scheduler pod to be ready...
Waited 93/180 s.
kube-scheduler is Running using the custom image: localhost:5000/scheduler-plugins/kube-scheduler:latest
```
As it can be seen, up to 10 minutes may be needed to get the different components running. This mostly due to the process of building the Docker images.
<!-- USAGE EXAMPLES -->
## Usage

In order to check that the scheduler has been invoked, you can deploy a workload, e.g. 

```
kubectl create deployment hello-node --image=k8s.gcr.io/echoserver:1.4
``` 

Then, check the logs saved in the scheduler container by executing the following command:

```
kubectl exec $(kubectl get pods -l component=kube-scheduler -n kube-system --no-headers -o custom-columns=":metadata.name") -n kube-system -- cat /usr/local/my.log
```

## Clean-up

The `scripts` directory contains a script that automates all the steps needed to clean-up the changes caused by enabling the custom scheduler in the previous step. It will revert the cluster to its state before deploying the custom scheduler.
```
chmod +x ./scripts/clean.sh && sudo ./scripts/clean.sh
```

You can use the following options with the clean-up script:
- `-t`: to remove the scheduler trainer Docker image.
- `-i`: to remove the scheduler inference Docker image. 
- `-s`: to remove the custom `kube-scheduler` Docker image. 

- Example output:
```
Deleting resources created for custom scheduler
Resources not installed!
Resources not installed!
Recovering default kube-scheduler
Waiting for scheduler pod to be ready...
Waited 3/180 s.
Waiting for scheduler pod to be ready...
Waited 6/180 s.
.
.
.
Waiting for scheduler pod to be ready...
Waited 78/180 s.
kube-scheduler is running using the image: k8s.gcr.io/kube-scheduler:v1.20.10
Removing custom Docker images
Deleted Images:
deleted: sha256:515b54025a9c62820d9819fed8fb90af17b3ef74974b723e2f28d121c29c2500
deleted: sha256:6939a3eb3c6eca317f2bc13245dd0ee96c07dc536453ebb3e4261c20a0990c45
deleted: sha256:e11a084b94db8aef079381590d74f877ebafa24bf67561a81a4134b8992df441
deleted: sha256:0e1bd3a2bd20f0f730485ec9c987cfc27cc5d5ad1ef49d6e196b502fc549bb31
deleted: sha256:a66e5cd1c13f9fcc76442248d2c1d618c3e9bab6b104c764960f635fd2e784b8
deleted: sha256:d2a6ca1a6992081f3e551dcd97922b9fe7a68077f89fd253b0c20b03fcaca4bf
deleted: sha256:571f757753a508458180b27728a14750f22b00ea40e996c699e2fcba5acee29e
deleted: sha256:639803eaeff1d96be4cebfab374539e9105138ed0270f2faa7fff46d9234952a
deleted: sha256:711dade3a540d25198f0ef497c487e85a155132bd3044bf39cef0d9b42340de5

Total reclaimed space: 1.652GB
Untagged: localhost:5000/scheduler-plugins/kube-scheduler:latest
Deleted: sha256:93cb8e3d04ad73241eb0e09cd485514d6bc7574f207fb71baee9eaa62759b6e6
Deleted: sha256:bd653ccffe08ebafac2f5e4d8fcc33d4872e43b32ef0923faf24de8b469f6c3b
Deleted: sha256:7c1e8a36d9ab4ac55c438197a9d0d3a18bbf4fa21af90597d84d20325a97c35d
```

As it can be seen, a wait time of up to 2 minutes is needed for the scheduler pod to be back in the `Ready` state.

<!-- KNOWN ISSUES -->
## Known issues

<!-- LICENSE -->
## License

?



<!-- CONTACT -->
## Contact

- Zeineb Rejiba - rejiba@l3s.de
- John Rothman - rothman@l3s.de
- Pawan Kumar - pawan.kumar1181994@gmail.com 







<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
