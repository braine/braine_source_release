import gym, torch, json
import os.path
from collections import defaultdict
from flask import Flask, request
from common.helper_func import *
from common.drl_algs.dqn_models import DQN
from common.config import GYM_ENV_CFG, GLOBAL_CFG, DRL_CFG

def swap_nested_dict(input_dict):
    d = defaultdict(dict)
    for key, value in input_dict.items(): 
        for k, v in value.items(): 
            d[k][key] = v
    return d 

def make_forward_pass(pod_features, node_features):

    test_data, test_task_dur_list, attr_idx, state_indices = pre_process_data(mode='test')

    env_test = gym.make('gym_emdc:emdc-v0',
                        task_list=test_data,
                        attr_idx=attr_idx,
                        task_dur_list=test_task_dur_list,
                        state_indices=state_indices,
                        testing=True,
                        gym_cfg=GYM_ENV_CFG,
                        global_cfg=GLOBAL_CFG,
                        drl_cfg=DRL_CFG)

    # we should remove this dependency on the gym env when instantiating the DDQN model. All we need is the model architecture and saved weights.
    ddqn = DQN(env=env_test,
               model_cfg=DRL_CFG,
               mask_env=True)
    save_path = "/data/"
    if os.path.isfile('%sDDQN_model_weights.pt' %save_path):
        print("NN weights file exists.")
        ddqn.current_model.load_state_dict(torch.load('%sDDQN_model_weights.pt' %save_path, map_location='cpu'))
    else:
        print("NN weights file does not exist. Will be using random weights.")

    # prepare state values and convert them into expected NN input format
    state_list = []
    for key, value in pod_features.items():
        state_list.append(value)
    
    #we get somthg like node_features: {"w1": {"cpu": 0.1, "mem": 0.2}, "w2": {"cpu": 2, "mem":0.4}}
    #ideally it would be {"cpu":{"w1": 0.1, "w2":2}, "mem": {"w1":0.2}, "w2":0.4}  
    d = swap_nested_dict(node_features)

    for key_n, value_n in d.items():
        for k_n, v_n in value_n.items():
            state_list.append(v_n)

    state = np.array(state_list)
    state_tensor = ddqn.Variable(torch.FloatTensor(state).unsqueeze(0))

    # Perform a forward pass using the state as input.
    q_values = ddqn.forward(state_tensor)
    qv_list = q_values.detach().numpy()[0].tolist()
    cnt = 0
    node_to_qvalue_map = {}
    for node_name, node_dict in node_features.items():
        node_to_qvalue_map[node_name] = qv_list[cnt]
        cnt += 1
    return node_to_qvalue_map

app = Flask(__name__)


@app.route('/', methods=['POST'])
def starting_url():
    json_data = request.get_json(force=True)
    node_features = {}
    pod_features = {}
    if json_data:
        if 'nodeFeatures' in json_data:
            node_features = json_data['nodeFeatures']
        if 'PodFeatures' in json_data:
            pod_features = json_data['PodFeatures']

    node_to_qvalue_map = make_forward_pass(pod_features, node_features)
    return json.dumps({"q_values": node_to_qvalue_map})

@app.route('/ping', methods=['GET'])
def ping():
    return {
        'status': 'healthy'
    }


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
