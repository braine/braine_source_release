#!/usr/bin/env python
# encoding: utf-8
from flask import Flask, jsonify
from make_query import query
import yaml
app = Flask(__name__)

with open("config.yml") as file: # Use file to refer to the file object
    cfg = yaml.safe_load(file)

@app.route('/')
def homepage():
    return 'Sanity check... The application is running.'
@app.route('/%s/%s/%s/%s/%s/%s' % (cfg['sys'], cfg['ele'], cfg['rsc'], cfg['agg'], cfg['atr'], cfg['wndw_t1']),
           defaults={'t2_offset': None})
@app.route('/%s/%s/%s/%s/%s/%s' % (cfg['sys'], cfg['ele'], cfg['rsc'], cfg['agg'], cfg['atr'], cfg['wndw_t1t2']))
def return_values(sys, sys_typ, ele, ele_typ,  rsc, rsc_typ, agg, atr, t1_offset, t2_offset):
    error_str, error_code = check_inputs(sys_typ, ele_typ,  rsc_typ, agg, atr, t1_offset, t2_offset, cfg)
    if error_code != 200:
        return error_str, error_code

    result = query(sys, sys_typ, ele, ele_typ,  rsc, rsc_typ, agg, atr, t1_offset, t2_offset)
    return jsonify(result)


def check_inputs(sys_typ, ele_typ,  rsc_typ, agg, atr, t1_offset, t2_offset, cfg):
    if t2_offset is None:
        t2_offset = 0
    elif t2_offset[-1] is not t1_offset[-1]:

        # look into way to return a http error message, errorcode
        # install posttmann postmann is a nice client for debugging apis
        return "Bad request. For simplicity, time offset 1 and 2 have to be the same unit. " \
               "e.g. 10s,1s or 10m,1m is acceptable, but 10m, 10s is not allowed", 400
    elif int(t2_offset[:-1]) < int(t1_offset[:-1]):
        return "Time offset 2 has to be larger in magnitude (more negative) than time offset 1", 400
    elif int(t1_offset[:-1]) > 0 or int(t2_offset[:-1]) > 0:
        return "Time offset 2 has to be larger in magnitude (more negative) than time offset 1", 400
    full_url = '%s:%s' % (cfg['url'], cfg['web_port'])
    error_str = 'Make sure your %s is spelled correctly, and is one of the elements from the following list: ['
    about_help = '<br/><br/>For examples see %s/help' % full_url

    if sys_typ not in cfg['systemTypes']:
        return error_str % 'systems' + ', '.join(cfg['systemTypes']) + ']' + about_help, 400
    if ele_typ not in cfg['elementTypes']:
        return error_str % 'elements' + ', '.join(cfg['elementTypes']) + ']' + about_help, 400
    if rsc_typ not in cfg['resourceTypes']:
        return error_str % 'resources' + ', '.join(cfg['resourceTypes']) + ']' + about_help, 400
    if atr not in cfg['attributes']:
        return error_str % 'attributes' + ', '.join(cfg['attributes']) + ']' + about_help, 400
    if agg not in cfg['aggregates']:
        return error_str % 'aggregates' + ', '.join(cfg['aggregates']) + ']' + about_help, 400

    return 'OK', 200

@app.route('/help')
def about():
    help_html = open("help.html").read().format(url=cfg['url'],
                                                port=cfg['web_port'],
                                                systemType=cfg['systemTypes'],
                                                elementTypes=cfg['elementTypes'],
                                                resourceTypes=cfg['resourceTypes'],
                                                atr=cfg['attributes'],
                                                agg=cfg['aggregates'],
                                                access_url='%s:%s' % (cfg['url'], cfg['web_port']),
                                                )
    return help_html
app.run(debug=True,host='0.0.0.0')