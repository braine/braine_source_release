# from influxdb import InfluxDBClient

from influxdb_client import InfluxDBClient
import yaml
import json
from influxdb_client.client.flux_table import FluxStructureEncoder
with open("queries.influxdb.flux.yml") as file: # Use file to refer to the file object
    query_cfg = yaml.safe_load(file)
with open("config.yml") as file: # Use file to refer to the file object
    cfg = yaml.safe_load(file)
#
#resource, attribute, aggregation, t1_ofn, t2_ofn
# sys, ele,  rsc, agg, atr, t1_offset, t2_offset
def query(sys, sys_typ, ele, ele_typ,  rsc, rsc_typ, agg, atr, t1_offset, t2_offset):
    unit = process_unit(rsc_typ, atr)
    client = InfluxDBClient(url='%s:%s' % (cfg['url'], cfg['db_port']),
                            token="my-token")

    query_api = client.query_api()

    query = query_cfg[rsc_typ][atr]
    query = change_placeholders(query, t1_offset, t2_offset, agg, rsc)

    query_result = query_api.query(query)
    json_format = json.dumps(query_result, cls=FluxStructureEncoder, indent=1)
    json_format = json.loads(json_format)
    output_json = {}
    message_counter = -1
    for result in json_format:
        del result['columns']


        for idx in range(len(result['records'])):

            message_counter += 1
            output_json[message_counter] = {}


            if "resource" in result['records'][idx]['values']:
                resource_val = result['records'][idx]['values']['resource']
            else:
                resource_val = '*'

            timestamp = process_timestamp(result['records'][idx]['values'])
            output_json[message_counter]['message'] = {'systemType': sys_typ,
                                                       'elementType': ele_typ,
                                                       'resourceType': rsc_typ,
                                                       'system': sys,
                                                       'element': ele,
                                                       'resource': resource_val,
                                                       'attribute': atr,
                                                       'aggregation': agg,
                                                       't1_ofn': t1_offset,
                                                       't2_ofn': t2_offset,
                                                       'value': result['records'][idx]['values']['_value'],
                                                       'time': timestamp,
                                                       'unit': unit}
    return output_json

# replaces placeholders inside the query
def change_placeholders(query, t1_offset, t2_offset, agg, rsc):
    query = query.replace('t1_placeholder', t1_offset)

    if agg == 'raw':
        query = query.replace('aggregate_placeholder', '')
    else:
        query = query.replace('aggregate_placeholder', '|> ' + agg + '()')

    if t2_offset == None:
        query = query.replace('t2_placeholder', 'now()')
    else:
        query = query.replace('t2_placeholder', t2_offset)
    query = query.replace('bucket_placeholder', cfg['bucket_name'])
    if rsc == '*':
        # regular expression used to capture all resources
        query = query.replace('resource_placeholder', "=~ /.*/")
    else:
        query = query.replace('resource_placeholder', "== \"" + rsc + "\"")
    return query

# Hard coded units for now.  Perhaps should be taken from a configuration file?  maybe a YAML
def process_unit(rsc_typ,atr):
    if rsc_typ == 'cpu':
        if atr == 'used':
            # if needed, later we can change this so it's not a percentage
            return 'percentage'
    elif rsc_typ == 'memory':
        if atr == 'used':
            return 'byte'
    elif rsc_typ == 'disk':
        if atr == 'write':
            return 'successful write'
        elif atr == 'read':
            return 'successful read'
    elif rsc_typ == 'nic':
        if atr == 'transmit' or 'recieve':
            return 'byte'

    return '1'
def process_timestamp(r):
    # I could also do a check for something
    # like  if agg == 'raw': return r["_time"] else return r["_stop"]
    if "_time" in r:
        return r["_time"]
    else:
        return r["_stop"]