import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output


def debug_plot_results(env, title, action):
    clear_output(True)
    plt.figure(figsize=(15, 8))
    grid = plt.GridSpec(1, 10, wspace=0.4, hspace=0.3)

    plt.subplot(grid[0,:1])
    plt.title('Next Task\n Action: %i' % action)


    print()
    y_pos = np.arange(3)
    plt.bar(y_pos, height=env.state[:3])
    plt.ylim(0, 1)
    bars = ('C', 'M', 'D')

    # Create names on the x-axis
    plt.title(title)
    plt.xticks(y_pos, bars)

    plt.subplot(grid[0, 1:])
    plt.title('Machine Usages Per Dimention, \n current timestamp:%f' % (env.elapsed_time))
    cap_dict = {}
    for m in env.machine_status:
        # More efficent
        cap_dict['C%i' % m] = 0
        cap_dict['M%i' % m] = 0
        cap_dict['D%i' % m] = 0
        for item in env.machine_status[m]:
            cap_dict['C%i' % m] += item['cpu']
            cap_dict['M%i' % m] += item['mem']
            cap_dict['D%i' % m] += item['disk']
    # Should be defined outside the function
    colors = ['black', 'black', 'black',
              'red', 'red', 'red',
              'green', 'green', 'green',
              'blue', 'blue', 'blue',
              'cyan', 'cyan', 'cyan',
              'greenyellow', 'greenyellow', 'greenyellow',
              'orchid', 'orchid', 'orchid',
              'royalblue', 'royalblue', 'royalblue']
    names = list(cap_dict.keys())
    values = list(cap_dict.values())
    plt.bar(range(len(cap_dict)), values, tick_label=names, color=colors)
    plt.ylim(0, 1)
    # can move outside the loop
    reference = [.25, .50, .75, 1]
    for elm in reference:
        plt.axhline(y=elm, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')

    plt.show()
    return


def plot_results(env,
                 results_dict,
                 ):
    clear_output(True)
    plt.figure(figsize=(30, 15))
    grid = plt.GridSpec(3, 10, wspace=0.4, hspace=0.3)

    epi_ends = np.cumsum(results_dict['numjobs']['Trained'])
    curr_epi_len = results_dict['numjobs']['Trained'][-1]
    current_step = epi_ends[-1]
    markers = ['*', 8, 9, 10, 11, '$R$', '$K$']
    losses = results_dict['losses']

    ########################Code to plot loss##############################################
    #
    # plt.subplot(grid[0, :5])
    # plt.title('Loss')
    # plt.plot(losses[:current_step])
    #
    # for epi_end in epi_ends:
    #     plt.axvline(x=epi_end, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')
    ########################Code to plot Episode loss##############################################


    plt.subplot(grid[0, 5:])
    plt.title('Loss  (only last epi)')
    plt.plot(losses[current_step - curr_epi_len:current_step])
    # plt.ylim(plot_lb, plot_ub)



    # ##########################Code to plot average of greedy actions per episode###################################
    # greedy_action_avg = results_dict['gaa']
    # plt.subplot(grid[0, 1])
    # plt.title('Episode Percentage of Greedy Actions')
    # plt.scatter(np.arange(len(greedy_action_avg)), greedy_action_avg)



    ###########################Code to plot available actions per step#####################################
    # pa = results_dict['possible_actions']
    # plt.subplot(grid[1, :5])
    # plt.title('Possible Action Per Step')
    # plt.plot(pa[:current_step])
    #
    # for epi_end in epi_ends:
    #     plt.axvline(x=epi_end, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')
    # # for epi_end in results_dict['numjobs']['TP']:
    # #     plt.axvline(x=epi_end, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')

    ###########################Wait time per step in just last episode#####################################
    was = results_dict['wait_at_step']
    plt.subplot(grid[0, :5])
    plt.title('Wait steps at action (only last epi)')
    # plt.scatter(np.arange(curr_epi_len), was[current_step - curr_epi_len:current_step], marker='.')
    plt.scatter(np.arange(curr_epi_len), was[current_step - curr_epi_len:current_step], marker='.')

    ###########################Hist of Available actions per step in just last episode#####################################
    # pa = results_dict['possible_actions']
    # plt.subplot(grid[1, 3:6])
    # plt.title('Possible Action Per Step hist (only last epi)')
    # # plt.scatter(np.arange(curr_epi_len), pa[current_step - curr_epi_len:current_step], marker='.')
    # plt.hist(pa[current_step - curr_epi_len:current_step],bins=[1, 2,3,4,5,6,7,8,9])
    #
    # ###########################Available actions per step in just last episode#####################################
    # pa = results_dict['possible_actions']
    # plt.subplot(grid[1, 6:])
    # plt.title('Possible Action Per Step (only last epi)')
    # plt.scatter(np.arange(curr_epi_len), pa[current_step - curr_epi_len:current_step], marker='.')
    # # plt.hist(pa[current_step - curr_epi_len:current_step],bins=8)

    ###########################Code to plot Jobs Per Episode#####################################
    # plt.subplot(grid[2, :])
    # jobs_d = results_dict['numjobs']
    # plt.title('Episode Number of Jobs Scheduled')
    #
    # for test_key, mark in zip(jobs_d, markers):
    #     vals = jobs_d[test_key]
    #     plt.scatter(np.arange(len(vals)), vals, label=test_key, marker=mark)
    #
    # for idx, episode in enumerate(jobs_d['Trained']):
    #     if idx % 5 == 0:
    #         plt.axvline(x=idx, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')
    #
    # plt.legend()
    ##############################Code to plot wait time per job ##################################
    plt.subplot(grid[1:3, :])
    wait_d = results_dict['wait']
    jobs_d = results_dict['numjobs']
    plt.title('Episode Average Wait Time per Job')
    for test_key, mark in zip(wait_d, markers):
        num_jobs = np.array(jobs_d[test_key])
        wait_times = np.array(wait_d[test_key])
        wait_per_job = wait_times / num_jobs
        if mark == '*':
            print('good')
            s = 12
        else:
            s = 8
        # plt.scatter(np.arange(len(wait_per_job)), wait_per_job, label=test_key, marker=mark, s=s)
        plt.plot(wait_per_job, label=test_key, marker=mark, markersize=s)
    plt.legend()

    for idx, episode in enumerate(wait_d['Trained']):
        if idx % 5 == 0:
            plt.axvline(x=idx, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')

    ######################Code to plot Episode reward#####################################
    # plt.subplot(grid[3, :])
    # reward_d = results_dict['reward']
    # plt.title('Episode Reward')
    # for test_key, mark in zip(reward_d, markers):
    #     vals = reward_d[test_key]
    #     plt.scatter(np.arange(len(vals)), vals, label=test_key, marker=mark)
    # plt.legend()
    ######################Code to plot Episode runtime#####################################
    # plt.subplot(grid[3, :])
    # run_d = results_dict['runtime']
    # plt.title('Episode runtime Time')
    # for test_key, mark in zip(run_d, markers):
    #     vals = run_d[test_key]
    #     plt.scatter(np.arange(len(vals)), vals, label=test_key, marker=mark)
    # plt.legend()
    ####################Code to plot machine usages########################################

    # plt.subplot(grid[3, :])
    # plt.title('Current Timestep Machine Usages Per Dimention')
    # cap_dict = {}
    # for m in env.machine_status:
    #     # More efficent
    #     cap_dict['C%i' % m] = 0
    #     cap_dict['M%i' % m] = 0
    #     cap_dict['D%i' % m] = 0
    #     for item in env.machine_status[m]:
    #         cap_dict['C%i' % m] += item['cpu']
    #         cap_dict['M%i' % m] += item['mem']
    #         cap_dict['D%i' % m] += item['disk']
    # # Should be defined outside the function
    # colors = ['black', 'black', 'black',
    #           'red', 'red', 'red',
    #           'green', 'green', 'green',
    #           'blue', 'blue', 'blue',
    #           'cyan', 'cyan', 'cyan',
    #           'greenyellow', 'greenyellow', 'greenyellow',
    #           'orchid', 'orchid', 'orchid',
    #           'royalblue', 'royalblue', 'royalblue']
    # names = list(cap_dict.keys())
    # values = list(cap_dict.values())
    # plt.bar(range(len(cap_dict)), values, tick_label=names, color=colors)
    #
    # # can move outside the loop
    # reference = [.25, .50, .75, 1]
    # for elm in reference:
    #     plt.axhline(y=elm, linewidth=0.3, color=(0, 0, 0, 0.75), linestyle='--')
    ######################################################################################


    plt.show()

def plot_clustered_stacked(dfall,
                           colors,
                           labels=None,
                           title="multiple stacked bar plot",
                           H="..", **kwargs):
    # NEED TO SOURCE WHERE I GOT THE BASE CODE FROM!!!!!!!!11

    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot.
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns)
    n_ind = len(dfall[0].index)
    axe = plt.subplot(111)

    for df in dfall:  # for each data frame
        axe = df.plot(kind="bar",
                      width=1,
                      stacked=True,
                      ax=axe,
                      legend=False,
                      grid=True,
                      color=colors,
                      **kwargs)  # make bar plots

    h, l = axe.get_legend_handles_labels()  # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col):  # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i + n_col]):
            for rect in pa.patches:  # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col))  # edited part
                rect.set_width(.95 / float(n_df + 1))

    # axe.get_legend().set_visible(False)
    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    # axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)-0.5) / 2.)
    axe.set_xticklabels(df.index, rotation=0)
    axe.set_title(title)

    # Add invisible data to add another legend
    n = []
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    # l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1])
    # axe.add_artist(l1)
    return axe