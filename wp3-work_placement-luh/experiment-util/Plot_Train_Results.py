from WP3R.helper_func import test_extractor, plotter
import pandas as pd

tests_info = pd.read_csv("test_results/test_logs.tsv",
                         sep="\t",
                         names=['filename', 'GYM_ENV_CFG', 'DRL_CFG', 'array_names'])

st_results, st_arry_names = test_extractor(tests_info,'12.npz')

plotter(frame_idx="Calc_later",
        training_rewards=st_results[st_arry_names[0]],
        losses=st_results[st_arry_names[1]],
        greedy_action_accuracy_avg=st_results[st_arry_names[2]],
        waiting_dict=st_results[st_arry_names[3]],
        rewards_dict=st_results[st_arry_names[4]]
        )