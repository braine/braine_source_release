import numpy as np
import pandas as pd
import seaborn as sns
from common.config import GYM_ENV_CFG
import matplotlib.pyplot as plt

np.random.seed(GYM_ENV_CFG['SEED'])

resource_list = []
data_dict = {'cpu_req': [],
             'mem_req': [],
             'disk_req': []}


def data_gen(sample_size, res_dist_dict, ratio=1):
    # give the probability of selecting [low, medium, high]

    for _ in range(sample_size):
        spiky_resource = np.random.choice(['cpu_req', 'mem_req', 'disk_req'])
        for resource in ['cpu_req', 'mem_req', 'disk_req']:
            if res_dist_dict[resource] == "t-uniform":
                probs = [0.5, 0.5, 0, 0, 0]
            if res_dist_dict[resource] == "s-uniform":
                probs = [1/3, 1/3, 1/3, 0, 0]
            if res_dist_dict[resource] == "uniform":
                probs = [0, 0, 1/3, 1/3, 1/3]
            elif res_dist_dict[resource] == "intensive":
                probs = [0, 0, 0.2, 0.2, 0.6]
            elif res_dist_dict[resource] == "medium":
                probs = [0, 0, 0.2, 0.6, 0.2]
            elif res_dist_dict[resource] == "light":
                probs = [0, 0, 0.6, 0.2, 0.2]

            elif "spike" in res_dist_dict[resource]:
                probs = [0, 0, 0, 0, 0]
                if res_dist_dict[resource] == 'tiny_spike':
                    high_idx = 1
                if res_dist_dict[resource] == 'small_spike':
                    high_idx = 2
                if res_dist_dict[resource] == 'medium_spike':
                    high_idx = 3
                if res_dist_dict[resource] == 'large_spike':
                    high_idx = 4
                if resource == spiky_resource:
                    probs[0] = 0.1
                    probs[high_idx] = 0.9
                else:
                    probs[0] = 0.9
                    probs[high_idx] = 0.1

            ee_low = np.random.uniform(low=0, high=0.1, size=1)
            e_low = np.random.uniform(low=0.1, high=0.2, size=1)
            lows = np.random.uniform(low=0, high=0.299, size=1)
            meds = np.random.uniform(low=0.3, high=0.599, size=1)
            highs = np.random.uniform(low=0.6, high=1, size=1)

            eelows = np.round(ee_low, 4)[0]
            elows = np.round(e_low, 4)[0]
            lows = np.round(lows, 4)[0]
            meds = np.round(meds, 4)[0]
            highs = np.round(highs, 4)[0]

            sample = np.random.choice([eelows, elows, lows, meds, highs], size=1, p=probs)[0]

            data_dict[resource].append(sample)

    df_sample = pd.DataFrame(data_dict)
    # Ratio should be between Range 0-1 specifies the Percentage of utilization Made as per request
    df_sample['cpu_rate'] = df_sample['cpu_req'] * ratio
    df_sample['can_mem_usg'] = df_sample['mem_req'] * ratio
    df_sample['local_disk_usg'] = df_sample['disk_req'] * ratio

    return df_sample


# Need to Pass the single col_list(e.g df['cpu_req']) as input and col name (e.g. 'cpu_req')
def gen_visualization(df, res_dist_dict):

    plt.figure(figsize=(15, 8))
    grid = plt.GridSpec(1, 14, wspace=0.4, hspace=0.3)
    start_set = {0: 0,
                 1: 4+1,
                 2: 4+1+4+1}
    sns.set(rc={'figure.figsize': (15, 8)})
    for idx,type in enumerate(['cpu_req', 'mem_req', 'disk_req']):
        plt.subplot(grid[0, start_set[idx]:start_set[idx]+4])
        plt.title('%s\n%s' % (type, res_dist_dict[type]))
        plt.xlabel('request amount')


        df_list = df[type]
        x = pd.DataFrame(df_list)
        conditions = [x[type] > 2 / 3,
                      (x[type] >= 1 / 3) & (x[type] <= 2 / 3),
                      x[type] < 1 / 3]
        choices = ["high", 'medium', 'low']
        x["load_class"] = np.select(conditions, choices)

        # sns.histplot(x, x=type, hue='load_class',ax = axs[idx])
        sns.histplot(x, x=type, hue='load_class')
    plt.show()


# res_dist_dict is a dictionary to specify the type of distribution each resource dimension will have
# Possible values are: 'uniform', 'intensive' and 'light'. Those values are set in the config file.
res_dist_dict = {}
for res in ['cpu_req', 'mem_req', 'disk_req']:
    res_dist_dict[res] = GYM_ENV_CFG[res]
df = data_gen(500_000, res_dist_dict, ratio=0.6)

filename = 'task_data_tuniform_500k_rate0.6'
# filename = 'test'
dir = '../SchedulerTrainer/input/'
compression_opts = dict(method='zip', archive_name=filename + '.csv')
df.to_csv(dir + filename + '.zip', index=False, compression=compression_opts)
# gen_visualization(df, res_dist_dict)

