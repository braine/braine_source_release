import pandas as pd
import matplotlib.pyplot as plt
from common.config import GLOBAL_CFG

GLOBAL_CFG['DATA_LOCATION'] = 'input/task_data_all_uniform_500k_rate0.6.zip'
task_data = pd.read_csv(GLOBAL_CFG['DATA_LOCATION'])
plt.figure(figsize=(30, 15))

grid = plt.GridSpec(5, 5, wspace=0.4, hspace=0.3)
counter = 0
for i in range(5):
    for j in range(5):
        plt.subplot(grid[i, j])
        row = task_data.iloc[counter,:3]
        plt.bar(['cpu','mem','disk'],row)
        counter+=1
plt.show()
