import gym
from common.drl_algs.dqn_models import DQN
from common.helper_func import *
import torch
from common.config import GYM_ENV_CFG, DRL_CFG

np.random.seed(GYM_ENV_CFG['SEED'])

test_data, test_task_dur_list, attr_idx, state_indices = pre_process_data(mode='test')

env_test = gym.make('emdc-v0',
                    task_list=test_data,
                    attr_idx=attr_idx,
                    task_dur_list=test_task_dur_list,
                    state_indices=state_indices,
                    testing=True,
                    gym_cfg=GYM_ENV_CFG,
                    global_cfg=GLOBAL_CFG,
                    drl_cfg=DRL_CFG)

ddqn = DQN(env=env_test,
           model_cfg=DRL_CFG,
           mask_env=True)

# If you want to load some weights, load it below!!!
ddqn.current_model.load_state_dict(torch.load('large-Spiky0.6_time1e4_shorterTaskDir.pt',map_location='cpu'))

epi_rewards, losses, all_rewards = np.empty(0), np.empty(0), np.empty(0)
pos_act_size = np.empty(0)
action_list, state_list, all_steps = np.empty(0), np.empty(0), np.empty(0)
epi_reward = 0
global_steps = 0

reward_per_alg = {'dqn': [], 'random': [], 'k8s-la': [], 'greedy': []}

results_dict = {'wait': {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'epi_len': {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'throughput': {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'runtime':  {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'reward':  {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'numjobs': {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []},
                'nb_unused': {'trained': [], 'random': [], 'k8s-la': [], 'g-util': []}}

nb_test_episodes = 5
for epi in range(nb_test_episodes):
    env_test.shuffle_dataset()
    print('Episode %i' % epi)
    # nv_unused = np.zeros()
    for alg in ['random', 'trained', 'k8s-la', 'g-util']:
        print('\tEvaluating: %s' % alg.upper())
        # initialize state
        state = env_test.reset()
        per_unused_list, reward_list = [], []
        mm_cnt, mmin_cnt, zm_cnt, epi_length = 0, 0, 0, 0
        while not env_test.done:
            epi_length += 1

            mask = env_test.get_valid_act_mask()
            valid_actions = np.where(np.array(env_test.get_valid_act_mask()) == 1)[0]

            if alg == 'trained':
                action = ddqn.act(state, 0)

            elif alg == 'random':
                action = np.random.choice(valid_actions, 1)[0]
            elif alg == 'k8s-la':
                mach_status = env_test.machine_status
                action = generate_kube_action(env_test, valid_actions)

            elif alg == 'g-util':
                action = greedy_policy(env_test, 'util')

            usages = state[env_test.usg_start:]
            cpu_usages = usages[:env_test.nb_w_nodes]
            mem_usages = usages[env_test.nb_w_nodes:env_test.nb_w_nodes*2]
            disk_usages = usages[env_test.nb_w_nodes*2:]

            per_unused_list.append(calculate_per_unused(usages, env_test.nb_res_dims))
            max_machine_idx = np.argmax([cpu_usages[i] + mem_usages[i] + disk_usages[i] for i in range(env_test.nb_w_nodes)])
            min_machine_idx = np.argmin([cpu_usages[i] + mem_usages[i] + disk_usages[i] for i in range(env_test.nb_w_nodes)])

            if action == max_machine_idx:
                mm_cnt += 1
            if action == min_machine_idx:
                mmin_cnt += 1
            if usages[action] == 0 and usages[action + env_test.nb_w_nodes] == 0 and usages[action + 2*env_test.nb_w_nodes] == 0:
                zm_cnt += 1

            # get reward, next_state
            state, reward, _, _ = env_test.step(action)

            reward_list.append(reward)


        results_dict['nb_unused'][alg].append(np.mean(per_unused_list))
        results_dict['reward'][alg].append(np.mean(reward_list))
        results_dict['epi_len'][alg].append(epi_length)
        results_dict['wait'][alg].append(env_test.cum_waittime)
        results_dict['throughput'][alg].append(env_test.i/env_test.elapsed_time)

for alg in ['random', 'trained', 'k8s-la', 'g-util']:
    results_dict['nb_unused'][alg] = np.mean(results_dict['nb_unused'][alg])  # mean over episodes
    results_dict['reward'][alg] = np.mean(results_dict['reward'][alg])  # mean over episodes

    results_dict['epi_len'][alg] = np.mean(results_dict['epi_len'][alg])  # mean over episodes
    results_dict['wait'][alg] = np.mean(results_dict['wait'][alg])
    results_dict['throughput'][alg] = np.mean(results_dict['throughput'][alg])

print('% of unused machines       :          ', results_dict['nb_unused'])
print('reward per episode         :            ', results_dict['reward'])
print('Avg. wait time per episode :    ', results_dict['wait'])
print('Avg. throughput per episode:    ', results_dict['throughput'])
# print('% of max machine selections:   ', mm_count)
# print('% of min machine selections:   ', mmin_count)
# print('% of empty machine selections: ', zm_count)
