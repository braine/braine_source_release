from PIL import Image
import gym
import torch
from WP3R.dqn_models import DQN
from WP3R.helper_func import *
from common.config import GYM_ENV_CFG, DRL_CFG
import os

frames_for_gif = 1000

# Create folder if it doesn't exist
out_dir = 'img_for_gif'
if not os.path.exists(out_dir):
    os.makedirs(out_dir)


test_data, test_task_dur_list, attr_idx, state_indices = pre_process_data(mode='test')

env_test = gym.make('emdc-v0',
               task_list=test_data,
               state_indices=state_indices,
               attr_idx=attr_idx,
               task_dur_list=test_task_dur_list,
               gym_cfg=GYM_ENV_CFG,
               global_cfg=GLOBAL_CFG,
               drl_cfg=DRL_CFG
              )


ddqn = DQN(env=env_test,
           gym_env=GYM_ENV_CFG,
           model_cfg=DRL_CFG,
           mask_env=True)




ddqn.current_model.load_state_dict(torch.load('DDQN_model_weights.pt', map_location='cpu'))

for file in glob.glob(out_dir + '/*.png'):
    os.remove(file)

done = False

state = env_test.reset()
j = 0
color_dict = {}
# for elm in plt.get_cmap('Set1',8):

cmap = plt.get_cmap('tab20b')
colors = cmap(np.linspace(0, 1,  frames_for_gif+1))
np.random.shuffle(colors)

task_name_list = []
tm_wait = 0
while not done:
    color_list = []
    plt.style.use('ggplot')
    fig, axes = plt.subplots(figsize=(20, 8))


    action = ddqn.act(state, 0)

    # mach_cap = env_test.machine_capacities
    # mach_status = env_test.machine_status
    # action = generate_kube_action(mach_status, mach_cap)

    # For debugging
    print('action: %s idx: %i' % (action, j))
    print_states = ['%.2f' % elem for elem in state[2:]]
    print('states: %s' % (print_states))

    # get reward, next_state
    state, reward, done, _ = env_test.step(action)
    current_tasks_per_machine = env_test.machine_status
    machine_usage = {}

    task_list = []
    for machine in range(GYM_ENV_CFG['NB_NODES']):
        curr_machine = current_tasks_per_machine[machine]
        if len(curr_machine) > 0:
            for task in curr_machine:
                task_list.append([task['task_id'], machine, task['mem'], task['cpu'], task['rem_time']])

    task_list.sort(key=lambda x: x[0])

    for elm in task_list:
        color_list.append(colors[int(elm[0])])
        if str(elm[0]) not in task_name_list:
            task_name_list.append(str(elm[0]))
    task_names = []
    for i in range(len(task_list)):
        curr_item = task_list[i]
        task_id = curr_item[0]
        time_remaining = curr_item[-1]
        task_names.append('tid:%i-tr%i' % (task_id, time_remaining))

    values_for_mem_df = np.zeros((GYM_ENV_CFG['NB_NODES'], len(task_list)))
    values_for_cpu_df = np.zeros((GYM_ENV_CFG['NB_NODES'], len(task_list)))

    for idx, row in enumerate(task_list):
        task_id, machine, mem, cpu, rem_time = row
        # task_list =
        values_for_mem_df[machine, idx] = mem
        values_for_cpu_df[machine, idx] = cpu

    # this can be performed outside of the loop since it's fixed
    machine_names = []
    for i in range(GYM_ENV_CFG['NB_NODES']):
        machine_names.append('M%i' % i)

    mem_df = pd.DataFrame(values_for_mem_df, columns=task_names)

    # Multiply values by 100 to get a percentage, can do this in the plotting function to simplify things too
    mem_df *= 100
    cpu_df = pd.DataFrame(values_for_cpu_df, columns=task_names)
    cpu_df *= 100
    mem_df.index= [machine_names]
    cpu_df.index = [machine_names]
    title = 'action: %i \n time: %i' % (action, j)
    ax = plot_clustered_stacked(dfall=[mem_df, cpu_df],
                                colors=color_list,
                                labels=["mem", "cpu"],
                                title=title)
    plt.ylabel('Percent usage')
    plt.xlabel('Machines')

    # for p in ax.get_children()[:-1]:  # skip the last patch as it is the background
    #     if isinstance(p, Rectangle):
    #         x, y = p.get_xy()
    #         w, h = p.get_width(), p.get_height()
    #         if h > 0:  # anything that have a height of 0 will not be annotated
    #             ax.text(x + 0.5 * w, y + 0.5 * h, '%0.2f' % h, va='center', ha='center')
    plt.ylim(0,110)
    plt.tight_layout()

    plt.gca.legend_ = None

    tj = str(j)
    if j < 10:
        tj = '00' + str(j)
    elif j < 100:
        tj = '0' + str(j)
    fig.savefig(out_dir + '/temp_%s.jpg' % tj, dpi=fig.dpi)
    j += 1
    plt.close()
    if j>= frames_for_gif:
        break

imgs = glob.glob(out_dir + '/*.jpg')
imgs.sort()
frames= []
for i in imgs:
    new_frame = Image.open(i)
    frames.append(new_frame)

# Save into a GIF file that loops forever
frames[0].save(out_dir+ '_png_to_gif2.gif', format='GIF',
               append_images=frames[1:],
               save_all=True,
               duration=len(frames)*2, loop=0)
