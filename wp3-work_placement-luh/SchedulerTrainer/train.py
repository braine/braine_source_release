#!/usr/bin/env python
import gym
from common.helper_func import *
#from display_functions import *
from common.drl_algs.dqn_models import DQN
from common.config import GYM_ENV_CFG, DRL_CFG
import torch

train_data, train_tasks_dur, attr_idx, state_indices = pre_process_data()


env = gym.make('gym_emdc:emdc-v0',
               task_list=train_data,
               state_indices=state_indices,
               attr_idx=attr_idx,
               task_dur_list=train_tasks_dur,
               gym_cfg=GYM_ENV_CFG,
               global_cfg=GLOBAL_CFG,
               drl_cfg=DRL_CFG
              )

# epsilon  is for the exploration value
epsilon_by_frame = exponential_decay_func(start=DRL_CFG['EPS_START'], final=DRL_CFG['EPS_FINAL'], decay=DRL_CFG['EPS_DECAY'])
# beta is for the PER buffer
beta_by_frame = beta_func(start=DRL_CFG['BETA_START'], frames=DRL_CFG['BETA_FRAMES'])

ddqn = DQN(env=env, model_cfg=DRL_CFG, mask_env=True)

results_dict = {'wait': {'Trained': [], 'G-ExpFrag': [], 'G-MAvgFrag': [],  'G-Frag': [], 'G-Util': [], 'Random': [], 'K8s': []},
                'runtime': {'Trained': [], 'G-ExpFrag': [], 'G-MAvgFrag': [], 'G-Frag': [], 'G-Util': [], 'Random': [], 'K8s': []},
                'reward': {'Trained': [], 'G-ExpFrag': [], 'G-MAvgFrag': [], 'G-Frag': [], 'G-Util': [], 'Random': [], 'K8s': []},
                'numjobs': {'Trained': [], 'G-ExpFrag': [], 'G-MAvgFrag': [], 'G-Frag': [], 'G-Util': [], 'Random': [], 'K8s': []},
                'possible_actions': np.zeros(DRL_CFG['MAX_STEPS']),
                'wait_at_step': np.zeros(DRL_CFG['MAX_STEPS']),
                'losses': np.empty(DRL_CFG['MAX_STEPS']),
                'step_rewards': np.empty(DRL_CFG['MAX_STEPS']),
                'gaa': [] # Greedy action average
                }


ga_epi_count = 0
total_steps = 0
for episode in range(DRL_CFG['TRAIN_EPISODES']):
    state = env.reset()
    done = False
    results_dict = full_episode_reward(env, results_dict)

    # While loop runs until the episode is over
    current_epi_reward = 0
    while not done:
        total_steps += 1
        # Select an action
        action = ddqn.act(state, epsilon_by_frame(total_steps))

        if greedy_policy(env, 'util') == action:
            ga_epi_count += 1
        # Apply action and observe new state
        next_state, reward, done, _ = env.step(action, )
        # Store the transition in memory
        ddqn.replay_buffer.push(state, action, reward, next_state, done)
        # Move to the next state
        state = next_state

        results_dict['possible_actions'][total_steps - 1] = env.get_valid_act_mask().sum()
        results_dict['wait_at_step'][total_steps - 1] = env.current_waittime
        results_dict['step_rewards'][total_steps-1] = reward
        current_epi_reward += reward


        # Episode complete
        if done:

            results_dict['wait']['Trained'].append(env.cum_waittime)
            results_dict['runtime']['Trained'].append(env.elapsed_time)
            results_dict['reward']['Trained'].append(current_epi_reward)
            results_dict['numjobs']['Trained'].append(env.i)
            results_dict['gaa'].append(ga_epi_count / env.i)

            #plot_results(env=env, results_dict=results_dict)

        if len(ddqn.replay_buffer) > DRL_CFG['BATCH_SIZE']:
            ddqn.beta = beta_by_frame(total_steps)
            loss = ddqn.compute_td_loss()
            results_dict['losses'][total_steps-1] = loss.item()

        if total_steps % DRL_CFG['TARGET_UPD_INT'] == 0:
            ddqn.update_target_model()

        display_progress(env, total_steps, episode)

# save_results(all_rewards, losses, gpas, episodes_waittime, episodes_reward)
save_path = "/data/"
torch.save(ddqn.current_model.state_dict(), '%sDDQN_model_weights.pt' %save_path)
