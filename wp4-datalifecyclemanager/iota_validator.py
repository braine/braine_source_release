import time
import iota_client

LOCAL_NODE_URL = "http://10.9.1.17:14265"

client = iota_client.Client(
    nodes_name_password=[[LOCAL_NODE_URL]], node_sync_disabled=True)

messages = client.find_messages(indexation_keys=["apache_ranger"])
message_id_indexation_queried = client.get_message_index("apache_ranger")
allblocks = []

# message_raw = client.get_message_raw("00c4a09a046089face22e5f021d24ab275cb9a45218b08299d9122deca5f69fc")
# print(message_raw)
start_time = time.time()
for data, i in enumerate(message_id_indexation_queried):
    message_raw = client.get_message_raw(message_id_indexation_queried[data])
    allblocks.append(message_raw)
print("--- %s seconds ---" % (time.time() - start_time))
