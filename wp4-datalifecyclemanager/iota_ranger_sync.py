#Sean Ahearne
#BRAINE Project
import json
import iota_client
import requests
import time

LOCAL_NODE_URL = "http://10.9.1.17:14265"   #REST API of IOTA node
client = iota_client.Client(nodes_name_password=[[LOCAL_NODE_URL]], node_sync_disabled=True) #Open new REST API connection to IOTA node

allblocks = []
allranger = []  #Define 2 lists, one to store all ranger entries, one to store all IOTA entries

def iota_write(block):  #Function to send entry to be written to IOTA blockchain
    client.message(index="apache_ranger", data=block)

def iota_get(allblocks):    #Function to get all entries from IOTA
    message_id_indexation_queried = client.get_message_index("apache_ranger")   #Find by index apache_ranger
    for data, i in enumerate(message_id_indexation_queried): #For all message ID's found with that index
        message_raw = client.get_message_raw(message_id_indexation_queried[data])   #Get the data within that message ID
        start=message_raw.find('"id"')
        end=message_raw.find('"}')
        clean=message_raw[start:end+1]
        clean=('{'+clean+'}')
        allblocks.append(clean) #The above lines fix the formatting to make it identical to ranger, then append to list

def ranger_get(events): #Function to get all entries from ranger
    #Ranger API parameters, date is in past to ensure all entries visible and page size = number of entries returned, change these for dynamic variables for increased performance
    api_url = "http://10.9.100.10:30180/service/assets/accessAudit?startDate=01%2F09%2F2022&pageSize=10000000"  
    response = requests.get(api_url, auth=('admin','Rangeradmin1'))
    list = json.dumps(response.json()) #We want to dump the JSON returned as a string as it contains extra entries we need to remove
    start=list.find('"id"')
    end=list.find('}]')
    test=list[start:end]
    test=('{'+test+'}') #More string formatting to match ranger and IOTA entries
    events=test.split('}, ') #Split the string into a list based on this byte sequence
    for i in (range(len(events)-1)):
        events[i]=(events[i]+'}') #Add the closing brace back to each entry to match it to IOTA
        allranger.append(events[i]) #Append each entry to the global list (won't persist unless you use .append)
    allranger.append(events[i+1]) #Don't add the closing brace to the very last entry as it's already there (hence -1)

iota_get(allblocks)
ranger_get(allranger) #Populate both lists, this can be twin-threaded for performance if needed

missing_blocks = (set(allranger).difference(allblocks)) #Check if there is any difference between ranger and IOTA (assuming Ranger is accurate)

print('Syncing IOTA blockchain with Ranger')
#start_time = time.time()
for i in missing_blocks: #Loop to write each missing entry to IOTA until completed
    encoded_block=i.encode('utf-8') #Required by IOTA
    iota_write(encoded_block)
#print(str(len(missing_blocks))+" missing entries in %s seconds " % (time.time() - start_time))
#print('Sync complete')